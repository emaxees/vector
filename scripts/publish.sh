# 1. Up containers
docker-compose up -d
cd ..
cd frontend
# 3. Build the frontend
docker-compose exec frontend yarn run build env NODE_PATH=src/ REACT_APP_API_URL=https://vectorapp.herokuapp.com/api REACT_APP_WS_URL=wss://vectorapp.herokuapp.com/ws/chat
# 4. Move files at the build root inside a root subdirectory
mkdir -p build/root
for file in $(ls build | grep -E -v '^(index\.html|static|root)$'); do
    cp -r "build/$file" build/root;
done

# 5. Create template folder into backend
mkdir -p ../backend/templates
# 6. Move index.html to backend
cp ./build/index.html ../backend/templates/

#7. Copy whitenoise root into backend app
cd ..
cd backend
rm -rf whitenoise
mkdir -p whitenoise
cp -r ../frontend/build/ ./whitenoise/root

# 8. Remove last collect static
rm -rf static
mkdir -p static
# 9. Build the backend
docker-compose exec backend python manage.py collectstatic
cp -r ./static ./assets

# 10. Publish to heroku
heroku login
heroku container:login
heroku container:push web -a vectorapp
heroku container:release web -a vectorapp

# 11. Down Containers
docker-compose down

echo 'App published successfully'
