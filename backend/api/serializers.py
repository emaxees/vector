from rest_framework import serializers
from users.models import User, Volunteer, Enterprise
from notifications.models import Notification
from skills.serializers import SkillSerializer, InterestSerializer


class VolunteerAuthSerializer(serializers.Serializer):
    id = serializers.CharField()
    email = serializers.EmailField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    occupation = serializers.CharField()
    profile_picture = serializers.ImageField(allow_empty_file=True, required=False)
    bio = serializers.CharField()
    type = serializers.CharField()
    skills = SkillSerializer(many=True)

class EnterpriseAuthSerializer(serializers.Serializer):
    id = serializers.CharField()
    email = serializers.EmailField()
    company_name = serializers.CharField()
    administrator_name = serializers.CharField()
    profile_picture = serializers.ImageField(allow_empty_file=True, required=False)
    type = serializers.CharField()
    interests = InterestSerializer(many=True)
    bio = serializers.CharField()

class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        depth = 1
        fields = '__all__'
