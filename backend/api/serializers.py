import json
from django.utils.six import text_type
from rest_framework import serializers
from django.core import serializers as DjangoSerializers
from rest_framework_simplejwt.serializers import TokenObtainSerializer
from rest_framework_simplejwt.tokens import RefreshToken
from users.models import User, Volunteer, Enterprise
from notifications.models import Notification
from skills.serializers import SkillSerializer, InterestSerializer


class VolunteerAuthSerializer(serializers.Serializer):
    id = serializers.CharField()
    email = serializers.EmailField()
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    occupation = serializers.CharField()
    bio = serializers.CharField()
    type = serializers.CharField()
    skills = SkillSerializer(many=True)

class EnterpriseAuthSerializer(serializers.Serializer):
    id = serializers.CharField()
    email = serializers.EmailField()
    company_name = serializers.CharField()
    administrator_name = serializers.CharField()
    type = serializers.CharField()
    interests = InterestSerializer(many=True)
    bio = serializers.CharField()

class CustomTokenObtainSerializer(TokenObtainSerializer):
    username_field = 'email'

    def validate(self, attrs):
        try:
            user = User.objects.get(email=attrs['email'])
            if user.check_password(attrs['password']):
                return user
            else:
                raise serializers.ValidationError(
                    'Password not valid',
                )
        except User.DoesNotExist:
            raise serializers.ValidationError(
                'No active account found with the given credentials',
            )

class CustomTokenObtainPairSerializer(CustomTokenObtainSerializer):
    @classmethod
    def get_token(cls, user):
        return RefreshToken.for_user(user)

    def validate(self, attrs):
        user = super().validate(attrs)
        refresh = self.get_token(user)
        data = {}
        if Volunteer.objects.filter(email=user.email).exists():
            query = Volunteer.objects.get(email=user.email)
            serializer = VolunteerAuthSerializer(query)
        else:
            query = Enterprise.objects.get(email=user.email)
            serializer = EnterpriseAuthSerializer(query)

        data['user'] = serializer.data
        data['refresh'] = text_type(refresh)
        data['access'] = text_type(refresh.access_token)
        return data

class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        depth = 1
        fields = '__all__'