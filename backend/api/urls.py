from django.urls import path, include
from rest_framework import routers
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from users.views import VolunteerViewSet, EnterpriseViewSet, UserViewSet
from skills.views import SkillViewSet, InterestViewSet
from jobs.views import JobViewSet
from chat.views import ChatViewSet
from reviews.views import ReviewViewSet
from feedback.views import FeedbackViewSet
from .views import NotificationViewSet


router = routers.DefaultRouter()
router.register('users', UserViewSet, 'users')
router.register('volunteers', VolunteerViewSet, 'volunteers')
router.register('enterprises', EnterpriseViewSet, 'enterprises')
router.register('interests', InterestViewSet, 'interests')
router.register('skills', SkillViewSet, 'skills')
router.register('jobs', JobViewSet, 'jobs')
router.register('chats', ChatViewSet, 'chats')
router.register('reviews', ReviewViewSet, 'reviews')
router.register('feedback', FeedbackViewSet, 'feedback')
router.register('notifications', NotificationViewSet, 'notifications')

urlpatterns = [
    path('auth/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('auth/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('auth/password-reset/', include('django_rest_passwordreset.urls', namespace='password_reset')),
]

urlpatterns += router.urls
