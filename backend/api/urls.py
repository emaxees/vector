from django.urls import path, include
from rest_framework import routers
from rest_framework_simplejwt.views import TokenRefreshView
from users.views import VolunteerViewSet, EnterpriseViewSet, UserViewSet
from skills.views import SkillViewSet, InterestViewSet
from jobs.views import JobViewSet
from chat.views import ChatViewSet
from reviews.views import ReviewViewSet
from feedback.views import FeedbackViewSet
from .views import CustomTokenObtainPairView, NotificationViewSet


router = routers.DefaultRouter()
router.register(r'users', UserViewSet, base_name='users')
router.register(r'volunteers', VolunteerViewSet, base_name='volunteers')
router.register(r'enterprises', EnterpriseViewSet, base_name='enterprises')
router.register(r'skills', SkillViewSet, base_name='skills')
router.register(r'interests', InterestViewSet, base_name='interests')
router.register(r'jobs', JobViewSet, base_name='jobs')
router.register(r'chats', ChatViewSet, base_name='chats')
router.register(r'reviews', ReviewViewSet, base_name='reviews')
router.register(r'notifications', NotificationViewSet, base_name='notifications')
router.register(r'feedback', FeedbackViewSet, base_name='feedback')

urlpatterns = [
    path('auth/token/', CustomTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('auth/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]
urlpatterns += router.urls
