from users.models import User
from rest_framework import viewsets
from rest_framework.response import Response
from .serializers import NotificationSerializer


class NotificationViewSet(viewsets.ViewSet):
    def list(self, request):
        user = User.objects.get(id=request.user.id)
        serializer = NotificationSerializer(user.notifications.all(), many=True)
        return Response(serializer.data)

    def update(self, request, pk=None):
        user = User.objects.get(id=pk)
        user.notifications.mark_all_as_read()
        serializer = NotificationSerializer(user.notifications.all(), many=True)
        return Response(serializer.data)
