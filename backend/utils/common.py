from users.models import Volunteer, Enterprise


def get_user_name(user):
    if user.type == 'volunteer':
        volunteer = Volunteer.objects.get(email=user.email)
        return '{} {}'.format(volunteer.first_name, volunteer.last_name)
    else:
        enterprise = Enterprise.objects.get(email=user.email)
        return '{}'.format(enterprise.company_name)
