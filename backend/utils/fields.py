from django.db import models


class EmailLowerField(models.EmailField):
    def to_python(self, value):
        try:
            return value.lower()
        except:
            return value
