from model_utils import Choices


FEE = Choices('paid', 'bono')
STATUES = Choices('pending', 'rejected', 'approved')
FEEDBACK_TYPES = Choices(
    ('issue', 'Issue'),
    ('improvement', 'Improvement'),
    ('visual', 'Visual'),
    ('comment', 'Comment'),
)