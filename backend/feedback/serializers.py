from rest_framework import serializers
from .models import Feedback
from utils.choises import STATUES


class FeedbackWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feedback
        fields = (
            'type',
            'description',
            'user'
        )
