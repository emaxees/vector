# Generated by Django 2.1.2 on 2019-03-25 15:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedback',
            name='submitted_date',
            field=models.DateField(auto_now=True, verbose_name='submitted date'),
        ),
    ]
