from jobs.models import Job
from django.db import models
from users.models import User
from utils.choises import FEEDBACK_TYPES


class Feedback(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    type = models.CharField(
        max_length=50,
        choices=FEEDBACK_TYPES,
        default=FEEDBACK_TYPES.comment
    )
    description = models.TextField('description')
    submitted_date = models.DateField('submitted date', auto_now=True)

    def __str__(self):
        return '{}'.format(self.user.email)

