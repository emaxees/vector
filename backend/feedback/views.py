from rest_framework.response import Response
from .serializers import FeedbackWriteSerializer
from rest_framework import viewsets
from .models import Feedback


class FeedbackViewSet(viewsets.ViewSet):
    def create(self, request):
        serializer = FeedbackWriteSerializer(data={**request.data, 'user': request.user.id })
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)