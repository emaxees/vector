from django.db.models.signals import post_save
from notifications.signals import notify
from django.dispatch import receiver
from utils.common import get_user_name
from .models import Chat


@receiver(post_save, sender=Chat)
def handle_pre_save (sender, instance, *args, **kwargs):
    if len(instance.messages.all()) == 0:
        notify.send(
            instance,
            recipient=instance.sender,
            verb='You have a new chat with {}'.format(get_user_name(instance.receiver))
        )
        notify.send(
            instance,
            recipient=instance.receiver,
            verb='You have a new chat with {}'.format(get_user_name(instance.sender))
        )
