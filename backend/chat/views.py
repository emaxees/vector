from rest_framework import viewsets
from django.db.models import Q
from django.shortcuts import render
from .models import Chat
from .serializers import ChatSerializer
from rest_framework.response import Response


class ChatViewSet(viewsets.ViewSet):

    def list(self, request):
        chats = Chat.objects.filter(Q(sender=request.user) | Q(receiver=request.user))
        serializer = ChatSerializer(chats, many=True)
        return Response(serializer.data)
