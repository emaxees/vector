import uuid
from django.core.exceptions import ValidationError
from django.db import models
from users.models import User


def validate_message_content(content):
    if content is None or content == "" or content.isspace():
        raise ValidationError(
            'Content is empty/invalid',
            code='invalid',
            params={'content': content},
        )

class Message(models.Model):
    id = models.UUIDField(
        primary_key=True,
        null=False,
        default=uuid.uuid4,
        editable=False
    )
    author = models.ForeignKey(
        User,
        blank=False,
        null=False,
        related_name='author',
        on_delete=models.CASCADE
    )
    content = models.TextField(validators=[validate_message_content])
    created_at = models.DateTimeField(auto_now_add=True, blank=True)

class Chat(models.Model):
    sender = models.ForeignKey(
        User,
        blank=False,
        null=False,
        related_name='sender',
        on_delete=models.CASCADE
    )
    receiver = models.ForeignKey(
        User,
        blank=False,
        null=False,
        related_name='receiver',
        on_delete=models.CASCADE
    )
    messages = models.ManyToManyField(Message, related_name='messages', blank=True)

    def __str__(self):
        return 'Chat {}-{}'.format(self.sender.email, self.receiver.email)

    class Meta:
        ordering = ['id']
        verbose_name_plural = 'Chats'

    def get_last_50_messages(self):
        return self.messages.all().order_by('-created_at')[:50]
