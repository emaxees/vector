from rest_framework import serializers
from .models import Chat
from utils.choises import STATUES


class ChatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chat
        depth = 1
        fields = (
            'id',
            'sender',
            'receiver',
            'messages',
        )
