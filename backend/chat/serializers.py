from rest_framework import serializers
from .models import Chat
from utils.choises import STATUES
from users.models import Enterprise, Volunteer
from users.serializers import VolunteerSerializer, EnterpriseSerializer


class ChatSerializer(serializers.ModelSerializer):
    sender = serializers.SerializerMethodField()
    receiver = serializers.SerializerMethodField()

    class Meta:
        model = Chat
        depth = 1
        fields = (
            'id',
            'sender',
            'receiver',
            'messages',
        )

    def get_sender(self, obj):
        if obj.sender.type == 'enterprise':
            enterprise = Enterprise.objects.get(id=obj.sender.id)
            serializer = EnterpriseSerializer(enterprise)
            return serializer.data
        else:
            volunteer = Volunteer.objects.get(id=obj.sender.id)
            serializer = VolunteerSerializer(volunteer)
            return serializer.data

    def get_receiver(self, obj):
        if obj.receiver.type == 'enterprise':
            enterprise = Enterprise.objects.get(id=obj.receiver.id)
            serializer = EnterpriseSerializer(enterprise)
            return serializer.data
        else:
            volunteer = Volunteer.objects.get(id=obj.receiver.id)
            serializer = VolunteerSerializer(volunteer)
            return serializer.data
