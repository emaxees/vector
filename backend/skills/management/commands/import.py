# -*- coding: utf-8 -*-
# Author: Thura Hlaing <trhura@gmail.com>
# Time-stamp: <2013-08-29 16:13:36 (trhura)>

import sys,os
import csv

from django.core.management.base import BaseCommand, CommandError
from skills.models import Interest, Skill, RelatedSkill


class Command(BaseCommand):
    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('csv-path', nargs='+', type=str)

    def handle(self, *args, **options):
        dir = os.path.dirname(__file__) 
        csvPath = os.path.realpath("{0}/{1}".format(dir, options['csv-path'][1]))
        print(csvPath)
        if not os.path.exists (csvPath):
            raise CommandError ("%s doesnt exist." %csvPath)
        with open (csvPath, 'r') as csvFile:
            reader = csv.reader (csvFile, delimiter=',', quotechar="\"")
            for row in reader:
                try:
                    for i, field in enumerate(row):
                        print(i)
                except Exception as e:
                    raise CommandError(e)