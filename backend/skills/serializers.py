from .models import Skill, Interest
from rest_framework import serializers


class SkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = Skill
        fields = (
            'id',
            'name'
        )

class InterestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Interest
        fields = (
            'id',
            'name'
        )
