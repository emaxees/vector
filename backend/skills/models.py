from django.db import models
from django.core.exceptions import ValidationError


class Skill(models.Model):
    name = models.CharField('Name', max_length=50, blank=False)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name_plural = 'Skills'


class Interest(models.Model):
    name = models.CharField('Name', max_length=50, blank=False)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name_plural = 'Interests'
