from rest_framework import viewsets
from django.shortcuts import render
from .models import Skill, Interest
from .serializers import SkillSerializer, InterestSerializer
from rest_framework.response import Response


class SkillViewSet(viewsets.ViewSet):

    def list(self, request):
        skills = Skill.objects.all()
        serializer = SkillSerializer(skills, many=True)
        return Response(serializer.data)

class InterestViewSet(viewsets.ViewSet):

    def list(self, request):
        interests = Interest.objects.all()
        serializer = InterestSerializer(interests, many=True)
        return Response(serializer.data)
