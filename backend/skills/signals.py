from django.db.models.signals import pre_save
from django.dispatch import receiver
from reviews.models import Review
from users.models import Volunteer, VolunteerSkills


@receiver(pre_save, sender=Review)
def calculate_skills_points (sender, instance, *args, **kwargs):
    if (instance.user.type == 'enterprise' or not instance.job.finished): return
    applicant = instance.job.applicants.all().filter(volunteer_id=instance.user.id)[0]
    volunteer_skills = Volunteer.objects.get(id=instance.user.id).skills.all()
    for requirement in instance.job.requirements.all():
        for volunteer_skill in volunteer_skills:
            if (requirement.skill.name == volunteer_skill.skill.name):
                skill = VolunteerSkills.objects.filter(id=volunteer_skill.id)
                if skill[0].points == 1000: return
                points = skill[0].points + applicant.hours*instance.rating
                if points >= 1000: 
                    skill.update(points=1000)
                else:
                    skill.update(points=points)
 