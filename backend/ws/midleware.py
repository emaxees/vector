from rest_framework.authtoken.models import Token
from rest_framework_simplejwt.authentication import JWTAuthentication


class TokenAuthMiddleware:
    """
    Token authorization middleware for Django Channels 2
    """

    def __init__(self, inner):
        self.inner = inner

    def __call__(self, scope):
        query = dict((x.split('=') for x in scope['query_string'].decode().split("&")))
        token = query['token']
        jwt_auth = JWTAuthentication()
        validated_token = jwt_auth.get_validated_token(raw_token=token)
        user = jwt_auth.get_user(validated_token)
        scope['user'] = user
        return self.inner(scope)