from django.conf.urls import url
from .midleware import TokenAuthMiddleware
from channels.routing import ProtocolTypeRouter, URLRouter
from .consumers import ChatConsumer


websocket_urlpatterns = [
    url(r'^ws/chat$', ChatConsumer),
]

application = ProtocolTypeRouter({
    'websocket': TokenAuthMiddleware(
        URLRouter(
            websocket_urlpatterns
        )
    ),
})
