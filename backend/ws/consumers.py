# chat/consumers.py
from channels.generic.websocket import AsyncWebsocketConsumer
from channels.layers import get_channel_layer
from django.db.models import Q
from channels.db import database_sync_to_async
from users.models import User, Enterprise, Volunteer
from users.serializers import VolunteerSerializer, EnterpriseSerializer
from chat.models import Message, Chat
import json


class ChatConsumer(AsyncWebsocketConsumer):

    @database_sync_to_async
    def create_chat(self, sender, receiver):
        return Chat.objects.create(sender=sender, receiver=receiver)

    @database_sync_to_async
    def save_message_to_chat(self, message):
        self.chat.messages.add(message)
        return self.chat.save()

    @database_sync_to_async
    def get_chat(self, sender, receiver):
        try:
            chat = Chat.objects.get(Q(sender=sender, receiver=receiver) | Q(sender=receiver, receiver=sender))
            return chat
        except Chat.DoesNotExist:
            return None


    @database_sync_to_async
    def create_message(self, author, content):
        return Message.objects.create(content=content, author=author)

    def messages_to_json(self, messages):
        result = []
        for message in messages:
            result.append(self.message_to_json(message))
        return result

    def message_to_json(self, message):
        if message.author.type == 'enterprise':
            enterprise = Enterprise.objects.get(id=message.author.id)
            serializer = EnterpriseSerializer(enterprise)
        else:
            volunteer = Volunteer.objects.get(id=message.author.id)
            serializer = VolunteerSerializer(volunteer)
        return {
            'id': str(message.id),
            'content': message.content,
            'author': serializer.data,
            'created_at': str(message.created_at)
        }

    async def init_chat(self, data):
        self.room_name = self.scope['user'].id
        receiver = User.objects.get(email=data['user'])
        if (self.room_name < receiver.id):
            self.room_group_name = 'chat_{}_{}'.format(self.room_name, receiver.id)
        else:
            self.room_group_name = 'chat_{}_{}'.format(receiver.id, self.room_name)

        content = {
            'command': 'init_chat'
        }

        if not receiver:
            content['error'] = 'Unable to start chat with username: {}'.format(data['user'])
            self.send_message(content)

        self.chat = await self.get_chat(sender=self.scope['user'], receiver=receiver)
        if not self.chat:
            self.chat = await self.create_chat(sender=self.scope['user'], receiver=receiver)

        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        content['success'] = 'Chatting success with: {}'.format(receiver.email)
        await self.send(text_data=json.dumps(content))


    async def new_message(self, data):
        message = await self.create_message(content=data['content'], author=self.scope['user'])
        await self.save_message_to_chat(message)
        if message.author.type == 'enterprise':
            enterprise = Enterprise.objects.get(id=message.author.id)
            serializer = EnterpriseSerializer(enterprise)
        else:
            volunteer = Volunteer.objects.get(id=message.author.id)
            serializer = VolunteerSerializer(volunteer)
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'channel_message',
                'payload': {
                    'content': message.content,
                    'command': 'new_message',
                    'created_at': message.created_at.date().isoformat(),
                    'author': serializer.data,
                    'id': message.id.__str__(),
                }
            }
        )

    async def channel_message(self, event):
        data = event['payload']
        await self.send(text_data=json.dumps(data))

    async def fetch_messages(self, data):
        messages = self.chat.get_last_50_messages()
        content = {
            'command': 'messages',
            'messages': self.messages_to_json(messages)
        }
        await self.send(text_data=json.dumps(content))

    commands = {
        'init_chat': init_chat,
        'new_message': new_message,
        'fetch_messages': fetch_messages,
        'channel_message': channel_message
    }

    async def connect(self):
        await self.accept()

    async def disconnect(self, close_code):
        await self.close()

    # Receive message from WebSocket
    async def receive(self, text_data):
        data = json.loads(text_data)
        await self.commands[data['command']](self, data)
