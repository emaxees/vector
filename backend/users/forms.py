from django import forms
from .models import User

class UserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = [
            'password',
            'username',
            'first_name',
            'email',
            'fist_name',
            'last_name',
            'occupation',
            'bio',
            'profile_picture',
        ]
