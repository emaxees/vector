# Generated by Django 2.1.2 on 2018-11-24 16:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0003_auto_20181124_1528'),
        ('users', '0003_enterprise_interests'),
    ]

    operations = [
        migrations.AddField(
            model_name='enterprise',
            name='jobs',
            field=models.ManyToManyField(blank=True, to='jobs.Job'),
        ),
    ]
