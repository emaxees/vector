from django.db import models
from django.core import validators
from django.utils import timezone
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, UserManager
from django.core.mail import send_mail
from rest_framework.exceptions import ValidationError
from skills.models import Skill, Interest
from jobs.models import Job
from utils.fields import EmailLowerField
from utils.choises import FOUND_TYPES


class User(AbstractBaseUser, PermissionsMixin):
    email = EmailLowerField(
        'email address', unique=True,
        error_messages={
            'unique': "A user with that email already exists.",
        }
    )
    profile_picture = models.ImageField('profile picture', upload_to='profiles', blank=True)
    username = models.CharField(
        'username', max_length=30, unique=True, blank=True, null=True,
        help_text='30 characters or fewer. Letters, digits and _ only.',
        validators=[
            validators.RegexValidator(
                r'^\w+$',
                'Enter a valid username. This value may contain only '
                  'letters, numbers and _ character.'
                'invalid'
            ),
        ],
        error_messages={
            'unique': "The username is already taken.",
        }
    )
    is_staff = models.BooleanField(
        'Staff Status', default=False,
        help_text= 'Designates whether the user can log into this admin '
                    'site.'
    )
    is_active = models.BooleanField(
        'Active', default=True,
        help_text= 'Designates whether this user should be treated as '
                    'active. Unselect this instead of deleting accounts.'
    )
    date_joined = models.DateTimeField('date joined' , default=timezone.now)
    online = models.BooleanField(null=False, blank=False, default=False)
    phone_number = models.CharField('phone number', max_length=50, blank=True)
    found = models.CharField(
        max_length=50,
        choices=FOUND_TYPES,
        default=FOUND_TYPES.social
    )

    objects = UserManager()

    USERNAME_FIELD = 'email'

    class Meta(object):
        verbose_name = 'User'
        verbose_name_plural = 'Users'
        abstract = False

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.get_full_name()

    def __str__(self):
        return self.email

    def email_user(self, subject, message, from_email=None, **kwargs):
        send_mail(subject, message, from_email, [self.email], **kwargs)

    @property
    def type(self):
        if (Volunteer.objects.filter(email=self.email).exists()):
            return 'volunteer'
        else:
            return 'enterprise'

class VolunteerSkills(models.Model):
    skill = models.ForeignKey(Skill, on_delete=models.CASCADE)
    points = models.IntegerField('points', default=0)

    def __str__(self):
        return '{} {}'.format(self.skill.name, self.points)

    @property
    def name(self):
        return self.skill.name


class Volunteer(User):
    first_name = models.CharField('first name', max_length=50, blank=False)
    last_name = models.CharField('last name', max_length=50, blank=False)
    occupation = models.CharField('ocupation', max_length=50, blank=True)
    bio = models.TextField('bio', max_length=500, blank=True)
    skills = models.ManyToManyField(VolunteerSkills, blank=True)

    def __str__(self):
        return self.email

    class Meta:
        ordering = ['email']
        verbose_name_plural = 'volunteers'


class Enterprise(User):
    company_name = models.CharField('company name', max_length=50, blank=False)
    administrator_name = models.CharField('administrator name', max_length=50, blank=False)
    bio = models.TextField('bio', blank=True)
    interests = models.ManyToManyField(Interest, blank=True)
    jobs = models.ManyToManyField(Job, blank=True)

    def __str__(self):
        return self.company_name

    class Meta:
        ordering = ['company_name']
        verbose_name_plural = 'enterprises'
