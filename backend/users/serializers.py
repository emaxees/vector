from rest_framework import serializers
from django.contrib.auth.hashers import make_password
from .models import Volunteer, Enterprise, VolunteerSkills
from skills.models import Skill, Interest
from reviews.models import Review
from reviews.serializers import ReviewSerializer
from utils.choises import STATUES


class VolunteerSkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = VolunteerSkills
        depth = 1
        fields = (
            'id',
            'name',
            'points',
            'skill'
        )

class VolunteerSerializer(serializers.ModelSerializer):
    skills = VolunteerSkillSerializer(many=True)
    reviews = serializers.SerializerMethodField()

    class Meta:
        model = Volunteer
        depth = 1
        fields = (
            'id',
            'email',
            'first_name',
            'last_name',
            'password',
            'occupation',
            'bio',
            'profile_picture',
            'type',
            'reviews',
            'skills'
        )
        extra_kwargs = {
            'password': { 'write_only': True }
        }

    def get_reviews(self, obj):
        return ReviewSerializer(Review.objects.filter(user_id=obj.id).all(), many=True).data


class VolunteerWriteSerializer(VolunteerSerializer):
    skills = serializers.PrimaryKeyRelatedField(queryset=Skill.objects.all(), required=False, many=True)

    class Meta(VolunteerSerializer.Meta):
        depth = 0

    def create(self, validated_data):
        return Volunteer.objects.create(
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            password = make_password(validated_data['password'])
        )

    def update(self, instance, validated_data):
        attrs = {}
        for key in validated_data:
            if key != 'skills':
                attrs[key] = validated_data[key]

        instance.__dict__.update(attrs)
        if 'skills' in validated_data:
            skills = [VolunteerSkills.objects.create(skill=skill) for skill in validated_data['skills']]
            instance.skills.set(skills)

        instance.save()
        return instance

class EnterpriseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Enterprise
        depth = 1
        fields = (
            'id',
            'company_name',
            'administrator_name',
            'email',
            'password',
            'type',
            'interests',
            'jobs',
            'bio'
        )
        extra_kwargs = {
            'password': { 'write_only': True }
        }

class EnterpriseWriteSerializer(EnterpriseSerializer):
    class Meta(EnterpriseSerializer.Meta):
        depth = 0

    def create(self, validated_data):
        return Enterprise.objects.create(
            email=validated_data['email'],
            company_name=validated_data['company_name'],
            administrator_name=validated_data['administrator_name'],
            password = make_password(validated_data['password'])
        )

    def update(self, instance, validated_data):
        attrs = {}
        for key in validated_data:
            if key != 'interests':
                attrs[key] = validated_data[key]

        if 'interests' in validated_data:
            instance.interests.set(validated_data['interests'])

        instance.__dict__.update(attrs)
        instance.save()
        return instance

class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)
