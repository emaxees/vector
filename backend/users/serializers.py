from rest_framework import serializers
from django.contrib.auth.hashers import make_password
from .models import Volunteer, Enterprise, VolunteerSkills
from skills.models import Skill, Interest
from reviews.models import Review
from jobs.models import Job, RequiredSkill
from reviews.serializers import ReviewSerializer
from utils.choises import STATUES


class RequiredSkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = RequiredSkill
        depth = 1
        fields = (
            'skill',
            'points',
        )

class JobSerializer(serializers.ModelSerializer):
    requirements = RequiredSkillSerializer(many=True)
    in_progress = serializers.SerializerMethodField()

    class Meta:
        model = Job
        depth = 1
        fields ='__all__'

    def get_in_progress(self, obj):
        applicant = obj.applicants.filter(status='approved')
        return applicant.exists()


class VolunteerSkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = VolunteerSkills
        depth = 1
        fields = (
            'id',
            'name',
            'points',
            'skill'
        )

class VolunteerSerializer(serializers.ModelSerializer):
    skills = VolunteerSkillSerializer(many=True)
    reviews = serializers.SerializerMethodField()
    display_name = serializers.SerializerMethodField()

    class Meta:
        model = Volunteer
        depth = 1
        fields = (
            'id',
            'email',
            'first_name',
            'last_name',
            'password',
            'occupation',
            'bio',
            'profile_picture',
            'type',
            'reviews',
            'skills',
            'phone_number',
            'found',
            'display_name'
        )
        extra_kwargs = {
            'password': { 'write_only': True },
            'found': { 'write_only': True },
        }

    def get_reviews(self, obj):
        return ReviewSerializer(Review.objects.filter(user_id=obj.id).all(), many=True).data

    def get_display_name(self, obj):
        return '{} {}'.format(obj.first_name, obj.last_name)

class VolunteerWriteSerializer(VolunteerSerializer):
    skills = serializers.PrimaryKeyRelatedField(queryset=Skill.objects.all(), required=False, many=True)
    profile_picture = serializers.ImageField(allow_empty_file=True, required=False)

    class Meta(VolunteerSerializer.Meta):
        depth = 0

    def create(self, validated_data):
        return Volunteer.objects.create(
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            password = make_password(validated_data['password']),
            phone_number = validated_data['phone_number'],
            found = validated_data['found'],
        )

    def update(self, instance, validated_data):
        attrs = {}
        for key in validated_data:
            if key != 'skills':
                attrs[key] = validated_data[key]

        instance.__dict__.update(attrs)
        if 'skills' in validated_data:
            skills = [VolunteerSkills.objects.create(skill=skill) for skill in validated_data['skills']]
            instance.skills.set(skills)

        instance.save()
        return instance

class EnterpriseSerializer(serializers.ModelSerializer):
    jobs = JobSerializer(many=True, required=False)
    display_name = serializers.SerializerMethodField()

    class Meta:
        model = Enterprise
        depth = 1
        fields = (
            'id',
            'company_name',
            'administrator_name',
            'email',
            'password',
            'type',
            'interests',
            'jobs',
            'bio',
            'profile_picture',
            'phone_number',
            'found',
            'display_name',
        )
        extra_kwargs = {
            'password': { 'write_only': True },
            'found': { 'write_only': True },
        }

    def get_display_name(self, obj):
        return '{}'.format(obj.company_name)

class EnterpriseWriteSerializer(EnterpriseSerializer):
    class Meta(EnterpriseSerializer.Meta):
        depth = 0

    def create(self, validated_data):
        return Enterprise.objects.create(
            email=validated_data['email'],
            company_name=validated_data['company_name'],
            administrator_name=validated_data['administrator_name'],
            password = make_password(validated_data['password']),
            phone_number = validated_data['phone_number'],
            found = validated_data['found'],
        )

    def update(self, instance, validated_data):
        attrs = {}
        for key in validated_data:
            if key != 'interests':
                attrs[key] = validated_data[key]

        if 'interests' in validated_data:
            instance.interests.set(validated_data['interests'])

        instance.__dict__.update(attrs)
        instance.save()
        return instance

class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)
