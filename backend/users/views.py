from rest_framework import viewsets, serializers, status
from django.shortcuts import get_object_or_404
from rest_framework.validators import ValidationError
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.contrib.auth.hashers import make_password
from .models import Volunteer, Enterprise, User
from .serializers import VolunteerSerializer, VolunteerWriteSerializer, EnterpriseSerializer, EnterpriseWriteSerializer, ChangePasswordSerializer


class BaseViewSet(viewsets.ViewSet):

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def get_permissions(self):
        if self.action != 'create':
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = []
        return [permission() for permission in permission_classes]


class UserViewSet(viewsets.ViewSet):
    def retrieve(self, request, pk=None):
        queryset = User.objects.all()
        user = get_object_or_404(queryset, pk=pk)
        if (user.type == 'volunteer'):
            volunteer = Volunteer.objects.get(email=user.email)
            serializer = VolunteerSerializer(volunteer)
        else:
            enterprise = Enterprise.objects.get(email=user.email)
            serializer = EnterpriseSerializer(enterprise)
        return Response(serializer.data)

    def create(self, request):
        user = request.user
        serializer = ChangePasswordSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
                if not user.check_password(serializer.data.get('old_password')):
                    return Response({'old_password': ['Wrong password.']}, status=status.HTTP_400_BAD_REQUEST)
                user.set_password(serializer.data.get('new_password'))
                user.save()
                return Response('Success.', status=status.HTTP_200_OK)

class VolunteerViewSet(BaseViewSet):

    def list(self, request):
        volunteers = Volunteer.objects.all()
        serializer = VolunteerSerializer(volunteers, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = VolunteerWriteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Volunteer.objects.all()
        user = get_object_or_404(queryset, pk=pk)
        serializer = VolunteerSerializer(user)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        volunteer = Volunteer.objects.filter(id=pk)
        if volunteer.exists():
            serializer = VolunteerWriteSerializer(volunteer[0], data=request.data, partial=True)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data)
        else:
            raise ValidationError('User {} doesn\'t exist'.format(pk), code=404)

class EnterpriseViewSet(BaseViewSet):

    def list(self, request):
        enterprises = Enterprise.objects.all()
        serializer = EnterpriseSerializer(enterprises, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = EnterpriseWriteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Enterprise.objects.all()
        user = get_object_or_404(queryset, pk=pk)
        serializer = EnterpriseSerializer(user)
        return Response(serializer.data)

    def partial_update(self, request, pk=None):
        enterprise = Enterprise.objects.filter(id=pk)
        if enterprise.exists():
            serializer = EnterpriseWriteSerializer(enterprise[0], data=request.data, partial=True)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data)
        else:
            raise ValidationError('User {} doesn\'t exist'.format(pk), code=404)
