import csv
from django.http import HttpResponse
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext, gettext_lazy as _
from .models import User, Volunteer, Enterprise, VolunteerSkills
from jobs.models import Job


class UserTypeFilter(admin.SimpleListFilter):
    title = 'User Types'
    parameter_name = 'type'

    def lookups(self, request, model_admin):
        return (
            ('volunteer', 'Volunteer'),
            ('enterprise', 'Enterprise'),
        )

    def queryset(self, request, queryset):
        if not self.value():
            return queryset
        filtered = []
        for user in queryset.all():
            if user.type == self.value():
                filtered.append(user.id)
        return queryset.filter(id__in=filtered)

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_filter = (UserTypeFilter, 'is_staff')

    def __init__(self, model, admin_site):
        self.model = model
        if model.__name__ == 'User':
            self.change_list_template = 'admin/change_list_users_graph.html'
        self.opts = model._meta
        self.admin_site = admin_site
        super(admin.ModelAdmin, self).__init__()

    def changelist_view(self, request, extra_context=None):
        volunteers = Volunteer.objects.all()
        enterprises = Enterprise.objects.all()
        extra_context = extra_context or { 'chart_data': [len(volunteers), len(enterprises)] }
        return super().changelist_view(request, extra_context=extra_context)


@admin.register(Volunteer)
class VolunteerAdmin(admin.ModelAdmin):
    actions = ['export_as_csv']
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('profile_picture','first_name', 'last_name', 'occupation', 'bio', 'skills', 'phone_number')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        (_('Marketing'), {'fields': ['found']}),
    )
    list_display = ('email', 'first_name', 'last_name')
    search_fields = ('email', 'first_name', 'last_name')

    def export_as_csv(self, request, queryset):
        field_names = ['email', 'first_name', 'last_name', 'skills', 'applied_to']
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=Volunteers.csv'
        writer = csv.writer(response)
        for query in queryset:
            volunteer = {
                'email': query.email,
                'first_name': query.first_name,
                'last_name': query.last_name,
                'skills': [skill.skill.name for skill in query.skills.all()],
                'applied_to': [job.title for job in Job.objects.filter(applicants__volunteer_id=query.id)],
            }
            writer.writerow([volunteer[field] for field in field_names])
        return response
    export_as_csv.short_description = 'Export as csv'


@admin.register(Enterprise)
class EnterpriseAdmin(admin.ModelAdmin):
    actions = ['export_as_csv']
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('profile_picture','company_name', 'administrator_name', 'interests', 'jobs', 'bio', 'phone_number')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        (_('Marketing'), {'fields': ['found']}),
    )
    list_display = ('email', 'administrator_name', 'company_name')
    search_fields = ('email', 'company_name')

    def export_as_csv(self, request, queryset):
        field_names = ['email', 'company_name', 'administrator_name', 'interests', 'jobs']
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=Enterprises.csv'
        writer = csv.writer(response)
        for query in queryset:
            volunteer = {
                'email': query.email,
                'company_name': query.company_name,
                'administrator_name': query.administrator_name,
                'interests': [interest.name for interest in query.interests.all()],
                'jobs': [job.title for job in query.jobs.all()],
            }
            writer.writerow([volunteer[field] for field in field_names])
        return response
    export_as_csv.short_description = 'Export as csv'
