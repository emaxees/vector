from django.conf import settings
from django.dispatch import receiver
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from anymail.message import attach_inline_image_file
from django_rest_passwordreset.signals import reset_password_token_created


@receiver(reset_password_token_created)
def password_reset_token_created(sender, instance, reset_password_token, *args, **kwargs):
    context = {
        'reset_password_url': "{}/reset-password?token={}".format(settings.SITE_URL, reset_password_token.key)
    }

    email_html_message = render_to_string('email/reset_password.html', context)

    msg = EmailMultiAlternatives(
    subject="Reset your account",
    to=[reset_password_token.user.email],)
    msg.attach_alternative(email_html_message, "text/html")

    msg.tags = ["reset", "password"]
    msg.track_clicks = True

    msg.send()
