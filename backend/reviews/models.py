from django.db import models
from users.models import User
from jobs.models import Job
from django.core.validators import MaxValueValidator, MinValueValidator


class Review(models.Model):
    title = models.CharField('title', max_length=50)
    description = models.TextField('description')
    rating = models.IntegerField(
        default=1,
        validators=[MaxValueValidator(5), MinValueValidator(1)]
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user')
    reviewer = models.ForeignKey(User, on_delete=models.CASCADE, related_name='reviewer')
    job = models.ForeignKey(Job, on_delete=models.CASCADE, related_name='jobs', null=True)
    submitted_date = models.DateField('submitted date', auto_now=True)

    def __str__(self):
        return '{}'.format(self.title)


