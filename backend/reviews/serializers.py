from rest_framework import serializers
from users.models import User
from jobs.models import Job
from .models import Review


class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        depth = 1
        fields = (
            'id',
            'title',
            'description',
            'rating',
            'user',
            'reviewer',
            'job',
            'submitted_date'
        )

class ReviewWriteSerializer(ReviewSerializer):
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    reviewer = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    title = serializers.CharField(required=True, allow_blank=False, max_length=50)
    description = serializers.CharField(required=True, allow_blank=False, max_length=150)
    job = serializers.PrimaryKeyRelatedField(queryset=Job.objects.all())

    def create(self, validated_data):
        review = Review.objects.create(**validated_data)
        return review
