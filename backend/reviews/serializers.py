from rest_framework import serializers
from users.models import User, Enterprise, Volunteer
from jobs.models import Job
from .models import Review


class ReviewSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()
    reviewer = serializers.SerializerMethodField()
    class Meta:
        model = Review
        depth = 1
        fields = (
            'id',
            'title',
            'description',
            'rating',
            'user',
            'reviewer',
            'job',
            'submitted_date'
        )

    def get_user(self, obj):
        if obj.user.type == 'volunteer':
            volunteer = Volunteer.objects.get(email=obj.user.email)
            return '{} {}'.format(volunteer.first_name, volunteer.last_name)
        enterprise = Enterprise.objects.get(email=obj.user.email)
        return '{}'.format(enterprise.company_name)

    def get_reviewer(self, obj):
        if obj.reviewer.type == 'volunteer':
            volunteer = Volunteer.objects.get(email=obj.reviewer.email)
            return '{} {}'.format(volunteer.first_name, volunteer.last_name)
        enterprise = Enterprise.objects.get(email=obj.reviewer.email)
        return '{}'.format(enterprise.company_name)

class ReviewWriteSerializer(ReviewSerializer):
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    reviewer = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    title = serializers.CharField(required=True, allow_blank=False, max_length=50)
    description = serializers.CharField(required=True, allow_blank=False, max_length=150)
    job = serializers.PrimaryKeyRelatedField(queryset=Job.objects.all())

    def create(self, validated_data):
        review = Review.objects.create(**validated_data)
        return review
