from .models import Review
from rest_framework import viewsets
from rest_framework.response import Response
from .serializers import ReviewSerializer, ReviewWriteSerializer


class ReviewViewSet(viewsets.ViewSet):

    def list(self, request):
        reviews = Review.objects.filter(user=request.user)
        serializer = ReviewSerializer(reviews, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        reviews = Review.objects.filter(user_id=pk)
        serializer = ReviewSerializer(reviews, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = ReviewWriteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)
