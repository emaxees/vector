from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.validators import ValidationError
from rest_framework.exceptions import PermissionDenied
from users.models import Enterprise, Volunteer
from chat.models import Chat
from .models import Job, Applicants
from .serializers import JobSerializer, JobWriteSerializer


class JobViewSet(viewsets.ViewSet):
    def list(self, request):
        enterprise = Enterprise.objects.filter(id=request.user.id)
        if enterprise.exists():
            serializer = JobSerializer(enterprise[0].jobs, many=True)
        else:
            jobs = Job.objects.all()
            serializer = JobSerializer(jobs, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Job.objects.all()
        job = get_object_or_404(queryset, pk=pk)
        serializer = JobSerializer(job)
        return Response(serializer.data)

    def create(self, request):
        serializer = JobWriteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()
        enterprise = Enterprise.objects.get(pk=request.user.id)
        enterprise.jobs.add(instance)
        enterprise.save()
        return Response(serializer.data)

    def update(self, request, pk=None):
        job = Job.objects.filter(id=pk)
        if job.exists():
            if request.user.id in [volunteer.id for volunteer in Volunteer.objects.all()]:
                volunteer = Volunteer.objects.get(pk=request.user.id)

                job_skills = [require.skill.name for require in job[0].requirements.all()]
                volunteer_skills = [skill.name for skill in volunteer.skills.all()]
                if all(skill in volunteer_skills for skill in job_skills):
                    for require in job[0].requirements.all():
                        skill = volunteer.skills.all().filter(skill__name=require.skill.name)
                        if skill[0].points < require.points:
                            raise ValidationError({'detail': 'Your skill level {}, is insufficient to apply for the job'.format(skill[0].name)})
                else:
                    raise ValidationError({'detail': 'Your skills does not match with the skills required'})

                aplicant = Applicants.objects.create(volunteer=volunteer)
                job[0].applicants.add(aplicant)
                serializer = JobSerializer(job[0])
                Chat.objects.create(
                    sender=Enterprise.objects.get(jobs__id=job[0].id),
                    receiver=volunteer
                )
                return Response(serializer.data)
            else:
                raise PermissionDenied('You dont have permissions for do this action')
        else:
            raise ValidationError({'detail': 'Job {} doesn\'t exist'.format(pk)}, code=404)

    def partial_update(self, request, pk=None):
        job = Job.objects.filter(id=pk)
        enterprise = Enterprise.objects.filter(id=request.user.id)
        if not enterprise.exists():
            raise PermissionDenied('You dont have permissions for do this action')
        if job.exists():
            if job[0] in enterprise[0].jobs.all():
                serializer = JobWriteSerializer(job[0], data=request.data, partial=True)
                serializer.is_valid(raise_exception=True)
                serializer.save()
                return Response(serializer.data)
            else:
                raise PermissionDenied('You dont have permissions for modify this Job')
        else:
            raise ValidationError({'message': 'Job {} doesn\'t exist'.format(pk)}, code=404)
