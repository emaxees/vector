import csv
from datetime import datetime
from django.http import HttpResponse
from django.contrib import admin
from .models import Job, Applicants, RequiredSkill


class ApplicantsStatusFilter(admin.SimpleListFilter):
    title = 'Aplication Status'
    parameter_name = 'applicants'

    def lookups(self, request, model_admin):
        return (
            ('pending', 'Pending'),
            ('rejected', 'Rejected'),
            ('approved', 'Approved'),
        )

    def queryset(self, request, queryset):
        if not self.value():
            return queryset
        return queryset.filter(applicants__status=self.value())


@admin.register(Job)
class JobAdmin(admin.ModelAdmin):
    change_list_template = 'admin/change_list_jobs_graph.html'
    actions = ['export_as_csv']
    list_filter = (ApplicantsStatusFilter, 'finished')

    def changelist_view(self, request, extra_context=None):
        approved = Applicants.objects.filter(status='approved').count()
        pending = Applicants.objects.filter(status='pending').count()
        rejected = Applicants.objects.filter(status='rejected').count()
        extra_context = extra_context or { 'chart_data': [approved, pending, rejected] }
        return super().changelist_view(request, extra_context=extra_context)

    def export_as_csv(self, request, queryset):
        field_names = ['title', 'fee', 'created_on']
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=Enterprises.csv'
        writer = csv.writer(response)
        for query in queryset:
            job = {
                'title': query.title,
                'fee': query.fee,
                'created_on': query.created_on.strftime("%b %d %Y"),
            }
            writer.writerow([job[field] for field in field_names])
        return response
    export_as_csv.short_description = 'Export as csv'
