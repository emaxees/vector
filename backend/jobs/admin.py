from django.contrib import admin
from .models import Job, Applicants, RequiredSkill


@admin.register(Job)
class JobAdmin(admin.ModelAdmin):
    pass

@admin.register(Applicants)
class ApplicantsAdmin(admin.ModelAdmin):
    pass

@admin.register(RequiredSkill)
class RequiredSkillAdmin(admin.ModelAdmin):
    pass