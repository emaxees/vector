# Generated by Django 2.1.2 on 2018-12-05 00:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('skills', '0003_auto_20181127_2156'),
        ('users', '0004_enterprise_jobs'),
        ('jobs', '0011_auto_20181205_0022'),
    ]

    operations = [
        migrations.CreateModel(
            name='Applicants',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('apply_date', models.DateField(auto_now=True, verbose_name='apply date')),
                ('status', models.CharField(choices=[('pending', 'pending'), ('rejected', 'rejected'), ('approved', 'approved')], default='pending', max_length=50)),
                ('volunteer', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='users.Volunteer')),
            ],
            options={
                'verbose_name_plural': 'Jobs Activities',
                'ordering': ['apply_date'],
            },
        ),
        migrations.RenameModel(
            old_name='JobSkills',
            new_name='Skill',
        ),
        migrations.RemoveField(
            model_name='jobapplicants',
            name='job',
        ),
        migrations.RemoveField(
            model_name='jobapplicants',
            name='volunteer',
        ),
        migrations.AlterField(
            model_name='job',
            name='aplicants',
            field=models.ManyToManyField(blank=True, related_name='aplicants', to='jobs.Applicants'),
        ),
        migrations.DeleteModel(
            name='JobApplicants',
        ),
    ]
