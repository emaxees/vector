# Generated by Django 2.1.2 on 2018-12-04 22:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0009_job_aplicants'),
    ]

    operations = [
        migrations.AlterField(
            model_name='job',
            name='fee',
            field=models.CharField(choices=[('Paid', 'Paid'), ('Free', 'Free')], default='Free', max_length=50),
        ),
        migrations.AlterField(
            model_name='jobapplicants',
            name='status',
            field=models.CharField(choices=[('Pending', 'Pending'), ('Rejected', 'Rejected'), ('Approved', 'Approved')], default='Pending', max_length=50),
        ),
        migrations.AlterField(
            model_name='jobskills',
            name='skill',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='skills.Skill'),
        ),
    ]
