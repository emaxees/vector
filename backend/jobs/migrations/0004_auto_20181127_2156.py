# Generated by Django 2.1.2 on 2018-11-27 21:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobs', '0003_auto_20181124_1528'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='job',
            name='contact_number',
        ),
        migrations.RemoveField(
            model_name='job',
            name='email_address',
        ),
        migrations.RemoveField(
            model_name='job',
            name='full_name',
        ),
        migrations.AlterField(
            model_name='job',
            name='commit_hours',
            field=models.TimeField(blank=True, verbose_name='commit hours'),
        ),
        migrations.AlterField(
            model_name='job',
            name='image',
            field=models.ImageField(blank=True, upload_to='jobs', verbose_name='image'),
        ),
        migrations.AlterField(
            model_name='job',
            name='time',
            field=models.TimeField(blank=True, verbose_name='time'),
        ),
        migrations.AlterField(
            model_name='job',
            name='vacancy',
            field=models.IntegerField(blank=True, verbose_name='vacancy'),
        ),
    ]
