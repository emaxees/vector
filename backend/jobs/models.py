from django.db import models
from django.core.validators import MinValueValidator
from utils.choises import FEE, STATUES


class RequiredSkill(models.Model):
    skill = models.ForeignKey('skills.skill', on_delete=models.CASCADE)
    points = models.IntegerField('required skill points', default=0)

    def __str__(self):
        return '{} {}'.format(self.skill.name, self.points)

class Applicants(models.Model):
    volunteer = models.ForeignKey('users.volunteer', on_delete=models.CASCADE)
    apply_date = models.DateField('apply date', auto_now=True)
    status = models.CharField(
        max_length=50,
        choices=STATUES,
        default=STATUES.pending
    )
    hours = models.IntegerField(
        default=0,
        validators=[MinValueValidator(0)]
    )

    def __str__(self):
        return '{} {}'.format(self.volunteer.first_name, self.volunteer.last_name)

    class Meta:
        ordering = ['apply_date']
        verbose_name_plural = 'applicants'

class Job(models.Model):
    pub_date = models.DateField('date', auto_now=True)
    image = models.ImageField('image', upload_to='jobs', blank=True)
    title = models.CharField('title', max_length=50)
    description = models.TextField('description')
    date = models.DateField('date', auto_now=False)
    location = models.CharField('location', max_length=250)
    fee = models.CharField(
        max_length=50,
        choices=FEE,
        default=FEE.bono
    )
    commit_hours = models.CharField('commit hours', default='', max_length=150)
    vacancy = models.IntegerField('vacancy', blank=True, null=True)
    requirements = models.ManyToManyField(RequiredSkill, related_name='skills', blank=True)
    applicants = models.ManyToManyField(Applicants, related_name='applicants', blank=True)
    finished = models.BooleanField('finished', default=False)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['date']
        verbose_name_plural = 'Jobs'
