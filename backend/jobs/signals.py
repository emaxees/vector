from django.db.models.signals import post_save, pre_save
from django.contrib.contenttypes.models import ContentType
from notifications.signals import notify
from notifications.models import Notification
from django.dispatch import receiver
from utils.common import get_user_name
from users.models import Enterprise, Volunteer
from .models import Job


def send_notification_matching_skill_user(job):
    content_type = ContentType.objects.get(model='job')
    for volunteer in Volunteer.objects.all():
        if not Notification.objects.filter(actor_content_type=content_type, actor_object_id=job.id, recipient=volunteer, level='info').exists():
            job_skills = [require.skill.name for require in job.requirements.all()]
            volunteer_skills = [skill.name for skill in volunteer.skills.all()]
            if len(volunteer_skills) == 0 and len(job_skills) == 0:
                notify.send(
                    job,
                    recipient=volunteer,
                    verb='You have a new job that match with your skills, {}'.format(job.title),
                    level='info'
                )
            else:
                if len(job_skills) != 0 and all(skill in volunteer_skills for skill in job_skills):
                    for require in job.requirements.all():
                        skill = volunteer.skills.all().filter(skill__name=require.skill.name)
                        if skill[0].points < require.points:
                            return
                    notify.send(
                        job,
                        recipient=volunteer,
                        verb='You have a new job that match with your skills, {}'.format(job.title),
                        level='info'
                    )

@receiver(pre_save, sender=Job)
def handle_pre_save (sender, instance, *args, **kwargs):
    if not instance.pk: return
    send_notification_matching_skill_user(instance)
    if Enterprise.objects.filter(jobs__in=[instance]).exists():
        user = Enterprise.objects.get(jobs__in=[instance])
        content_type = ContentType.objects.get(model='job')
        for applicant in instance.applicants.all():
            if applicant.status == 'approved':
                if not Notification.objects.filter(actor_content_type=content_type, actor_object_id=instance.id, recipient=applicant.volunteer, level='success').exists():
                    notify.send(
                        instance,
                        recipient=applicant.volunteer,
                        verb='Your job ({}) submission was approved, you are ready for start, let\'s do it'.format(instance.title),
                        level='success'
                    )
                    try:
                        device.send_message(message='Your job ({}) submission was approved, you are ready for start, let\'s do it'.format(instance.title))
                    except:
                        print('Web push not registered')

            try:
                if not Notification.objects.filter(actor_content_type=content_type, actor_object_id=instance.id, recipient=user, level='success').exists():
                    notify.send(
                        instance,
                        recipient=user,
                        verb='{} was applied to your job {}'.format(get_user_name(applicant.volunteer), instance.title),
                        level='success'
                    )
            except:
                print("Notification send error")
