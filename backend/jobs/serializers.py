from rest_framework import serializers
from drf_extra_fields.fields import Base64ImageField
from utils.choises import STATUES
from users.models import Enterprise, Volunteer
from users.serializers import VolunteerSerializer, EnterpriseSerializer
from skills.models import Skill
from reviews.models import Review
from reviews.serializers import ReviewSerializer
from .models import Job, RequiredSkill, Applicants


class ApplicantsSerializer(serializers.ModelSerializer):
    volunteer = VolunteerSerializer()
    has_review = serializers.SerializerMethodField()

    class Meta:
        model = Applicants
        fields = (
            'id',
            'volunteer',
            'apply_date',
            'status',
            'hours',
            'has_review'
        )

    def get_has_review(self, obj):
        return Review.objects.filter(job__id=self.context.get('job'), user_id=obj.volunteer.id).exists()

class ApplicantsWriteSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    volunteer = serializers.PrimaryKeyRelatedField(queryset=Volunteer.objects.all(), required=False)
    status = serializers.ChoiceField(choices=STATUES, required=False)
    hours = serializers.IntegerField(min_value=0, required=False)

    class Meta(ApplicantsSerializer.Meta):
        fields = (
            'id',
            'volunteer',
            'status',
            'hours'
        )


class RequiredSkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = RequiredSkill
        depth = 1
        fields = (
            'skill',
            'points',
        )

class RequiredSkillWriteSerializer(serializers.Serializer):
    skill = serializers.PrimaryKeyRelatedField(queryset=Skill.objects.all())
    points = serializers.IntegerField(min_value=0, max_value=1000)

class UserSerializer(EnterpriseSerializer):
    class Meta(EnterpriseSerializer.Meta):
        fields = (
            'id',
            'company_name',
            'administrator_name',
        )

class JobReviewSerializer(ReviewSerializer):
    class Meta(ReviewSerializer.Meta):
        depth = 0
        fields = (
            'id',
            'user',
            'reviewer',
            'title',
            'description',
            'rating'
        )

class JobSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()
    requirements = RequiredSkillSerializer(many=True)
    applicants = serializers.SerializerMethodField()
    reviews = serializers.SerializerMethodField()

    class Meta:
        model = Job
        depth = 1
        fields = (
            'id',
            'image',
            'title',
            'description',
            'date',
            'location',
            'fee',
            'commit_hours',
            'vacancy',
            'applicants',
            'requirements',
            'user',
            'finished',
            'reviews'
        )

    def get_reviews(self, obj):
        return JobReviewSerializer(Review.objects.filter(job__id=obj.id).all(), many=True).data

    def get_user(self, obj):
        enterprise = Enterprise.objects.filter(jobs__id=obj.id)
        if enterprise.exists():
            return UserSerializer(enterprise[0]).data
        else:
            return 'Vector'

    def get_applicants(self, obj):
        return ApplicantsSerializer(obj.applicants.all(), many=True, context={'job': obj.id }).data

class JobWriteSerializer(JobSerializer):
    requirements = RequiredSkillWriteSerializer(many=True)
    applicants = ApplicantsWriteSerializer(many=True, required=False)
    image = Base64ImageField(required=False)

    class Meta(JobSerializer.Meta):
        depth = 0

    def create(self, validated_data):
        attrs = {}
        for key in validated_data:
            if key != 'requirements':
                attrs[key] = validated_data[key]

        job = Job.objects.create(**attrs)
        requirements = [RequiredSkill.objects.create(**skill) for skill in validated_data['requirements']]
        job.requirements.set(requirements)
        return job

    def update(self, instance, validated_data):
        attrs = {}
        for key in validated_data:
            if key != 'requirements' or key != 'applicants':
                attrs[key] = validated_data[key]

        instance.__dict__.update(attrs)
        if 'requirements' in validated_data:
            requirements = [RequiredSkill.objects.create(**skill) for skill in validated_data['requirements']]
            instance.requirements.set(requirements)

        if 'applicants' in validated_data:
            applicants = [Applicants.objects.filter(id = applicant.get('id')).update(status=applicant.get('status'), hours=applicant.get('hours')) for applicant in validated_data['applicants']]

        instance.save()
        return instance
