const { override, addDecoratorsLegacy, addPostcssPlugins } = require('customize-cra');


module.exports = override(
    addDecoratorsLegacy(),
    addPostcssPlugins(
        [
            require('postcss-preset-env')({ stage: 0 }),
            require('postcss-import')({ path: ['src', 'node_modules'] }),
            require('postcss-custom-media')({ importFrom: 'src/styles/breakpoints.css' }),
            require('postcss-global-import')(),
        ]
    )
);
