import posed from 'react-pose';

const SlideUp = posed.div({
    open: {
        y: '0',
        opacity: 1,
        transform: 'none',

        delay: ({ count }) => count * 100,
    },
    closed: {
        y: '50px',
        opacity: 0,
    },
});

export default SlideUp;
