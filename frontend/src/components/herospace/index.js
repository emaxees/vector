import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BackgroundImage } from 'components';
import styles from './styles.module.css';


class Herospace extends Component {
    static propTypes = {
        url: PropTypes.string,
    };

    render() {
        const { url } = this.props;

        return (
            <div className={styles.root}>
                <BackgroundImage src={url} />
            </div>
        );
    }
}

export default Herospace;
