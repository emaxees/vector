import React, { Component } from 'react';
import autobind from 'autobind';
import { Card, SkillsList } from 'components';
import { formatDate } from 'utils';
import styles from './styles.module.css';


@autobind
class JobPreview extends Component {
    render() {
        const { job, onClick } = this.props;
        return (
            <Card>
                <div className={styles.root} onClick={onClick}>
                    <div className={styles.header}>
                        <div>{job.title}</div>
                        <div dangerouslySetInnerHTML={{__html: job.description.slice(0,100)}} />
                    </div>
                    <div className={styles.image} style={{ backgroundImage: `url(${job.image}), url(http://www.btklsby.go.id/images/placeholder/camera.jpg)`}}/>
                    <div className={styles.bottom} >
                        <div className={styles.info}>
                            <i className='material-icons'>calendar_today</i>
                            {`${formatDate(job.date)}`}
                        </div>
                        <div className={styles.info}>
                            <i className='material-icons'>location_on</i>
                            {job.location}
                        </div>
                        <div className={styles.info}>
                            <i className='material-icons'>people_outline</i>
                            {job.vacancy}
                        </div>
                        <div className={styles.skills}>
                            <SkillsList skills={job.requirements.map(requirement => ({id: requirement.skill.id, name: requirement.skill.name, points: requirement.points }))} />
                        </div>
                    </div>
                </div>
            </Card>
        )
    }
}

export default JobPreview;