import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './styles.module.css';


class Image extends Component {
    static propTypes = {
        src: PropTypes.string.isRequired,
        onClick: PropTypes.func,
    };

    render() {
        const { src, alt, onClick } = this.props;
        return (
            <div onClick={onClick} className={styles.root}>
                <img src={src} alt={alt} />
            </div>
        );
    }
}

export default Image;
