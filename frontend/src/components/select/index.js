import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './styles.module.css';


class Select extends Component {
    static propTypes = {
        label: PropTypes.string,
        theme: PropTypes.string,
        value: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number,
        ]),
        options: PropTypes.array,
        onChange: PropTypes.func,
        placeholder: PropTypes.string,
        error: PropTypes.bool,
        disabled: PropTypes.bool,
    };

    handleChange = (event) => {
        if (this.props.onChange) this.props.onChange(event.target.value);
    }

    render() {
        return (
            <label className={classNames(styles.root, styles[this.props.theme], {[styles.error]: this.props.error})}>
                {this.props.label && (
                    <div className={styles.label}>
                        {this.props.label}
                    </div>
                )}
                <div className={styles.wrapper}>
                    <select
                        value={this.props.value}
                        onChange={this.handleChange}
                        onBlur={this.props.onBlur}
                        disabled={this.props.disabled}>
                        {this.props.placeholder && <option value='' disabled>{this.props.placeholder}</option>}
                        {this.props.options.map(option => (
                            <option value={option.value} key={option.value}>
                                {option.label}
                            </option>
                        ))}
                    </select>
                </div>
            </label>
        );
    }
}

export default Select;
