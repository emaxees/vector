import React, { Component } from 'react';
import classnames from 'classnames';
import { withRouter } from 'react-router'
import PropTypes from 'prop-types';
import styles from './styles.module.css';


class Message extends Component {
    static propTypes = {
        title: PropTypes.string,
        icon: PropTypes.string,
        text: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.array,
        ]),
    };

    render() {
        const { title, icon, text } = this.props;
        return (
            <div className={styles.root}>
                <i className={classnames(['material-icons', styles.icon])} style={{fontSize: '7rem'}}>
                    {icon}
                </i>
                <div className={styles.title}>
                    {title}
                </div>
                <div className={styles.text}>
                    { text }
                </div>
            </div>
        );
    }
}

export default withRouter(Message);
