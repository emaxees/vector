import React, { Component } from 'react';
import styles from './styles.module.css';

class Section extends Component {
    render() {
        const { children, borderTop, borderBottom } = this.props;
        return (
            <div className={styles.root}>
                { borderTop && <div className={styles.border} />}
                {children}
                { borderBottom && <div className={styles.border} />}
            </div>
        );
    }
}

export default Section;
