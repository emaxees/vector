import React, { Component } from 'react';
import styles from './styles.module.css';


class Line extends Component {
    render() {
        const { children } = this.props;
        return (
            <div className={styles.root}>
                <span>{ children }</span>
            </div>
        );
    }
}

export default Line;
