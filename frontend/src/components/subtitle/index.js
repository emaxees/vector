import React, { Component } from 'react';
import styles from './styles.module.css';


class Subtitle extends Component {
    render() {
        const { children } = this.props;
        return (
            <div className={styles.root}>
                { children }
            </div>
        );
    }
}

export default Subtitle;
