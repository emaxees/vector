import React, { Component } from 'react';
import autobind from 'autobind';
import styles from './styles.module.css';


@autobind
class Subtitle extends Component {
    render() {
        const { children } = this.props;
        return (
            <div className={styles.root}>
                { children }
            </div>
        );
    }
}

export default Subtitle;
