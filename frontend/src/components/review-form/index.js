import React, { Component } from 'react';
import { connect } from 'react-redux';
import autobind from 'autobind';
import { updateReviewForm, resetReviewForm, submitReview } from 'actions';
import { bindActionCreators } from 'redux';
import { Button, Input, Textarea } from 'components';
import { isValidField } from 'utils';
import classnames from 'classnames';
import styles from './styles.module.css';


@connect(
    state => ({
        form: state.reviews.form,
        job: state.jobs.form,
    }),
    dispatch => bindActionCreators({ updateReviewForm, resetReviewForm, submitReview }, dispatch)
)
@autobind
class ReviewForm extends Component {

    componentWillUnmount() {
        const { resetReviewForm } = this.props;
        resetReviewForm();
    }

    handleChange(key) {
        const { updateReviewForm } = this.props;
        return value => updateReviewForm(key, value);
    }

    handleClickStart(value) {
        const { updateReviewForm } = this.props;
        return () => updateReviewForm('rating', Number(value));
    }

    handleClickSubmit() {
        const { submitReview, job, form } = this.props;
        submitReview({
            job: job.id,
            ...form,
        });
    }

    getButtonDisabled() {
        const {
            form: { title, rating, description },
        } = this.props;
        return (!isValidField(title) || !isValidField(rating) || !isValidField(description))
    }

    render() {
        const {
            form: { title, rating, description },
        } = this.props;
        return (
            <div className={styles.root}>
                <Input
                    label='Title'
                    type="text"
                    value={title}
                    onChange={this.handleChange('title')}
                />
                <Textarea
                    label='Description'
                    value={description}
                    rows='4'
                    onChange={this.handleChange('description')}
                />
                <div className={styles.rating}>
                    {[...Array(5).keys()].map(key => (
                        <i key={key} onClick={this.handleClickStart(key+1)} className={classnames('material-icons', {[styles.yellow]: key+1 <= rating })}>
                            star_rate
                        </i>
                    ))}
                </div>
                <Button disabled={this.getButtonDisabled()} theme='primary' onClick={this.handleClickSubmit}>
                    Submit Review
                </Button>
            </div>
        );
    }
}
export default ReviewForm;
