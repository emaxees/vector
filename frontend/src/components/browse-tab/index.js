import React, { Component } from 'react';
import classnames from 'classnames';
import autobind from 'autobind';
import styles from './styles.module.css';


@autobind
class BrowseTab extends Component {
    hangleClickTab(id) {
        const { onClick } = this.props;
        return () => onClick(id);
    }

    render() {
        const { currentTab, tabs } = this.props;
        return (
            <div className={styles.root}>
                {tabs.map(tab => (
                    <div
                        key={tab.id}
                        onClick={this.hangleClickTab(tab.id)}
                        className={classnames({[styles.active]: currentTab === tab.id})}>
                        {tab.label}
                    </div>
                ))}
            </div>
        )
    }
}

export default BrowseTab;
