import React, { Component } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import styles from './styles.module.css';

class Button extends Component {
    static propTypes = {
        onClick: PropTypes.func,
        disabled: PropTypes.bool,
        type: PropTypes.string,
    };

    render() {
        const {
            disabled, onClick, type, children, theme,
        } = this.props;
        return (
            <button
                className={classnames(styles.root, {
                    [styles.disabled]: disabled }, styles[theme])}
                disabled={disabled}
                onClick={onClick}
                type={type}
            >
                {children}
            </button>
        );
    }
}
export default Button;
