import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './styles.module.css';


class Checkbox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            idCheckbox: Math.random().toString(36).substring(7),
        };
    }

    render() {
        const {
            value, label, disabled = false,
            checked, onChange,
        } = this.props;
        const { idCheckbox } = this.state;

        return (
            <label className={styles.root} htmlFor={idCheckbox}>
                <i className={classnames('material-icons', styles['check-mark'])}>
                    {checked ? 'check_box' : 'check_box_outline_blank'}
                </i>
                <input
                    id={idCheckbox}
                    type="checkbox"
                    onChange={onChange}
                    value={value}
                    disabled={disabled}
                    checked={checked}
                />
                { label }
            </label>
        );
    }
}

Checkbox.propTypes = {
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.bool]),
    label: PropTypes.string,
    disabled: PropTypes.bool,
    checked: PropTypes.bool,
    onChange: PropTypes.func,
};

export default Checkbox;

