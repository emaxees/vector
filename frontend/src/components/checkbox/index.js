import React, { Component } from 'react';
import classnames from 'classnames';
import autobind from 'autobind';
import styles from './styles.module.css';


@autobind
class Checkbox extends Component {
    render() {
        const {
            value, label, disabled,
            checked, onChange, id
        } = this.props;
        return (
            <label className={styles.root} htmlFor={id}>
                <span>{ label }</span>
                <div>
                    <input
                        id={id}
                        type='checkbox'
                        onChange={onChange}
                        value={value}
                        disabled={disabled}
                        checked={checked}
                    />
                    <span className={classnames(styles.checkMark, { [styles.checked]: checked})} />
                </div>
            </label>
        );
    }
}

export default Checkbox;
