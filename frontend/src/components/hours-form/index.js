import React, { Component } from 'react';
import { connect } from 'react-redux';
import autobind from 'autobind';
import { updateApplicantsHours, resetApplicant, updateTimesheet } from 'actions';
import { bindActionCreators } from 'redux';
import { Button, Input } from 'components';
import styles from './styles.module.css';


@connect(
    state => ({
        form: state.jobs.form,
        applicant: state.jobs.applicant,
        user: state.user,
    }),
    dispatch => bindActionCreators({ updateApplicantsHours, resetApplicant, updateTimesheet }, dispatch)
)
@autobind
class HoursForm extends Component {
    componentWillUnmount() {
        const { resetApplicant } = this.props;
        resetApplicant();
    }

    handleChange(value) {
        const { updateApplicantsHours, applicant: { id } } = this.props;
        updateApplicantsHours({id, hours: Number(value)});
    }

    handleClickStart(value) {
        const { updateReviewForm } = this.props;
        return () => updateReviewForm('rating', Number(value));
    }

    handleClickSubmitHours() {
        const { updateTimesheet, form } = this.props;
        updateTimesheet(form);
    }

    render() {
        const {
            form: { applicants }, applicant: { id },
        } = this.props;
        const { hours } = applicants.find(applicant => applicant.id === id)
        return (
            <div className={styles.root}>
                <Input
                    label='Hours'
                    type='number'
                    value={hours}
                    onChange={this.handleChange}
                />
                <Button theme='primary' onClick={this.handleClickSubmitHours}>
                    Submit Hours
                </Button>
            </div>
        );
    }
}
export default HoursForm;
