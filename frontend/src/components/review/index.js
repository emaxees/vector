import React, { Component } from 'react';
import { formatDate } from 'core/utils';
import classnames from 'classnames';
import styles from './styles.module.css';


class Review extends Component {
    render() {
        const {
            review: { title, rating, description, reviewer, submittedDate}
        } = this.props;

        return (
            <div className={styles.root}>
                <div className={styles.title}>{title}</div>
                <div className={styles.rating}>
                    {[...Array(5).keys()].map(key => (
                        <i key={key} className={classnames('material-icons', {[styles.yellow]: key+1 <= rating })}>
                            star_rate
                        </i>
                    ))}
                </div>
                <div className={styles.description}>
                        {description}
                </div>
                <div className={styles.bottom}>
                    <div className={styles.user}>
                    <i className='material-icons'>
                            business
                        </i>
                        {`${reviewer}`}
                    </div>
                    <div className={styles.date}>
                        {formatDate(submittedDate)}
                    </div>
                </div>
            </div>
        );
    }
}
export default Review;
