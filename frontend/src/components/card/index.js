import React, { Component } from 'react';
import classnames from 'classnames';
import autobind from 'autobind';
import styles from './styles.module.css';


@autobind
class Card extends Component {
    render() {
        const { children, title, noPaddingTop } = this.props;
        return (
            <div className={styles.root}>
                { title && <div className={styles.title}>{title}</div> }
                <div className={classnames(styles.content, {
                    [styles.noPaddingTop]: noPaddingTop })}>
                    {children}
                </div>
            </div>
        )
    }
}

export default Card;