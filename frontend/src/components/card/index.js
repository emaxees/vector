import React, { Component } from 'react';
import classnames from 'classnames';
import styles from './styles.module.css';


class Card extends Component {
    render() {
        const { children, title, noPaddingTop } = this.props;
        return (
            <div className={styles.root}>
                { title && <div className={styles.title}>{title}</div> }
                <div className={classnames(styles.content, {
                    [styles['no-padding-top']]: noPaddingTop })}>
                    {children}
                </div>
            </div>
        )
    }
}

export default Card;