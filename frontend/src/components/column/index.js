import React, { Component } from 'react';
import classnames from 'classnames';
import styles from './styles.module.css';


class Column extends Component {
    render() {
        const {
            children, shrink, basis, width,
        } = this.props;
        const stylesInline = {
            flexBasis: basis,
            width,
        };
        return (
            <div
                className={classnames(styles.root, { [styles.shrink]: shrink })}
                style={stylesInline}
            >
                { children }
            </div>
        );
    }
}

export default Column;
