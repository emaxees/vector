import React, { Component } from 'react';
import posed, { PoseGroup } from 'react-pose';
import { showPopup, hidePopup } from 'actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import autobind from 'autobind';
import ReactDOM from 'react-dom';
import styles from './styles.module.css';

const Container = posed.div({
    enter: {
        y: 0,
        opacity: 1,
        delay: 0,
        transition: {
            y: { type: 'spring', stiffness: 1000, damping: 15 },
            default: { duration: 100 },
        },
    },
    exit: {
        y: 50,
        opacity: 0,
        transition: { duration: 150 },
    },
});

const Shade = posed.div({
    enter: { opacity: 1 },
    exit: { opacity: 0 },
});

@connect(
    state => ({
        isPopupVisible: state.app.isPopupVisible,
        popupContent: state.app.popupContent,
    }),
    dispatch => bindActionCreators({ showPopup, hidePopup }, dispatch)
)
@autobind
class Popup extends Component {
    handleClickClose() {
        const { hidePopup } = this.props;
        hidePopup();
    }

    getChildren() {
        const { isPopupVisible, popupContent } = this.props;
        return (
            <PoseGroup>
                {isPopupVisible && [
                    <Shade key="shade" className={styles.shade} />,
                    <Container className={styles.container} key="container">
                        <div onClick={this.handleClickClose} className={styles.header}>
                            <i className="material-icons">
                                close
                            </i>
                        </div>
                        {popupContent}
                    </Container>,
                ]}
            </PoseGroup>
        );
    }

    render() {
        const popup = document.getElementById('popup');
        const { isPopupVisible } = this.props;
        if (!popup) return null;
        return (
            ReactDOM.createPortal(
                <div
                    className={styles.root}
                    style={{ height: isPopupVisible ? '100%' : 0 }}
                >
                    {this.getChildren()}
                </div>,
                popup,
            )
        );
    }
}

export default Popup;
