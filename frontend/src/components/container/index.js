import React, { Component } from 'react';
import classnames from 'classnames';
import styles from './styles.module.css';


class Container extends Component {
    render() {
        const { children, noMarginBottom, noMarginTop, ...rest } = this.props;
        return (
            <div
                {...rest}
                className={
                classnames(
                    styles.root,
                    rest.className,
                    {
                        [styles['no-margin-bottom']]: noMarginBottom,
                        [styles['no-margin-top']]: noMarginTop
                    },
                )}
            >
                {children}
            </div>
        );
    }
}

export default Container;
