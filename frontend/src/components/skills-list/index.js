import React, { Component } from 'react';
import autobind from 'autobind';
import { getSkillLevel } from 'utils';
import styles from './styles.module.css';


@autobind
class SkillsList extends Component {

    render() {
        const { skills } = this.props;
        return (
            <div className={styles.root}>
                {skills.map(skill => (
                    <div key={skill.id}>
                        <span>{skill.name}</span>
                        <b>{getSkillLevel(skill.points)}</b>
                    </div>
                ))}
            </div>
        );
    }
}

export default SkillsList;
