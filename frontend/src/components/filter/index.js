import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Checkbox } from 'components';
import styles from './styles.module.css';


class Filter extends Component {
    static propTypes = {
        filters: PropTypes.arrayOf(PropTypes.shape({
            name: PropTypes.string,
            id: PropTypes.number,
            slug: PropTypes.string,
            open: PropTypes.bool,
        })).isRequired,
        selectedFilters: PropTypes.arrayOf(PropTypes.number).isRequired,
    };

    state = {
        filtersOpened: {},
    }

    handleSubFilterClick = (filter) => {
        return () => {
            const { filtersOpened } = this.state;
            if (filtersOpened[filter.id] === undefined && filter.open) {
                filtersOpened[filter.id] = false;
            } else filtersOpened[filter.id] = !filtersOpened[filter.id];
            this.setState({ filtersOpened });
        };
    }

    handleChangeCheckbox = ($event) => {
        const { value } = $event.target;
        const { onChangeFilter } = this.props;
        onChangeFilter(Number(value));
    }

    renderSubFilters = (filter) => {
        const { onChangeFilter, selectedFilters } = this.props;
        const { filtersOpened } = this.state;
        if ((filtersOpened[filter.id] === undefined && filter.open) || filtersOpened[filter.id]) {
            return (
                <Filter
                    onChangeFilter={onChangeFilter}
                    selectedFilters={selectedFilters}
                    filters={filter.child}
                    subfilter
                />
            );
        }
    }

    renderFilters = () => {
        const { filters } = this.props;
        return filters.map(filter => (
            <li
                key={filter.id}
                className={
                    classnames({ [styles.haveSubFilters]: filter.child && filter.child.length })
                }
            >
                { this.renderFilter(filter) }
            </li>
        ));
    }

    renderFilter = (filter) => {
        const { selectedFilters } = this.props;
        const haveSubFilters = (filter.child && filter.child.length);
        if (haveSubFilters) {
            return (
                <Fragment>
                    <span
                        onClick={this.handleSubFilterClick(filter)}
                    >
                        <span>{ filter.name }</span>
                        { this.renderArrow(filter) }
                    </span>
                    { this.renderSubFilters(filter) }
                </Fragment>
            );
        }
        return (
            <Checkbox
                onChange={this.handleChangeCheckbox}
                label={filter.name}
                value={filter.id}
                checked={selectedFilters.includes(filter.id)}
            />
        );
    }

    renderArrow = (filter) => {
        const { filtersOpened } = this.state;
        if (filter.child && filtersOpened[filter.id]) return <i className="material-icons">arrow_drop_up</i>;
        if (filter.child && !filtersOpened[filter.id]) return <i className="material-icons">arrow_drop_down</i>;
    }

    renderTitle = () => {
        const { title } = this.props;
        if (title) return <h2>{ title }</h2>;
    }

    render() {
        const { subfilter } = this.props;
        return (
            <div className={styles.root}>
                { this.renderTitle() }
                <ul className={classnames({ [styles.subFilter]: subfilter })}>
                    { this.renderFilters() }
                </ul>
            </div>
        );
    }
}

export default Filter;
