import React, { Component } from 'react';
import Avatar from 'react-avatar';
import { NavLink } from 'react-router-dom'
import { withRouter } from 'react-router'
import autobind from 'autobind';
import { Card, SkillsList } from 'components';
import styles from './styles.module.css';


@autobind
@withRouter
class VolunteerPreview extends Component {
    render() {
        const { volunteer } = this.props;
        return (
            <Card>
                <NavLink className={styles.root} to={`/profile/${volunteer.id}`}>
                    <div className={styles.container}>
                        <div className={styles.left}>
                            <Avatar size='60px' round name={`${volunteer.firstName}`} />
                        </div>
                        <div className={styles.right} >
                            <div className={styles.header}>
                                <div>{volunteer.firstName} {volunteer.lastName}</div>
                                <div>{volunteer.ocupation}</div>
                            </div>
                            <div className={styles.info}>
                                <div dangerouslySetInnerHTML={{__html: volunteer.bio }} />
                            </div>
                        </div>
                    </div>
                    <SkillsList skills={volunteer.skills} />
                </NavLink>
            </Card>
        )
    }
}

export default VolunteerPreview;