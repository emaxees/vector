import React, { Component } from 'react';
import Avatar from 'react-avatar';
import { NavLink } from 'react-router-dom'
import { withRouter } from 'react-router'
import { Card, SkillsList } from 'components';
import styles from './styles.module.css';


class VolunteerPreview extends Component {
    renderBio = () => {
        const { volunteer: { bio }, compact } = this.props;
        if (compact) return;
        return (
            <div className={styles.info}>
                <div dangerouslySetInnerHTML={{__html:bio }} />
            </div>
        );
    }

    renderSkillList = () => {
        const { volunteer: { skills }, compact } = this.props;
        if (compact) return;
        return <SkillsList skills={skills} />;
    }

    render() {
        const { volunteer: { firstName, lastName, id, profilePicture, ocupation } } = this.props;
        return (
            <Card>
                <NavLink className={styles.root} to={`/profile/volunteer/${id}`}>
                    <div className={styles.container}>
                        <div className={styles.left}>
                            <Avatar size='60px' round name={`${firstName}`} src={profilePicture}/>
                        </div>
                        <div className={styles.right} >
                            <div className={styles.header}>
                                <div>{firstName} {lastName}</div>
                                <div>{ocupation}</div>
                            </div>
                            {this.renderBio()}
                        </div>
                    </div>
                    {this.renderSkillList()}
                </NavLink>
            </Card>
        )
    }
}

export default withRouter(VolunteerPreview);
