import React, { Component } from 'react';
import spinner from './assets/spinner.svg';
import styles from './styles.module.css';

class Loader extends Component {
    render() {
        const { ...rest } = this.props;
        return (
            <div className={styles.root} {...rest}>
                <img src={spinner} alt='spinner' />
            </div>
        )
    }
}

export default Loader;
