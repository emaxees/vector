import React, { Component } from 'react';
import spinner from './assets/spinner.svg';
import styles from './styles.module.css';

class Loader extends Component {
    render() {
        return (
            <div className={styles.root}>
                <img src={spinner} alt='spinner' />
            </div>
        )
    }
}

export default Loader;
