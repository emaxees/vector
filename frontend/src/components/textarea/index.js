import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './styles.module.css';


class Textarea extends Component {
    static propTypes = {
        value: PropTypes.string,
        type: PropTypes.string,
        rows: PropTypes.string,
        error: PropTypes.bool,
        placeholder: PropTypes.string,
        onChange: PropTypes.func,
        onBlur: PropTypes.func,
    };

    handleChange = ($event) => {
        const { value } = $event.target;
        const { onChange } = this.props;
        onChange(value);
    }

    render() {
        const {
            error, label, value, rows,  placeholder, onBlur, onFocus,
        } = this.props;

        return (
            <div
                className={
                    classnames(styles.root, { [styles.error]: error })
                }
            >
                { label && <div className={styles.label}>{label}</div> }
                <div className={styles.wrapper}>
                    <textarea
                        value={value}
                        rows={rows}
                        placeholder={placeholder}
                        onChange={this.handleChange}
                        onBlur={onBlur}
                        onFocus={onFocus}
                    />
                </div>
            </div>
        );
    }
}
export default Textarea;
