import React, { Component } from 'react';
import classnames from 'classnames';
import { withRouter } from 'react-router'
import PropTypes from 'prop-types';
import styles from './styles.module.css';


class Navbar extends Component {
    static propTypes = {
        text: PropTypes.string,
        right: PropTypes.node,
    };

    handleClick = () => {
        const { history: { goBack } } = this.props;
        goBack();
    }

    render() {
        const { text, right, shadow } = this.props;
        return (
            <div className={classnames(styles.root, {[styles.shadow]: shadow})}>
                <div className={styles.left}>
                    <i onClick={this.handleClick} className='material-icons'>
                        arrow_back_ios
                    </i>
                </div>
                <div className={styles.center}>
                    {text}
                </div>
                <div className={styles.right}>
                    { right }
                </div>
            </div>
        );
    }
}

export default withRouter(Navbar);
