import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router'
import { NavLink } from 'react-router-dom';
import styles from './styles.module.css';


class Tabs extends Component {
    getAmmountOfNotifications = () => {
        const { notifications } = this.props;
        return notifications.filter(notification => notification.unread).length;
    }

    renderCount = () => {
        const count = this.getAmmountOfNotifications();
        if (count > 0) return <div className={styles.count}>{count}</div>;
    }

    render() {
        const { match, user } = this.props;
        return (
            <div className={styles.root}>
               <NavLink activeClassName={styles['is-active']} to={`${match.path}/dashboard`}>
                    <i className='material-icons'>
                        dehaze
                    </i>
                    <span>Dashboard</span>
                </NavLink>
                <NavLink activeClassName={styles['is-active']} to={`${match.path}/chat`}>
                    <i className='material-icons'>
                        chat
                    </i>
                    <span>Chat</span>
                </NavLink>
                <NavLink activeClassName={styles['is-active']} to={`${match.path}/notifications`}>
                    {this.renderCount()}
                    <i className='material-icons'>
                        notifications
                    </i>
                    <span>Notifications</span>
                </NavLink>
                <NavLink activeClassName={styles['is-active']} to={`${match.path}/profile/${user.id}`}>
                    <i className='material-icons'>
                        person
                    </i>
                    <span>Profile</span>
                </NavLink>
                <NavLink activeClassName={styles['is-active']} to={`${match.path}/settings`}>
                    <i className='material-icons'>
                        settings
                    </i>
                    <span>Settings</span>
                </NavLink>
            </div>
        );
    }
}

export default withRouter(
    connect(
        state => ({
            notifications: state.notifications.list,
        }),
        dispatch => bindActionCreators({}, dispatch)
    )(Tabs)
);
