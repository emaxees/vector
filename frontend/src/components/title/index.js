import React, { Component } from 'react';
import autobind from 'autobind';
import styles from './styles.module.css';

@autobind
class Title extends Component {
    render() {
        const { children } = this.props;

        return (
            <div className={styles.root}>
                {children}
            </div>
        );
    }
}

export default Title;
