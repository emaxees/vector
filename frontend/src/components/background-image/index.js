import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './styles.module.css';
import preloadImageLight from './assets/preload-image-light.png';
import preloadImageDark from './assets/preload-image-dark.png';


class BackgroundImage extends Component {
    static propTypes = {
        src: PropTypes.string,
    };

    render() {
        const { src, theme } = this.props;
        const stylesInline = {};
        if (theme === 'dark') {
            stylesInline.backgroundImage = `url(${src}), url(${preloadImageDark}) `;
            stylesInline.backgroundColor = '#111111';
        } else {
            stylesInline.backgroundImage = `url(${src}), url(${preloadImageLight}) `;
            stylesInline.backgroundColor = '#ebebeb';
        }

        return (
            <div className={styles.root} style={stylesInline} />
        );
    }
}

export default BackgroundImage;
