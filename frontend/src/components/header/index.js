import React, { Component, Fragment } from 'react';
import { withRouter } from 'react-router'
import classnames from 'classnames';
import autobind from 'autobind';
import Avatar from 'react-avatar';
import styles from './styles.module.css';


@withRouter
@autobind
class Header extends Component {

    handleClickAction(action) {
        const { history: { goBack } } = this.props;
        return () => {
            switch(action) {
                case 'close': {
                    goBack();
                    break;
                }
                default: {
                    break;
                }
            }
        }
    }

    renderFull() {
        const { user } = this.props;
        return (
            <Fragment>
                <div className={styles.actions}>
                    <i className='material-icons' onClick={this.handleClickAction('close')}>
                        close
                    </i>
                </div>
                <div className={styles.info}>
                    <Avatar
                        size='7rem'
                        round
                        name={user.type === 'volunteer' ? `${user.firstName} ${user.lastName}` : user.companyName }
                        color={Avatar.getRandomColor('sitebase', ['red', 'orange'])}
                    />
                    <div>{`${user.firstName} ${user.lastName}`}</div>
                    <div>{user.occupation}</div>
                </div>
                <div className={styles.bio} dangerouslySetInnerHTML={{__html: `${user.bio.slice(0,150)}...`}} />
            </Fragment>
        );
    }

    renderCompact() {
        const { user } = this.props;
        return (
            <Fragment>
                <div>
                    { user.type === 'volunteer' ? `${user.firstName} ${user.lastName}` : user.companyName }
                </div>
                <div>
                    <Avatar
                        round
                        name={user.type === 'volunteer' ? `${user.firstName} ${user.lastName}` : user.companyName }
                        color={Avatar.getRandomColor('sitebase', ['red', 'orange'])}
                    />
                </div>
            </Fragment>
        );
    }

    render() {
        const { full } = this.props;
        return (
            <div className={classnames(styles.root, {[styles.full]: full})}>
               { full ? this.renderFull(): this.renderCompact() }
            </div>
        );
    }
}

export default Header;
