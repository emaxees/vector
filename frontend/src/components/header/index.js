import React, { Component } from 'react';
import { withRouter } from 'react-router'
import { Loader } from 'components';
import classnames from 'classnames';
import Avatar from 'react-avatar';
import styles from './styles.module.css';


class Header extends Component {
    handleClickAction = (action) => {
        const { history: { goBack } } = this.props;
        return () => {
            switch(action) {
                case 'close': {
                    goBack();
                    break;
                }
                default: {
                    break;
                }
            }
        }
    }

    getInfo = () => {
        const { user } = this.props;
        return (
            <div className={styles.info}>
                <Avatar
                    size='7rem'
                    round
                    src={user.profilePicture}
                    name={user.type === 'volunteer' ? `${user.firstName} ${user.lastName}` : user.companyName }
                    color={Avatar.getRandomColor('sitebase', ['red', 'orange'])}
                />
                <div>{user.displayName}</div>
                <div>{user.occupation}</div>
            </div>
        )
    }

    renderFull = () => {
        return (
            <>
                <div className={styles.actions}>
                    <i className='material-icons' onClick={this.handleClickAction('close')}>
                        close
                    </i>
                </div>
                {this.getInfo()}
            </>
        );
    }

    renderCompact = () => {
        const { user } = this.props;
        return (
            <>
                <div>
                    {user.displayName}
                </div>
                <div>
                    <Avatar
                        className={styles.avatar}
                        src={user.profilePicture}
                        size='40px'
                        round
                        name={user.displayName}
                        color={Avatar.getRandomColor('sitebase', ['red', 'orange'])}
                    />
                </div>
            </>
        );
    }

    getContent = () => {
        const { isLoading, full } = this.props;
        if (isLoading) return <Loader />;
        if (full) return this.renderFull();
        return this.renderCompact()
    }

    render() {
        const { full } = this.props;
        return (
            <div className={classnames(styles.root, {[styles.full]: full})}>
               {this.getContent()}
            </div>
        );
    }
}

export default withRouter(Header);
