import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './styles.module.css';


class Input extends Component {
    static propTypes = {
        value: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number
        ]),
        type: PropTypes.string,
        error: PropTypes.bool,
        placeholder: PropTypes.string,
        onChange: PropTypes.func,
        onBlur: PropTypes.func,
    };

    state = {
        showPassword: false,
    }

    getShowPassword = () => {
        const { showPassword } = this.state;
        return <i className="material-icons" onClick={this.handleClickEye}>{ showPassword ? 'visibility_off' : 'visibility'}</i>
    }

    getType = (type) => {
        const { showPassword } = this.state;
        if (type !== 'password') return type;
        if (showPassword) return 'text';
        return 'password';
    }

    handleClickEye = () => {
        const { showPassword } = this.state;
        this.setState({ showPassword: !showPassword });
    }

    handleChange = ($event) => {
        const { value } = $event.target;
        const { onChange } = this.props;
        onChange(value);
    }

    render() {
        const {
            error, label, value, type, placeholder, onBlur, onFocus, min, max
        } = this.props;

        return (
            <div
                className={
                    classnames(styles.root, { [styles.error]: error })
                }
            >
                { label && <div className={styles.label}>{label}</div> }
                <div className={styles.wrapper}>
                    <input
                        value={value}
                        type={this.getType(type)}
                        min={min}
                        max={max}
                        placeholder={placeholder}
                        onChange={this.handleChange}
                        onBlur={onBlur}
                        onFocus={onFocus}
                    />
                    {type === 'password' && this.getShowPassword()}
                </div>
            </div>
        );
    }
}
export default Input;
