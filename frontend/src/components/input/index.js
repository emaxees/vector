import React, { Component } from 'react';
import autobind from 'autobind';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import styles from './styles.module.css';

@autobind
class Input extends Component {
    static propTypes = {
        value: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number
        ]),
        type: PropTypes.string,
        error: PropTypes.bool,
        placeholder: PropTypes.string,
        onChange: PropTypes.func,
        onBlur: PropTypes.func,
    };

    handleChange($event) {
        const { value } = $event.target;
        const { onChange } = this.props;
        onChange(value);
    }

    render() {
        const {
            error, label, value, type, placeholder, onBlur, onFocus, min, max
        } = this.props;

        return (
            <div
                className={
                    classnames(styles.root, { [styles.error]: error })
                }
            >
                { label && <div className={styles.label}>{label}</div> }
                <div className={styles.wrapper}>
                    <input
                        value={value}
                        type={type}
                        min={min}
                        max={max}
                        placeholder={placeholder}
                        onChange={this.handleChange}
                        onBlur={onBlur}
                        onFocus={onFocus}
                    />
                </div>
            </div>
        );
    }
}
export default Input;
