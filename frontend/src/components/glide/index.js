import React, { Component } from 'react';
import autobind from 'autobind';
import PropTypes from 'prop-types';
import uuidv4 from 'uuid/v4';
import './styles.module.css';


@autobind
class Glide extends Component {
    static propTypes = {
        options: PropTypes.object,
        arrowLeft: PropTypes.node,
        arrowRight: PropTypes.node,
        onChange: PropTypes.func,
        children: PropTypes.node.isRequired,
    };

    componentDidMount() {
        const { options, onChange } = this.props;
        const Glide = require('@glidejs/glide').default;
        const instance = new Glide('.glide', options)
            .mount()
            .on('move', () => {
                if (onChange) onChange(instance.index);
            })
    }

    getButtonLeft() {
        const { arrowLeft } = this.props;
        if (arrowLeft) return arrowLeft;
        return 'PREV';
    }

    getButtonRight() {
        const { arrowRight } = this.props;
        if (arrowRight) return arrowRight;
        return 'NEXT';
    }

    getArrows() {
        const { arrowLeft, arrowRight } = this.props;
        if (!(arrowLeft && arrowRight)) return;
        return (
            <div className='glide__arrows' data-glide-el='controls'>
                <button className='glide__arrow glide__arrow--left' data-glide-dir='<'>{this.getButtonLeft()}</button>
                <button className='glide__arrow glide__arrow--right' data-glide-dir='>'>{this.getButtonRight()}</button>
            </div>
        )
    }

    render() {
        const { children } = this.props;
        return (
            <div className='glide'>
                <div className='glide__track' data-glide-el='track'>
                    <ul className='glide__slides'>
                        { children.map((child, index) => <li key={index} className='glide__slide'>{child}</li>) }
                    </ul>
                </div>
                {this.getArrows()}
                <div className='glide__bullets' data-glide-el='controls[nav]'>
                    { [...Array(children.length).keys()].map(key => <button key={uuidv4()} className='glide__bullet' data-glide-dir={`=${key}`} />) }
                </div>
            </div>
        );
    }
}
export default Glide;
