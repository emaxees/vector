import React, { Component } from 'react';
import { Input, Checkbox, Select } from 'components';
import PropTypes from 'prop-types';
import { between } from 'core/utils';
import styles from './styles.module.css';


class Selector extends Component {
    static propTypes = {
        value: PropTypes.string,
        type: PropTypes.string,
        placeholder: PropTypes.string,
        onAdd: PropTypes.func,
        onRemove: PropTypes.func,
        onBlur: PropTypes.func,
        list: PropTypes.arrayOf(PropTypes.object),
        selected: PropTypes.arrayOf(PropTypes.object),
    };

    state = {
        value: '',
    }

    handleChangeInput = (value) => {
        this.setState({ value });
    }

    handleChangeSelect = (skill) => {
        return (value) => {
            const { onAdd, onRemove } = this.props;
            onRemove(skill)
            onAdd({ skill, points: Number(value) });
        }
    }

    handleCheckboxChange = ($event) => {
        const { currentTarget: { value, checked } } = $event;
        const { onAdd, onRemove } = this.props;
        if (checked) onAdd({skill: Number(value), points: 0});
        else onRemove(Number(value));
    }

    getSelectValue = (points) => {
        if (between(points, 0, 500)) return '0';
        if (between(points, 500, 999)) return '999';
        return '1000';
    }

    renderSelect = (item) => {
        const { selected, hideSelect } =  this.props;
        if (hideSelect) return;
        if (!selected.map(item => Number(item.skill)).includes(item.id)) return;
        const { points, skill } = selected.find(ele => ele.skill === item.id);
        return (
            <div className={styles.slider}>
                <Select
                    options={[
                        { value:'0', label: 'Beginner' },
                        { value:'999', label: 'Intermediate' },
                        { value:'1000', label: 'Advanced' }
                    ]}
                    onChange={this.handleChangeSelect(skill)}
                    value={this.getSelectValue(points)}
                />
            </div>
        )
    }

    render() {
        const { label, placeholder, selected, list } = this.props;
        const { value } = this.state;
        return (
            <div className={styles.root}>
                <div className={styles.wrapper}>
                    <Input
                        label={label}
                        type='text'
                        onChange={this.handleChangeInput}
                        placeholder={placeholder}
                    />
                    <i className='material-icons'>search</i>
                </div>
                <ul className={styles.container}>
                    {list.filter(item => item.name.toLowerCase().includes(value.toLowerCase())).map(item => (
                        <li key={item.id}>
                            <Checkbox
                                value={item.id}
                                id={item.id}
                                label={item.name}
                                onChange={this.handleCheckboxChange}
                                checked={selected.map(item => Number(item.skill)).includes(item.id)}
                            />
                            {this.renderSelect(item)}
                        </li>
                    ))}
                </ul>
            </div>
        );
    }
}

export default Selector;
