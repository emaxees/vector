class Api {
    signUpVolunteer = (data) => {
        return this.fetchBase('volunteers/',
            {
                method: 'POST',
                body: data
            }
        )
    }

    signUpEnterprise = (data) => {
        return this.fetchBase('enterprises/',
            {
                method: 'POST',
                body: data
            }
        )
    }

    getAuth = ({email, password}) => {
        return this.fetchBase('auth/token/',
            {
                method: 'POST',
                body: { email, password }
            }
        )
    }

    forgetPassword = ({email}) => {
        return this.fetchBase('auth/password-reset/',
            {
                method: 'POST',
                body: { email }
            }
        )
    }

    resetPasswordConfirm = ({password, token}) => {
        return this.fetchBase(`auth/password-reset/confirm/`,
        {
            method: 'POST',
            body: { password, token }
        }
    )
    }

    validateToken = ({token}) => {
        return this.fetchBase(`auth/password-reset/validate_token/`,
        {
            method: 'POST',
            body: { token }
        }
    )
    }

    updatePassword = ({oldPassword, newPassword}) => {
        return this.fetchBase(`users/`,
        {
            method: 'POST',
            body: { oldPassword, newPassword }
        }
    )
    }

    getInterests = () => {
        return this.fetchBase('interests/', { method: 'GET' });
    }

    getSkills = () => {
        return this.fetchBase('skills/', { method: 'GET' });
    }

    updateSkills = ({skills, id}) => {
        return this.fetchBase(`volunteers/${id}/`,
            {
                method: 'PATCH',
                body: { skills }
            }
        );
    }

    updateInterests = ({interests, id}) => {
        return this.fetchBase(`enterprises/${id}/`,
            {
                method: 'PATCH',
                body: { interests }
            }
        );
    }

    updateVolunteer = ({data, id}) => {
        return this.fetchMultipartBase(`volunteers/${id}/`,
            {
                method: 'PATCH',
                body: data
            }
        );
    }

    updateEnterprise = ({data, id}) => {
        return this.fetchMultipartBase(`enterprises/${id}/`,
            {
                method: 'PATCH',
                body: data,
            }
        );
    }

    getCurrentUser = () => {
        return this.fetchBase('users/me/', { method: 'get' });
    }

    getUser = (id) => {
        return this.fetchBase(`users/${id}/`, { method: 'get' });
    }

    getVolunteers = (page=1) => {
        return this.fetchBase(`volunteers/?page=${page}`, { method: 'get' });
    }

    getVolunteer = (id) => {
        return this.fetchBase(`volunteers/${id}/`, { method: 'get' });
    }

    getEnterprise = (id) => {
        return this.fetchBase(`enterprises/${id}/`, { method: 'get' });
    }

    getJob = (id) => {
        return this.fetchBase(`jobs/${id}/`, { method: 'get' });
    }

    getJobs = () => {
        return this.fetchBase(`jobs/`, { method: 'get' });
    }

    postJob = (job) => {
        return this.fetchMultipartBase(`jobs/`,
            {
                method: 'POST',
                body: job
            }
        );
    }

    updateJob = ({id, form}) => {
        return this.fetchMultipartBase(`jobs/${id}/`,
            {
                method: 'PATCH',
                body: form
            }
        );
    }

    updateJobBase = ({id, job}) => {
        return this.fetchBase(`jobs/${id}/`,
            {
                method: 'PATCH',
                body: job
            }
        );
    }

    applyJob = (id) => {
        return this.fetchBase(`jobs/${id}/`, { method: 'PUT' });
    }

    getChats = () => {
        return this.fetchBase(`chats/`, { method: 'get' });
    }

    getReviews = (id) => {
        return this.fetchBase(`reviews/${id}/`, { method: 'get' });
    }

    postReview = (review) => {
        return this.fetchBase(`reviews/`,
            {
                method: 'POST',
                body: { ...review }
            }
        );
    }

    getNotifications = () => {
        return this.fetchBase('notifications/', { method: 'GET' });
    }

    markNotificationAsRead = (id) => {
        return this.fetchBase(`notifications/${id}/`,
            {
                method: 'PUT',
            }
        );
    }

    postFeedback = (feedback) => {
        return this.fetchBase(`feedback/`,
            {
                method: 'post',
                body: feedback
            }
        );
    }

    deleteJob = (id) => {
        return this.fetchBase(`jobs/${id}/`, { method: 'DELETE' });
    }

    fetchMultipartBase = async(resource, options = {}) => {
        const apiUrl = process.env.REACT_APP_API_URL;
        const url = `${apiUrl}/${resource}`;
        const headers = new Headers();
        const token = localStorage.getItem('token');
        if (token) headers.set('Authorization', `Bearer ${token}`)
        const response = await fetch(url, {
            method: options.method,
            body: options.body,
            headers,
        });
        const data = await response.json();
        if (response.status >= 200 && response.status < 300) return data;
        throw new Error(JSON.stringify(data));
    }

    fetchBase = async(resource, options = {}) => {
        const apiUrl = process.env.REACT_APP_API_URL;
        const url = `${apiUrl}/${resource}`;
        const headers = new Headers({
            'Content-Type': 'application/json; charset=UTF-8',
        });
        const token = localStorage.getItem('token');
        if (token) headers.set('Authorization', `Bearer ${token}`)
        const response = await fetch(url, {
            method: options.method,
            body: JSON.stringify(options.body),
            headers,
        });
        const data = await response.json();
        if (response.status >= 200 && response.status < 300) return data;
        throw data;
    }
}

export default Api;
