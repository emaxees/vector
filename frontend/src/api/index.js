import autobind from 'autobind';


@autobind
class Api {
    signUpVolunteer({ firstName, lastName, email, password}) {
        return this.fetchBase('volunteers/',
            {
                method: 'POST',
                body: { email, password, firstName, lastName }
            }
        )
    }

    signUpEnterprise({ companyName, administratorName, email, password}) {
        return this.fetchBase('enterprises/',
            {
                method: 'POST',
                body: { email, password, companyName, administratorName }
            }
        )
    }

    getAuth({ email, password }) {
        return this.fetchBase('auth/token/',
            {
                method: 'POST',
                body: { email, password }
            }
        )
    }

    updatePassword( { oldPassword, newPassword }) {
        return this.fetchBase(`users/`,
        {
            method: 'POST',
            body: { oldPassword, newPassword }
        }
    )
    }

    getInterests() {
        return this.fetchBase('interests/', { method: 'GET' });
    }

    getSkills() {
        return this.fetchBase('skills/', { method: 'GET' });
    }

    updateSkills({ skills, id }) {
        return this.fetchBase(`volunteers/${id}/`,
            {
                method: 'PATCH',
                body: { skills }
            }
        );
    }

    updateInterests({ interests, id }) {
        return this.fetchBase(`enterprises/${id}/`,
            {
                method: 'PATCH',
                body: { interests }
            }
        );
    }

    updateVolunteer({ data, id }) {
        return this.fetchBase(`volunteers/${id}/`,
            {
                method: 'PATCH',
                body: { ...data }
            }
        );
    }

    updateEnterprise({ data, id }) {
        return this.fetchBase(`enterprises/${id}/`,
            {
                method: 'PATCH',
                body: { ...data }
            }
        );
    }

    getUser(id) {
        return this.fetchBase(`users/${id}/`, { method: 'get' });
    }

    getVolunteers() {
        return this.fetchBase(`volunteers/`, { method: 'get' });
    }

    getJob(id) {
        return this.fetchBase(`jobs/${id}/`, { method: 'get' });
    }

    getJobs() {
        return this.fetchBase(`jobs/`, { method: 'get' });
    }

    postJob(job) {
        return this.fetchBase(`jobs/`,
            {
                method: 'POST',
                body: { ...job }
            }
        );
    }

    updateJob({id, job}) {
        return this.fetchBase(`jobs/${id}/`,
            {
                method: 'PATCH',
                body: { ...job }
            }
        );
    }

    applyJob(id) {
        return this.fetchBase(`jobs/${id}/`, { method: 'PUT' });
    }

    getChats() {
        return this.fetchBase(`chats/`, { method: 'get' });
    }

    getReviews(id) {
        return this.fetchBase(`reviews/${id}/`, { method: 'get' });
    }

    postReview(review) {
        return this.fetchBase(`reviews/`,
            {
                method: 'POST',
                body: { ...review }
            }
        );
    }

    getNotifications() {
        return this.fetchBase('notifications/', { method: 'GET' });
    }

    markNotificationAsRead(id) {
        return this.fetchBase(`notifications/${id}/`,
            {
                method: 'PUT',
            }
        );
    }

    postFeedback(feedback){
        return this.fetchBase(`feedback/`,
            {
                method: 'post',
                body: feedback
            }
        );
    }

    async fetchBase(resource, options = {}) {
        const apiUrl = process.env.REACT_APP_API_URL;
        const url = `${apiUrl}/${resource}`;
        const headers = new Headers({
            'Content-Type': 'application/json; charset=UTF-8',
        });
        const token = localStorage.getItem('token');
        if (token) headers.set('Authorization', `Bearer ${token}`)
        const response = await fetch(url, {
            method: options.method,
            body: JSON.stringify(options.body),
            headers,
        });
        const data = await response.json();
        if (response.status >= 200 && response.status < 300) return data;
        throw new Error(JSON.stringify(data));
    }
}

export default Api;
