import React, { Component } from 'react';
import { Card, Frame, Container, Navbar } from 'components';
import styles from './styles.module.css';


class PrivacyPolicy extends Component {
    render() {
        return (
            <Frame>
                <Navbar shadow text='Privacy Policy' />
                <Container>
                    <Card>
                    <article className={styles.root}>
                        <h3>Introduction</h3>
                        <p>Vector App Pte. Ltd. (“Vector”, “we” or “us”) takes the privacy of your information
                            seriously. This Privacy Policy applies to the vectorapp.info website (the “Website”) and
                            governs data collection, processing and usage in compliance with the Personal Data
                            Protection Act 2012 (No. 26 of 2012) of Singapore (“PDPA”). By using the Website, you
                            consent to the data practices described in this statement. Capitalized terms that are not
                            defined in this Privacy Policy have the meaning given to them in our Terms of Service.</p>
                        <h3>Information Collected from All Visitors to our Website</h3>
                        <p>We will obtain personal data about you when you visit us. When you visit us, we may monitor
                            the use of this Website through the use of cookies and similar tracking devices. For
                            example, we may monitor the number of times you visit our Website or which pages you go to.
                            This information helps us to build a profile of our users. Some of this data will be
                            aggregated or statistical, which means that we will not be able to identify you
                            individually.<br />This Privacy Policy applies to all visitors to our Website.</p>
                        <h3>Additional Personal Information that May be Collected</h3>
                        <p>Vector may collect and process:</p>
                        <ol>
                            <li>Personally identifiable information, such as:
                                <ul>
                                    <li>Your e-mail address and name, when you contact us;</li>
                                    <li>Details contained in the relevant document that you key in when you use our
                                        Services. These details may include your name, handphone number, email, the
                                        purpose of your query, and details about your will; (“Personal Information”)
                                    </li>
                                </ul>
                            </li>
                            <li>Information about your computer hardware and software when you use our Website. The
                                information can include: your IP address, browser type, domain names, access times and
                                referring website addresses. This information is used by Vector for the operation of the
                                Services, to maintain quality of the Services, and to provide general statistics
                                regarding use of the vectorapp.info Website.</li>
                        </ol>
                        <h3>Use of Personal Information</h3>
                        <p>Vector uses the collected information:</p>
                        <ol>
                            <li>To operate the Website and deliver the Services;</li>
                            <li>To process, and where necessary, respond to your application, enquiry or request;</li>
                            <li>To gather customer feedback;</li>
                            <li>To inform or update you of other products or services available from VAPL and its
                                affiliates, where you have consented to be contacted for such purposes;</li>
                            <li>To monitor, improve and administer the Website and Services, and to provide general
                                statistics regarding user of the Website;</li>
                            <li>To update you on changes to the Website and Services.</li>
                        </ol>
                        <h3>Non-disclosure</h3>
                        <p>Vector does not sell, rent, lease, or release your Personal Information to third parties.
                            Vector may, from time to time, contact you on behalf of external business partners about a
                            particular offering that may be of interest to you. In those cases, your unique Personal
                            Information is not transferred to the third party without your explicit consent. In
                            addition, Vector may share data with trusted partners to help us perform statistical
                            analysis, send you email or provide customer support. All such third parties are prohibited
                            from using your personal information except to provide these services to Vector, and they
                            are required to maintain the confidentiality of your Personal Information.</p>
                        <h3>Disclosure of Personal Information</h3>
                        <p>Vector will disclose or share your Personal Information, without notice, only if required to
                            do so by law or in the good faith belief that any such action is necessary to:</p>
                        <ol>
                            <li>Comply with any legal requirements or comply with legal process served on Vector or the
                                Website; </li>
                            <li>Protect and defend the rights or property of Vector; and </li>
                            <li>Act under exigent circumstances to protect the personal safety of users of
                                vectorapp.info, or the general public.</li>
                        </ol>
                        <p>We may disclose your personal information to third parties: </p>
                        <ol>
                            <li>In the event that we sell or buy any business or assets, in which case we may disclose
                                your personal data to the prospective seller or buyer of such business or assets; and
                            </li>
                            <li>If vectorapp.info or substantially all of its assets are acquired by a third party, in
                                which case personal data held by it about its customers will be one of the transferred
                                assets.</li>
                        </ol>
                        <h3>Use Of Cookies</h3>
                        <p>The Website uses “cookies” to help you personalize your online experience. A cookie is a text
                            file that is placed on your hard drive by a web page server. Cookies cannot be used to run
                            programs or deliver viruses to your computer. Cookies are uniquely assigned to you, and can
                            only be read by a web server in the domain that issued the cookie to you.</p>
                        <p>Cookies on the Website may be used to ensure a smooth user experience, perform analytics, and
                            for showing relevant advertisements. Please note that third parties (such as analytics
                            software) may also use cookies, over which we have no control. These cookies are likely to
                            be analytical/performance cookies or targeting cookies. The Website uses Google Analytics.
                            Please refer to <a href="http://www.google.com/policies/privacy/partners"
                                target="_blank" rel="noopener noreferrer">http://www.google.com/policies/privacy/partners</a> to find out more
                            about how Google uses data when you use our website and how to control the information sent
                            to Google.</p>
                        <p>Most Web browsers automatically accept cookies, but you can usually modify your browser
                            setting to decline cookies if you prefer. If you choose to decline cookies, you may not be
                            able to access all or parts of our Website or to fully experience the interactive features
                            of the Vector services or websites you visit.</p>
                        <h3>Security Of Your Personal Information</h3>
                        <p>We strive to maintain the safety of your Personal Information. Any payment transactions will
                            be encrypted using SSL technology. Unfortunately, no internet-based service is completely
                            secure. Although we will do our best to protect your personal data, we cannot guarantee the
                            security of your data transmitted to our site; any transmission is at your own risk. Once we
                            have received your information, we will use strict procedures and security features to try
                            to prevent unauthorised access.</p>
                        <h3>Access to, Updating, and Non-Use of Your Personal Information</h3>
                        <p>Subject to the exceptions referred to in section 21(2) of PDPA, you have the right to request
                            a copy of the information that we hold about you. If you would like a copy of some or all of
                            your personal information, please send an email to <a
                                href="mailto:hello@vectorapp.info">hello@vectorapp.info</a>. We may make a small charge
                            for this service.</p>
                        <p>We want to ensure that your Personal Information is accurate and up to date. If any of the
                            information that you have provided to Vector changes, for example if you change your email
                            address, name or contact number, please let us know the correct details by sending an email
                            to <a href="mailto:hello@vectorapp.info">hello@vectorapp.info</a>. You may ask us, or we may
                            ask you, to correct information you or we think is inaccurate, and you may also ask us to
                            remove information which is inaccurate.</p>
                        <p>You have the right to ask us not to collect, use, process, or disclose your Personal
                            Information in any of the manner described herein. We will usually inform you (before
                            collecting your Personal Information) if we intend to use your Personal Information for such
                            purposes or if we intend to disclose your Personal Information to any third party for such
                            purposes. You can give us notice of your intention to halt the collection, use, processing,
                            or disclosure of your Personal Information at any time by contacting us at <a
                                href="mailto:hello@vectorapp.info">hello@vectorapp.info</a>.</p>
                        <h3>Links to Other Websites</h3>
                        <p>Our Website may contain links to other websites. This Privacy Policy only applies to this
                            website so when you link to other websites you should read their own privacy policies.</p>
                        <h3>Changes To This Statement</h3>
                        <p>Vector will occasionally update this Privacy Policy to reflect customer feedback. Vector
                            encourages you to periodically review this Privacy Policy to be informed of how FWP is
                            protecting your information.</p>
                        <h3>Contact Information</h3>
                        <p>Vector App Pte. Ltd. welcomes your comments regarding this Privacy Policy. If you believe
                            that vectorapp.info has not adhered to this Privacy Policy, please contact us at <a
                                href="mailto:hello@vectorapp.info">hello@vectorapp.info</a>.</p>
                    </article>
                    </Card>
                </Container>
            </Frame>
        )
    }
}

export default PrivacyPolicy;