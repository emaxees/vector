import React, { Component } from 'react';
import { resetApp } from 'core/store/actions';
import {trackEvent} from 'core/tracking';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button } from 'components';
import { isAuthenticated } from 'core/utils';
import styles from './styles.module.css';


class Welcome extends Component {
    componentDidMount() {
        const { history:{ push }, resetApp } = this.props;
        if (isAuthenticated()) push('/home');
        else resetApp()
    }

    handleClickLogin = () => {
        const { history:{ push } } = this.props;
        push('/login');
    }

    handleClickSignup = () => {
        trackEvent('signup');
        const { history:{ push } } = this.props;
        push('/signup');
    }

    render() {
        return (
            <div className={styles.root}>
                <div className={styles.logo}/>
                <div className={styles.slogan}> Matching skills to social causes</div>
                <Button onClick={this.handleClickSignup} theme='primary'>
                    Get Started
                </Button>
                <div className={styles.link} onClick={this.handleClickLogin}> Got account? <span>Log in here</span></div>
            </div>
        )
    }
}

export default connect(
    null,
    dispatch => bindActionCreators({ resetApp }, dispatch)
)(Welcome);