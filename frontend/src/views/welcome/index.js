import React, { Component } from 'react';
import { Button } from 'components';
import autobind from 'autobind';
import { isAuthenticated } from 'utils';
import styles from './styles.module.css';


@autobind
class Welcome extends Component {
    componentDidMount() {
        const { history:{ push } } = this.props;
        if (isAuthenticated()) push('/home');
    }

    handleClickLogin() {
        const { history:{ push } } = this.props;
        push('/login');
    }

    handleClickSignup() {
        const { history:{ push } } = this.props;
        push('/signup');
    }

    render() {
        return (
            <div className={styles.root}>
                <div className={styles.logo}/>
                <Button onClick={this.handleClickSignup} theme='primary'>
                    Get Started
                </Button>
                <div className={styles.link} onClick={this.handleClickLogin}> Got account? <span>Log in here</span></div>
            </div>
        )
    }
}

export default Welcome;