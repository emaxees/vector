import React, { Component } from 'react';
import { Button, Title, Input, Navbar } from 'components';
import {trackEvent} from 'core/tracking';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { isValidEmail } from 'core/utils';
import { updateLoginForm, requestForgetPassword } from 'core/store/actions';
import styles from './styles.module.css';
import logo from './assets/logo.svg'


class ForgetPassword extends Component {
    state = {
        shouldValidate: {},
    }

    getButtonDisabled = () => {
        const { email } = this.props.login;
        return !(isValidEmail(email));
    }

    handleBlur = (key) => {
        const { shouldValidate } = this.state;
        return () => this.setState({ shouldValidate: { ...shouldValidate, [key]: true } });
    }

    handleFocus = () => {
        window.scroll(0, 0);
    }

    handleChange = (key) => {
        const { updateLoginForm } = this.props;
        return value => updateLoginForm(key, value);
    }

    handleSubmit = ($event) => {
        $event.preventDefault();
        const { requestForgetPassword } = this.props;
        const { email } = this.props.login;
        trackEvent('requestForgetPassword');
        requestForgetPassword({ email })
    }

    render() {
        const { login: { email, message } } = this.props;
        let content = (
            <>
                <Navbar />
                <div className={styles.container}>
                    <img className={styles.logo} alt='logo' src={logo} />
                    <Title>Enter your email to reset your password</Title>
                    <form onSubmit={this.handleSubmit}>
                        <Input
                            placeholder='Email'
                            type="text"
                            value={email}
                            onChange={this.handleChange('email')}
                            onBlur={this.handleBlur('email')}
                        />
                        <br />
                        <Button theme='primary' disabled={this.getButtonDisabled()} type='submit'>
                            Send
                        </Button>
                    </form>
                    { message &&  Object.keys(message).map(key => message[key].map(error => <p>{error}</p>))}

                </div>
            </>
        )
        return (
            <div className={styles.root}>
                {content}
            </div>
        )
    }
}

export default  connect(
    state => ({
        login: state.login,
    }),
    dispatch => bindActionCreators({ updateLoginForm, requestForgetPassword }, dispatch),
)(ForgetPassword);
