import React, { Component, Fragment } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import {trackEvent} from 'core/tracking';
import { bindActionCreators } from 'redux';
import {
    changeBrowseTab, requestVolunteers, requestJobs,
    applyJob, showPopup, setPopupContent, setJob,
    updateReviewForm, requestJob, deleteJob, requestCurrentUserProfile,
} from 'core/store/actions';
import {
    Card, Navbar, Button,
    SkillsList, Frame, Container,
    ReviewForm, Loader, Message,
} from 'components';
import classnames from 'classnames';
import { capitalize, formatDate } from 'core/utils';
import styles from './styles.module.css';


class JobDetail extends Component {
    componentDidMount(){
        const {
            match: { params: { id } },
            jobs,
            requestJob,
            currentUserFetched,
            requestCurrentUserProfile,
        } = this.props;
        if (!currentUserFetched) requestCurrentUserProfile();
        if (jobs.length || !jobs.find((job) => job.id)) requestJob(id);
    }

    getRight = () => {
        const { user: { type } } = this.props;
        if (type === 'enterprise') return <div onClick={this.handleClickRight}>Edit</div>
    }

    getJob = () => {
        const { match: { params: { id } }, jobs, current } = this.props;
        if (!jobs.length) return current;
        return jobs.find(job => job.id === Number(id));
    }

    hasReview = () => {
        const { user } = this.props;
        const { reviews } = this.getJob();
        return Boolean(
            reviews.find(review => {
                return Number(review.reviewer) === Number(user.id)
            })
        );
    }

    handleClickRight = () => {
        const {  match: { params: { id } }, history: { push } } = this.props;
        push(`/job/${id}/edit`)
    }

    handleClickApply = () => {
        const { id } = this.getJob();
        const { applyJob } = this.props;
        trackEvent('applyJob');
        applyJob(id);
    }

    handleClickApplicants = () => {
        const { match: { params: { id } }, history: { push }, user: { type } } = this.props;
        if (type === 'enterprise') push(`/job/${id}/applicants`);
    }

    handleClickWriteReview = () => {
        const { setPopupContent, showPopup, setJob, updateReviewForm, user } = this.props;
        const job = this.getJob();
        updateReviewForm('reviewer', user.id);
        updateReviewForm('user', job.user.id);
        setJob(this.getJob());
        setPopupContent(<ReviewForm />);
        showPopup();
    }

    handleClickAccept = () => {
        const { deleteJob } = this.props;
        const { id } = this.getJob();
        deleteJob(id);
    }

    getWarningMessage = () => {
        const props = {
            icon: 'notification_important',
            text: `Are you sure that you want to delete this job?, this action is permanently`,
        };
        return (
            <div>
                <Message {...props} />
                <Button onClick={this.handleClickAccept} theme='primary'>
                    <i className='material-icons'>thumb_up</i>
                </Button>
            </div>
        );
    }

    handleClickDelete = () => {
        const { showPopup, setPopupContent } = this.props;
        setPopupContent(this.getWarningMessage());
        showPopup();
    }

    isAlreadyApplied = () => {
        const job = this.getJob();
        const { user: { id } } = this.props;
        return Boolean(job.applicants.find(applicant => applicant.volunteer.id === Number(id)));
    }

    renderApplyJobButton = () => {
        const { user: { type } } = this.props;
        if (type === 'enterprise' || this.isAlreadyApplied()) return;
        return <Button onClick={this.handleClickApply} theme='primary'>Apply to job</Button>;
    }

    renderReviewButton = () => {
        const { user: { type } } = this.props;
        const { finished } = this.getJob();
        if (type === 'volunteer' && this.isAlreadyApplied() && !this.hasReview() && finished) return <Button onClick={this.handleClickWriteReview} theme='primary'>Write a Review</Button>;
    }

    renderFee = (fee) => {
        if (fee === 'bono') return `Pro bono`;
        return capitalize(fee);
    }

    renderApplicants = (job) => {
        const { user: { type } } = this.props;
        if (type === 'volunteer') return;
        return (
            <Card>
                <div onClick={this.handleClickApplicants} className={styles.applicants}>
                    <div>View applicants</div>
                    <div>{job.applicants.length}</div>
                </div>
            </Card>
        );
    }

    renderJob = (job) => {
        return (
            <Fragment>
                <Card noPaddingTop>
                    <div className={styles.image} style={{ backgroundImage: `url(${job.image}) ,url(http://www.btklsby.go.id/images/placeholder/camera.jpg)`}}/>
                        <div className={styles.job}>
                            <div className={styles.title}>
                                {job.title}
                                {job.inProgress && <i className='material-icons'>history</i>}
                            </div>
                            <div className={styles.subtitle}>
                                {job.user.companyName}
                            </div>
                            <div className={styles.stats}>
                                <div>
                                    <i className='material-icons'>access_time</i>
                                    <div>Commit hours</div>
                                    <span>{job.commitHours}</span>
                                </div>
                                <div>
                                    <i className='material-icons'>people</i>
                                    <div>Vacancy</div>
                                    <span>{job.vacancy}</span>
                                </div>
                                <div>
                                    <i className='material-icons'><i className='material-icons'>people</i></i>
                                    <div>Location</div>
                                    <span>{job.location}</span>
                                </div>
                            </div>
                            <div className={styles.skills}>
                                <SkillsList skills={job.requirements.map(requirement => ({id: requirement.skill.id, name: requirement.skill.name, points: requirement.points }))} />
                            </div>
                        </div>
                    </Card>
                    {this.renderApplicants(job)}
                    <Card title='Job Description'>
                        <div className={styles.description}>
                            <div dangerouslySetInnerHTML={{__html: job.description}} />
                        </div>
                    </Card>
                    <Card title='Created on'>
                        <div className={styles.content}>
                            {`${formatDate(job.date, 'D MMM YYYY')}`}
                        </div>
                    </Card>
                    <Card title='Schedule'>
                        <div className={styles.schedule}>
                            <div>
                                <i className='material-icons'>date_range</i>
                                <div>{ `${job.commitHours}` }</div>
                            </div>
                            <div>
                                <i className='material-icons'>location_on</i>
                                <div>{ job.location }</div>
                            </div>
                        </div>
                    </Card>
                    <Card title='Application Period'>
                        <div className={styles.content}>
                            {`Apply by ${formatDate(job.date, 'D MMM YYYY')}`}
                        </div>
                    </Card>
                    <Card title='Fee'>
                        <div className={styles.content}>
                            {this.renderFee(job.fee)}
                        </div>
                    </Card>
                    <Card title='Social Enterprise'>
                        <NavLink to={`/profile/enterprise/${job.user.id}`}>
                            <div className={styles['social-enterprise']}>
                                <div>{job.user.companyName} </div>
                                <i className="material-icons">
                                    arrow_forward_ios
                                </i>
                            </div>
                        </NavLink>
                    </Card>
            </Fragment>
        );
    }

    renderButton = (job) => {
        if (!job) return null;
        return (
            <div className={styles['button-container']}>
                {this.renderApplyJobButton()}
                {this.renderReviewButton()}
            </div>
        );
    }

    renderDeleteButton = () => {
        const { user: { type } } = this.props;
        if (type !== 'enterprise') return;
        return (
            <div className={classnames(styles['quit-left'], styles['button-container'], )} >
                <Button onClick={this.handleClickDelete} theme='circle'>
                    <i className='material-icons'>delete</i>
                </Button>
            </div>
        );
    }

    render() {
        const job = this.getJob();
        return (
            <Frame>
                <Navbar shadow text='Job Details' right={this.getRight()} />
                <Container>
                    { job ? this.renderJob(job) : <Loader /> }
                </Container>
                { this.renderButton(job) }
                { this.renderDeleteButton(job)}
            </Frame>
        )
    }
}

export default connect(
    state => ({
        user: state.profile.user,
        currentUserFetched: state.profile.user.fetched,
        jobs: state.jobs.list,
        current: state.jobs.current,
    }),
    dispatch => bindActionCreators({
        changeBrowseTab, requestVolunteers, requestJobs,
        applyJob, showPopup, setPopupContent, setJob,
        updateReviewForm, requestJob, deleteJob, requestCurrentUserProfile,
    }, dispatch)
)(JobDetail);
