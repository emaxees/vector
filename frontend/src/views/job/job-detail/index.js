import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
    changeBrowseTab, requestVolunteers, requestJobs,
    applyJob, showPopup, setPopupContent, setJob,
    updateReviewForm, requestJob
} from 'actions';
import autobind from 'autobind';
import {
    Card, Navbar, Button,
    SkillsList, Frame, Container,
    ReviewForm, Loader
} from 'components';
import { capitalize } from 'utils';
import styles from './styles.module.css';


@connect(
    state => ({
        user: state.user,
        jobs: state.jobs.list,
        current: state.jobs.current,
    }),
    dispatch => bindActionCreators({
        changeBrowseTab, requestVolunteers, requestJobs,
        applyJob, showPopup, setPopupContent, setJob,
        updateReviewForm, requestJob
    }, dispatch)
)
@autobind
class JobDetail extends Component {
    componentDidMount(){
        const { match: { params: { id } }, jobs, requestJob } = this.props;
        if (!jobs.length) requestJob(id);
    }

    getRight() {
        const { user: { type } } = this.props;
        if (type === 'enterprise') return <div onClick={this.handleClickRight}>Edit</div>
    }

    getJob() {
        const { match: { params: { id } }, jobs, current } = this.props;
        if (!jobs.length) return current;
        return jobs.find(job => job.id === Number(id));
    }

    hasReview() {
        const { user } = this.props;
        const { reviews } = this.getJob();
        return Boolean(
            reviews.find(review => {
                return Number(review.reviewer) === Number(user.id)
            })
        );
    }

    handleClickRight() {
        const {  match: { params: { id } }, history: { push } } = this.props;
        push(`/job/${id}/edit`)
    }

    handleClickApply() {
        const { id } = this.getJob();
        const { applyJob } = this.props;
        applyJob(id);
    }

    handleClickApplicants() {
        const { match: { params: { id } }, history: { push }, user: { type } } = this.props;
        if (type === 'enterprise') push(`/job/${id}/applicants`);
    }

    handleClickWriteReview() {
        const { setPopupContent, showPopup, setJob, updateReviewForm, user } = this.props;
        const job = this.getJob();
        updateReviewForm('reviewer', user.id);
        updateReviewForm('user', job.user.id);
        setJob(this.getJob());
        setPopupContent(<ReviewForm />);
        showPopup();
    }

    isAlreadyApplied() {
        const job = this.getJob();
        const { user: { id } } = this.props;
        return Boolean(job.applicants.find(applicant => applicant.volunteer.id === Number(id)));
    }

    renderApplyJobButton() {
        const { user: { type } } = this.props;
        if (type === 'enterprise' || this.isAlreadyApplied()) return;
        return <Button onClick={this.handleClickApply} theme='primary'>Apply to job</Button>;
    }

    renderReviewButton() {
        const { user: { type } } = this.props;
        const { finished } = this.getJob();
        if (type === 'volunteer' && this.isAlreadyApplied() && !this.hasReview() && finished) return <Button onClick={this.handleClickWriteReview} theme='primary'>Write a Review</Button>;
    }

    renderFee(fee) {
        if (fee === 'bono') return `Pro bono`;
        return capitalize(fee);
    }

    renderJob(job) {
        return (
            <Fragment>
                <Card noPaddingTop>
                        <div className={styles.image} style={{ backgroundImage: `url(${job.image}) ,url(http://www.btklsby.go.id/images/placeholder/camera.jpg)`}}/>
                        <div className={styles.job}>
                            <div className={styles.jobHeader}>
                                <div>{job.title}</div>
                            </div>
                            <div className={styles.stats}>
                                <div>
                                    <i className='material-icons'>access_time</i>
                                    <span>Commit hours</span>
                                    <span>{job.commitHours}</span>
                                </div>
                                <div>
                                    <i className='material-icons'>people</i>
                                    <span>Vacancy</span>
                                    <span>{job.vacancy}</span>
                                </div>
                                <div>
                                    <i className='material-icons'><i className='material-icons'>people</i></i>
                                    <span>Location</span>
                                    <span>{job.location}</span>
                                </div>
                            </div>
                            <div className={styles.skills}>
                                <SkillsList skills={job.requirements.map(requirement => ({id: requirement.skill.id, name: requirement.skill.name, points: requirement.points }))} />
                            </div>
                        </div>
                    </Card>
                    <Card>
                        <div onClick={this.handleClickApplicants} className={styles.applicants}>
                            <div>View applicants</div>
                            <div>{job.applicants.length}</div>
                        </div>
                    </Card>
                    <Card title='Job Description'>
                        <div className={styles.description}>
                            <div dangerouslySetInnerHTML={{__html: job.description}} />
                        </div>
                    </Card>
                    <Card title='Schedule'>
                        <div className={styles.schedule}>
                            <div>
                                <i className='material-icons'>date_range</i>
                                <div>{ `${job.date}` }</div>
                            </div>
                            <div>
                                <i className='material-icons'>location_on</i>
                                <div>{ job.location }</div>
                            </div>
                        </div>
                    </Card>
                    <Card title='Application Period'>
                        <div className={styles.content}>
                            Apply by 19 Sep 2018
                        </div>
                    </Card>
                    <Card title='Fee'>
                        <div className={styles.content}>
                            {this.renderFee(job.fee)}
                        </div>
                    </Card>
                    <Card title='Social Enterprise'>
                        <div className={styles['social-enterprise']}>
                            {/* <div className={styles.logo}>
                                logo
                            </div> */}
                            <div className={styles.info}>
                                {job.user.companyName}
                            </div>
                        </div>
                    </Card>
            </Fragment>
        );
    }

    renderButton(job) {
        if (!job) return null;
        return (
            <div className={styles['button-container']}>
                {this.renderApplyJobButton()}
                {this.renderReviewButton()}
            </div>
        );
    }

    render() {
        const job = this.getJob();
        return (
            <Frame>
                <Navbar shadow text='Job Details' right={this.getRight()} />
                <Container>
                    { job ? this.renderJob(job) : <Loader /> }
                </Container>
                { this.renderButton(job) }
            </Frame>
        )
    }
}

export default JobDetail;