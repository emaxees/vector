import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router';
import { bindActionCreators } from 'redux';
import { changeBrowseTab, requestVolunteers, requestJobs } from 'actions';
import { Frame, Container } from 'components';
import autobind from 'autobind';
import JobDetail from './job-detail';
import JobForm from './job-form';
import JobApplicants from './job-applicants';


@connect(
    state => ({
        user: state.user,
        currentTab: state.app.browseTab,
    }),
    dispatch => bindActionCreators({ changeBrowseTab, requestVolunteers, requestJobs }, dispatch)
)
@autobind
class Job extends Component {
    render() {
        const { match } = this.props;
        return (
            <Frame>
                <Container noMarginBottom noMarginTop>
                    <Switch>
                        <Route
                            path={`${match.path}/add`}
                            component={JobForm}
                        />
                        <Route
                            path={`${match.path}/:id/edit`}
                            component={JobForm}
                        />
                        <Route
                            path={`${match.path}/:id/applicants`}
                            component={JobApplicants}
                        />
                        <Route
                            path={`${match.path}/:id`}
                            component={JobDetail}
                        />
                    </Switch>
                </Container>
            </Frame>
        )
    }
}

export default Job;
