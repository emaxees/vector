import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
    changeBrowseTab, requestVolunteers, requestJobs, updateJobForm,
    postJob, setJob, requestSkills, updateJob, resetJobForm,
    addRequirement, removeRequirement,

} from 'actions';
import autobind from 'autobind';
import { Navbar, Card, Input, Button, Selector, Select, Container, Frame } from 'components';
import ReactQuill from 'react-quill';
import styles from './styles.module.css';


@connect(
    state => ({
        user: state.user,
        currentTab: state.app.browseTab,
        job: state.jobs.form,
        jobs: state.jobs.list,
        skills: state.skills,
    }),
    dispatch => bindActionCreators({
        changeBrowseTab, requestVolunteers, requestJobs, updateJobForm,
        postJob, setJob, requestSkills, updateJob,
        resetJobForm, addRequirement, removeRequirement,
    }, dispatch)
)
@autobind
class JobForm extends Component {
    state = {
        shouldValidate: {},
    }

    componentDidMount() {
        const { match: { params: { id } }, jobs, setJob, requestSkills } = this.props;
        requestSkills();
        if (!id) return;
        const job = jobs.find(job => job.id === Number(id));
        setJob({
            ...job,
            requirements: job.requirements.map(requirement => ({ skill: requirement.skill.id, points: requirement.points })),
            image: undefined,
            applicants: undefined,
        });
    }

    componentWillUnmount() {
        const { resetJobForm } = this.props;
        resetJobForm();
    }

    handleChange(key) {
        const { updateJobForm } = this.props;
        return value => updateJobForm(key, value);
    }

    handleChangeImage($event) {
        const { updateJobForm } = this.props;
        const file = $event.currentTarget.files[0];
        const reader = new FileReader();
        reader.onloadend = () => {
            updateJobForm('image', reader.result);
        }
        reader.readAsDataURL(file);
    }

    handleChangeSelector($event) {
        const { job: { skills }, updateJobForm } = this.props;
        const { currentTarget: { value, checked } } = $event;
        if (checked) updateJobForm('skills', [...skills, { skill: value ,points: Number(value)}]);
        else updateJobForm('skills', skills.filter(item => item.skill !== Number(value)));
    }

    handleBlur(key) {
        const { shouldValidate } = this.state;
        return () => this.setState({ shouldValidate: { ...shouldValidate, [key]: true } });
    }

    handleClickEditJob() {
        const { job, job: { id }, updateJob } = this.props;
        updateJob({ id, job });
    }

    handleClickCreateJob() {
        const { job, postJob } = this.props;
        postJob(job);
    }

    handleAdd(value) {
        const { addRequirement } = this.props;
        addRequirement(value);
    }

    handleRemove(id) {
        const { removeRequirement } = this.props;
        removeRequirement(id)
    }

    renderButton() {
        const { job: { id } } = this.props
        if (id) return <Button onClick={this.handleClickEditJob} theme='primary'>Save</Button>;
        return <Button onClick={this.handleClickCreateJob} theme='primary'>Create Job</Button>;
    }

    render() {
        const { job: {
            title ,description ,date ,location ,commitHours ,vacancy, image, requirements, fee,
        }, skills: { types } } = this.props
        return (
            <Frame>
                <Navbar shadow text='Add Job'/>
                <Container>
                    <Card noPaddingTop>
                        <label className={styles.image} style={{ backgroundImage: `url(${image}), url(http://www.btklsby.go.id/images/placeholder/camera.jpg)`}} htmlFor="image">
                            <input id="image" onChange={this.handleChangeImage} type="file" />
                        </label>
                        <div className={styles['card-content']}>
                            <Input
                                placeholder='Job Title'
                                type="text"
                                value={title}
                                onChange={this.handleChange('title')}
                                onBlur={this.handleBlur('title')}
                            />
                            <ReactQuill
                                value={description}
                                theme='snow'
                                onChange={this.handleChange('description')}
                            />
                        </div>
                    </Card>
                    <Card title='Schedule and Job Information'>
                        <div className={styles['card-content']}>
                            <Input
                                label='Required by'
                                type='date'
                                value={date}
                                onChange={this.handleChange('date')}
                                onBlur={this.handleBlur('date')}
                            />
                            <Input
                                label='Location'
                                type='text'
                                value={location}
                                onChange={this.handleChange('location')}
                                onBlur={this.handleBlur('location')}
                            />
                            <Input
                                label='Commit Hours'
                                type='text'
                                value={commitHours}
                                onChange={this.handleChange('commitHours')}
                                onBlur={this.handleBlur('commitHours')}
                            />
                            <Input
                                label='Number of vacancy'
                                type='number'
                                value={vacancy}
                                onChange={this.handleChange('vacancy')}
                                onBlur={this.handleBlur('vacancy')}
                            />
                            <Select
                                label='Fee'
                                options={[{value:'bono', label: 'Pro bono'},{value:'paid', label: 'Paid'}]}
                                onChange={this.handleChange('fee')}
                                value={fee}
                            />
                        </div>
                    </Card>
                    <Card title='Skills'>
                        <div className={styles['card-content']}>
                            <Selector
                                placeholder='Type some Skill'
                                list={types}
                                selected={requirements}
                                onAdd={this.handleAdd}
                                onRemove={this.handleRemove}
                            />
                        </div>
                    </Card>
                </Container>
                <div className={styles['button-container']}>
                    {this.renderButton()}
                </div>
            </Frame>
        )
    }
}

export default JobForm;