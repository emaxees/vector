import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {trackEvent} from 'core/tracking';
import {
    changeBrowseTab, requestVolunteers, requestJobs, updateJobForm,
    postJob, setJob, requestSkills, updateJob, resetJobForm,
    addRequirement, removeRequirement, requestJob, updateJobFormInBulk,
} from 'core/store/actions';
import { JSONToFormData } from 'core/utils';
import {
    Navbar, Card, Input,
    Button, Selector, Select,
    Container, Frame, Loader
} from 'components';
import ReactQuill from 'react-quill';
import styles from './styles.module.css';


class JobForm extends Component {
    state = {
        shouldValidate: {},
    }

    componentDidMount(){
        const { match: { params: { id } }, jobs, requestJob, requestSkills, updateJobFormInBulk } = this.props;
        requestSkills();
        if (!id) return;
        const job = this.getJob(id);
        if (!jobs.length) requestJob(id);
        if (job) updateJobFormInBulk(job)
    }

    componentWillUnmount() {
        const { resetJobForm } = this.props;
        resetJobForm();
    }

    getJob = (id) => {
        const { jobs } = this.props;
        return jobs.find(job => job.id === Number(id));
    }

    handleChange = (key) => {
        const { updateJobForm } = this.props;
        return value => updateJobForm(key, value);
    }

    handleChangeImage = ($event) => {
        const { updateJobForm } = this.props;
        $event.preventDefault();
        let reader = new FileReader();
        let file = $event.target.files[0];
        reader.onloadend = () => updateJobForm('image', file);
        reader.readAsDataURL(file);
    }

    handleChangeSelector = ($event) => {
        const { job: { skills }, updateJobForm } = this.props;
        const { currentTarget: { value, checked } } = $event;
        if (checked) updateJobForm('skills', [...skills, { skill: value ,points: Number(value)}]);
        else updateJobForm('skills', skills.filter(item => item.skill !== Number(value)));
    }

    handleBlur = (key) => {
        const { shouldValidate } = this.state;
        return () => this.setState({ shouldValidate: { ...shouldValidate, [key]: true } });
    }

    handleClickEditJob = () => {
        const { match: { params: { id } } } = this.props;
        const { job, updateJob } = this.props;
        const form = JSONToFormData({
            ...job,
            requirements: JSON.stringify(job.requirements)
        });
        trackEvent('editJob');
        updateJob({ id, form });
    }

    handleClickCreateJob = () => {
        const { job, postJob } = this.props;
        const form = JSONToFormData({
            ...job,
            requirements: JSON.stringify(job.requirements)
        });
        trackEvent('createJob');
        postJob(form);
    }

    handleAdd = (value) => {
        const { addRequirement } = this.props;
        addRequirement(value);
    }

    handleRemove = (id) => {
        const { removeRequirement } = this.props;
        removeRequirement(id)
    }

    renderButton = () => {
        const { match: { params: { id } } } = this.props;
        if (id) return <Button onClick={this.handleClickEditJob} theme='primary'>Save</Button>;
        return <Button onClick={this.handleClickCreateJob} theme='primary'>Create Job</Button>;
    }

    getBackgroundImage = (image) => {
        if (image instanceof File) return URL.createObjectURL(image);
        return image;
    }

    renderForm = () => {
        const {
            skills: { types },
            job: {
                image, title, description,
                date, location, commitHours,
                vacancy, requirements, fee,
            }
        } = this.props;
        return (
            <>
                <Card noPaddingTop>
                    <label className={styles.image} style={{ backgroundImage: `url(${this.getBackgroundImage(image)}), url(http://www.btklsby.go.id/images/placeholder/camera.jpg)`}} htmlFor="image">
                        <input id="image" onChange={this.handleChangeImage} type="file" />
                    </label>
                    <div className={styles['card-content']}>
                        <Input
                            placeholder='Job Title'
                            type="text"
                            value={title}
                            onChange={this.handleChange('title')}
                            onBlur={this.handleBlur('title')}
                        />
                        <ReactQuill
                            value={description}
                            theme='snow'
                            onChange={this.handleChange('description')}
                        />
                    </div>
                </Card>
                <Card title='Schedule and Job Information'>
                    <div className={styles['card-content']}>
                        <Input
                            label='Required by'
                            type='date'
                            value={date}
                            onChange={this.handleChange('date')}
                            onBlur={this.handleBlur('date')}
                        />
                        <Input
                            label='Location'
                            type='text'
                            value={location}
                            onChange={this.handleChange('location')}
                            onBlur={this.handleBlur('location')}
                        />
                        <Input
                            label='Commit Hours'
                            type='text'
                            value={commitHours}
                            onChange={this.handleChange('commitHours')}
                            onBlur={this.handleBlur('commitHours')}
                        />
                        <Input
                            label='Number of vacancy'
                            type='number'
                            value={vacancy}
                            onChange={this.handleChange('vacancy')}
                            onBlur={this.handleBlur('vacancy')}
                        />
                        <Select
                            label='Fee'
                            options={[{value:'bono', label: 'Pro bono'},{value:'paid', label: 'Paid'}]}
                            onChange={this.handleChange('fee')}
                            value={fee}
                        />
                    </div>
                </Card>
                <Card title='Skills'>
                    <div className={styles['card-content']}>
                        <Selector
                            placeholder='Type some Skill'
                            list={types}
                            selected={requirements}
                            onAdd={this.handleAdd}
                            onRemove={this.handleRemove}
                        />
                    </div>
                </Card>
            </>
        )
    }

    getText = () => {
        const { match: { params: { id } } } = this.props;
        if (id) return 'Edit Job';
        return 'Add Job';
    }

    renderContent = () => {
        const { match: { params: { id } } } = this.props;
        const job = this.getJob(id);
        if (!id) return this.renderForm();
        if (job) return this.renderForm();
        return <Loader />;

    }

    render() {
        return (
            <Frame>
                <Navbar shadow text={this.getText()}/>
                <Container>
                    {this.renderContent()}
                </Container>
                <div className={styles['button-container']}>
                    {this.renderButton()}
                </div>
            </Frame>
        )
    }
}

export default connect(
    state => ({
        user: state.user,
        currentTab: state.app.browseTab,
        job: state.jobs.form,
        jobs: state.jobs.list,
        skills: state.skills,
    }),
    dispatch => bindActionCreators({
        changeBrowseTab, requestVolunteers, requestJobs, updateJobForm,
        postJob, setJob, requestSkills, updateJob, requestJob,
        resetJobForm, addRequirement, removeRequirement, updateJobFormInBulk,
    }, dispatch)
)(JobForm);