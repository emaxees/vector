import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {trackEvent} from 'core/tracking';
import {
    markJobAsDone, showPopup, setPopupContent,
    setJob, setApplicant, updateReviewForm,
    approveApplicant, rejectApplicant, requestJob,
} from 'core/store/actions';
import {
    Card, Navbar, Button, Loader,
    Message, Frame, Container,
    ReviewForm, HoursForm, VolunteerPreview
} from 'components';
import styles from './styles.module.css';


class JobApplicants extends Component {
    componentDidMount(){
        const { match: { params: { id } }, jobs, requestJob } = this.props;
        if (jobs.length || !jobs.find((job) => job.id)) requestJob(id);
    }

    getRight = () => {
        const { user: { type } } = this.props;
        if (type === 'enterprise') return <div onClick={this.handleClickRight}>Edit</div>
    }

    getJob = () => {
        const { match: { params: { id } }, jobs } = this.props;
        return jobs.find(job => job.id === Number(id));
    }

    handleClickRight = () => {
        const {  match: { params: { id } }, history: { push } } = this.props;
        push(`/job/${id}/edit`)
    }

    handleClickApply = () => {
        const { id } = this.getJob();
        const { applyJob } = this.props;
        applyJob(id);
    }

    getApprovedApplicants = () => {
        const { applicants } = this.getJob();
        return applicants.filter(applicant => applicant.status === 'approved');
    }

    handleClickReject = (applicant) => {
        const { match, rejectApplicant } = this.props;
        const { id } = applicant;
        return () => {
            const applicants = this.getJob().applicants.map(applicant => ({...applicant, volunteer: applicant.volunteer.id}));
            const job = {
                applicants: [
                    ...applicants,
                    {
                        ...applicants.find(applicant => applicant.id === id),
                        status: 'rejected'
                    }
                ]
            }
            rejectApplicant({id: match.params.id, job});
            trackEvent('rejectApplicant');
        }
    }


    handleClickApprove = (applicant) => {
        const { match, approveApplicant } = this.props;
        const { id } = applicant;
        return () => {
            const applicants = this.getJob().applicants.map(applicant => ({...applicant, volunteer: applicant.volunteer.id}));
            const job = {
                applicants: [
                    ...applicants.filter(applicant => applicant.id !== id),
                    {
                        ...applicants.find(applicant => applicant.id === id),
                        status: 'approved'
                    }
                ]
            }
            approveApplicant({id: match.params.id, job});
            trackEvent('approveApplicant');
        }
    }

    handleClickMarkJobAsDone = () => {
        const job = this.getJob();
        const { markJobAsDone } = this.props;
        markJobAsDone({
            id: job.id,
            job: { finished: true }
        })
        trackEvent('markJobAsDone');
    }

    isAlreadyApplied = () => {
        const job = this.getJob();
        const { user: { id } } = this.props;
        return Boolean(job.applicants.find(applicant => applicant.volunteer.id === Number(id)));
    }

    handleClickRateReview = (applicant) => {
        const { setPopupContent, showPopup, updateReviewForm, setJob, user } = this.props;
        return () => {
            updateReviewForm('reviewer', user.id);
            updateReviewForm('user', applicant.volunteer.id);
            setJob(this.getJob());
            setPopupContent(<ReviewForm />);
            showPopup();
        };
    }

    handleClickSetHours = (applicant) => {
        const { setPopupContent, showPopup, setJob, setApplicant } = this.props;
        return () => {
            setApplicant(applicant);
            setJob(this.getJob());
            setPopupContent(<HoursForm />);
            showPopup();
        }
    }

    renderApplicantsActions = (applicant) => {
        const job = this.getJob();
        const { user: { type } } = this.props;
        if (type === 'volunteer') return;
        if (applicant.status === 'rejected' && !job.finished) return <i className='material-icons'>person_add_disabled</i>;
        if (applicant.status === 'approved' && !job.finished) return <i className='material-icons'>person_add</i>;
        if (job.finished && !applicant.hours) return <i onClick={this.handleClickSetHours(applicant)} className="material-icons">watch_later</i>
        if (applicant.hasReview) return <i className='material-icons'>verified_user</i>;
        if (job.finished && applicant.hours > 0) return <i onClick={this.handleClickRateReview(applicant)} className="material-icons">rate_review</i>
        return (
            <div className={styles.actions}>
                <Button onClick={this.handleClickApprove(applicant)} theme='icon'>
                    <i className='material-icons'>done</i>
                </Button>
                <Button onClick={this.handleClickReject(applicant)} theme='icon'>
                    <i className='material-icons'>clear</i>
                </Button>
            </div>
        );
    }

    renderButton = () => {
        const { user: { type } } = this.props;
        const { finished } = this.getJob();
        if (type === 'enterprise' && this.getApprovedApplicants().length >= 1 && !finished) return <Button onClick={this.handleClickMarkJobAsDone} theme='primary'>Mark job as done</Button>;
    }

    renderApplicants = (job) => {
        const props = {
            text: `There are no applicants yet`,

            icon: 'people',
        };
        if (!job.applicants.length) return <Message {...props} />;
        return job.applicants.map(applicant => (
            <Card key={applicant.id}>
                <div className={styles.content}>
                    <VolunteerPreview compact volunteer={applicant.volunteer} />
                    { this.renderApplicantsActions(applicant) }
                </div>
            </Card>
        ));
    }

    render() {
        const job = this.getJob();
        return (
            <Frame>
                <Navbar shadow text='Applicants' />
                <Container>
                    { job ? this.renderApplicants(job) : <Loader /> }
                </Container>
                <div className={styles['button-container']}>
                    { job  && this.renderButton() }
                </div>
            </Frame>
        )
    }
}

export default connect(
    state => ({
        user: state.user,
        jobs: state.jobs.list,
    }),
    dispatch => bindActionCreators({
        markJobAsDone, showPopup, setPopupContent,
        setJob, setApplicant, updateReviewForm,
        approveApplicant, rejectApplicant, requestJob,
    }, dispatch)
)(JobApplicants);
