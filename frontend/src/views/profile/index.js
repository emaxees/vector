import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeBrowseTab, requestVolunteers, requestJobs } from 'actions';
import { SlideUp } from 'animations';
import { Frame, Container, Header, BrowseTab, Message, Card, Review } from 'components';
import CircularProgressbar from 'react-circular-progressbar';
import autobind from 'autobind';
import { getSkillLevel } from 'utils';
import styles from './styles.module.css';


@connect(
    state => ({
        volunteers: state.volunteers.list,
        currentTab: state.app.browseTab,
    }),
    dispatch => bindActionCreators({ changeBrowseTab, requestVolunteers, requestJobs }, dispatch)
)
@autobind
class Profile extends Component {
    state = {
        currentTab: 1,
    }

    getUser() {
        const { match: { params: { id } }, volunteers } = this.props;
        return volunteers.find(volunteer => volunteer.id === Number(id));
    }

    getChartData() {
        const { skills } = this.getUser();
        return skills.map(skill => (
            {
                name: skill.name,
                points: skill.points,
                fullMark: 1000
            }
        ));
    }

    getTabs() {
        return [{ label: 'Skills', id: 1 },{ label: 'Reviews', id: 2 }];
    }

    handleClickBrowseTab(id) {
        this.setState({currentTab: id});
    }

    renderSkills() {
        const { skills } = this.getUser();
        return skills.map(skill => (
            <Card key={skill.id}>
                <div className={styles.skill}>
                    <CircularProgressbar
                        percentage={(skill.points*100)/1000}
                        text={`${(skill.points*100)/1000}%`}
                        styles={{
                            root: {},
                            path: {
                              stroke: '#E64D4B',
                              strokeLinecap: 'butt',
                              transition: 'stroke-dashoffset 0.5s ease 0s',
                            },
                            trail: {
                              stroke: '#d6d6d6',
                            },
                            text: {
                              fill: '#E64D4B',
                            },
                        }}
                    />
                    <div>
                        <h1>{skill.name}</h1>
                        <p>{getSkillLevel(skill.points)}</p>
                        <div>
                            <i className='material-icons'>
                                insert_chart
                            </i>
                            {`Skill points ${skill.points}/1000`}
                        </div>
                    </div>
                </div>
            </Card>)
        )
    }

    renderReviews() {
        const { reviews } = this.getUser();
        if (!reviews.length) {
            const props = {
                icon: 'stars',
                text: `You don’t have any review`,

            };
            return <Message {...props} />;
        }
        return (
            <div className={styles.reviews}>
                {reviews.map((review, index) => (
                    <SlideUp key={review.id} initialPose="closed" pose="open" count={index}>
                        <Card key={review.id}>
                            <div className={styles.cardContent}>
                                <Review review={review} />
                            </div>
                        </Card>
                    </SlideUp>
                ))}
            </div>
        )
    }

    renderTabs() {
        const { currentTab } = this.state;
        if (currentTab === 2) return this.renderReviews();
        return this.renderSkills();
    }

    render() {
        const { currentTab } = this.state;
        return (
            <Frame>
                <Header full user={this.getUser()}/>
                <BrowseTab tabs={this.getTabs()} onClick={this.handleClickBrowseTab} currentTab={currentTab}/>
                <Container>
                    <div className={styles.list}>
                        {this.renderTabs()}
                    </div>
                </Container>
            </Frame>
        )
    }
}

export default Profile;
