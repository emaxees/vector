import React from 'react';
import { Route, Switch } from 'react-router';
import Volunteer from './volunteer';
import Enterprise from './enterprise';


const Profile = ({ match }) => (
    <Switch>
        <Route
            path={`${match.path}/enterprise/:id`}
            component={Enterprise}
        />
        <Route
            path={`${match.path}/volunteer/:id`}
            component={Volunteer}
        />
    </Switch>
);

export default Profile;
