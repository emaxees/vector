import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeBrowseTab, requestEnterprise } from 'core/store/actions';
import { SlideUp } from 'animations';
import { Frame, Container, Header, BrowseTab, Message, Card, JobPreview, Loader } from 'components';
import styles from './styles.module.css';


const Enterprise = ({
    enterprise,
    fetched,
    history : { push },
    match: { params: { id } },
    requestEnterprise,
}) => {
    const [currentTab, setCurrentTab] = useState(1);
    useEffect(() => {
        requestEnterprise(id);
    }, [id, requestEnterprise])

    const getTabs = () => [{ label: 'About', id: 1 },{ label: 'Jobs', id: 2 }];

    const handleClickJobPreview = (id) => () => push(`/job/${id}`);

    const handleClickBrowseTab = (id) => setCurrentTab(id);

    const renderAbout = () => {
        if (!fetched) return <Loader style={{height: '100%'}}/>;
        return (
            <div>
                <Card title='Company Overview'>
                    <div className={styles['card-content']}>
                        <div dangerouslySetInnerHTML={{__html: enterprise.bio}} />
                    </div>
                </Card>
                <Card title='Administrator Name'>
                    <div className={styles['card-content']}>
                        {enterprise.administratorName}
                    </div>
                </Card>
                <Card title='Interests'>
                    <div className={styles['card-content']}>
                        <div className={styles.list}>
                            {enterprise.interests.map(interest => (
                                <span key={interest.id}>{interest.name}</span>
                            ))}
                        </div>
                    </div>
                </Card>
            </div>
        );
    }

    const renderJobs = () => {
        if (!fetched) return <Loader style={{height: '100%'}}/>;
        if (!enterprise.jobs.length) {
            const props = {
                icon: 'notification_important',
                text: 'There are no jobs posted!',
            };
            return <Message {...props} />;
        };
        return enterprise.jobs.map((job, index) => {
            return (
                <SlideUp key={job.id} initialPose="closed" pose="open" count={index}>
                    <JobPreview onClick={handleClickJobPreview(job.id)} job={job}/>
                </SlideUp>
            );
        });
    }


    const renderTabs = () => {
        if (currentTab === 2) return renderJobs();
        return renderAbout();
    }

    return (
        <Frame>
            <Header isLoading={!fetched} full user={enterprise}/>
            <BrowseTab tabs={getTabs()} onClick={handleClickBrowseTab} currentTab={currentTab}/>
            <Container>
                <div className={styles.list}>
                    {!fetched ? <Loader/> : renderTabs()}
                </div>
            </Container>
        </Frame>
    );
}

export default connect(
    state => ({
        enterprise: state.profile.enterprise,
        fetched: state.profile.isEnterpriseFetched,
    }),
    dispatch => bindActionCreators({ changeBrowseTab, requestEnterprise }, dispatch)
)(Enterprise)
