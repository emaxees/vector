import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeBrowseTab, requestVolunteers, requestVolunteer, requestJobs } from 'core/store/actions';
import { SlideUp } from 'animations';
import { Frame, Container, Header, BrowseTab, Message, Card, Review, Loader } from 'components';
import { CircularProgressbar } from 'react-circular-progressbar';
import { getSkillLevel } from 'core/utils';
import styles from './styles.module.css';


class Volunteer extends Component {
    state = {
        currentTab: 1,
    }

    componentDidMount() {
        const { requestVolunteer, volunteers, match: { params: { id } } } = this.props;
        if (!volunteers.length) requestVolunteer(id);
    }

    getUser = () => {
        const { match: { params: { id } }, volunteers } = this.props;
        return volunteers.find(volunteer => volunteer.id === Number(id));
    }

    getChartData = () => {
        const { skills } = this.getUser();
        return skills.map(skill => (
            {
                name: skill.name,
                points: skill.points,
                fullMark: 1000
            }
        ));
    }

    getTabs = () => {
        return [{ label: 'About', id: 1 }, { label: 'Skills', id: 2 },{ label: 'Reviews', id: 3 }];
    }

    handleClickBrowseTab = (id) => {
        this.setState({currentTab: id});
    }

    renderSkills = () => {
        const { skills } = this.getUser();
        return skills.map(skill => (
            <Card key={skill.id}>
                <div className={styles.skill}>
                    <CircularProgressbar
                        value={(skill.points*100)/1000}
                        text={`${(skill.points*100)/1000}%`}
                        styles={{
                            root: {},
                            path: {
                              stroke: '#E64D4B',
                              strokeLinecap: 'butt',
                              transition: 'stroke-dashoffset 0.5s ease 0s',
                            },
                            trail: {
                              stroke: '#d6d6d6',
                            },
                            text: {
                              fill: '#E64D4B',
                            },
                        }}
                    />
                    <div>
                        <h1>{skill.name}</h1>
                        <p>{getSkillLevel(skill.points)}</p>
                        <div>
                            <i className='material-icons'>
                                insert_chart
                            </i>
                            {`Skill points ${skill.points}/1000`}
                        </div>
                    </div>
                </div>
            </Card>)
        )
    }

    renderReviews = () => {
        const { reviews } = this.getUser();
        if (!reviews.length) {
            const props = {
                icon: 'stars',
                text: `There's not reviews published yet`,

            };
            return <Message {...props} />;
        }
        return (
            <div className={styles.reviews}>
                {reviews.map((review, index) => (
                    <SlideUp key={review.id} initialPose="closed" pose="open" count={index}>
                        <Card key={review.id}>
                            <div className={styles.cardContent}>
                                <Review review={review} />
                            </div>
                        </Card>
                    </SlideUp>
                ))}
            </div>
        )
    }

    renderAbout = () => {
        const { firstName, lastName, bio, skills } = this.getUser();
        return (
            <div>
                <Card title='First Name'>
                    <div className={styles['card-content']}>
                        {firstName}
                    </div>
                </Card>
                <Card title='Last Name'>
                    <div className={styles['card-content']}>
                        {lastName}
                    </div>
                </Card>
                <Card title='Bio'>
                    <div className={styles['card-content']}>
                        <div dangerouslySetInnerHTML={{__html: bio}} />
                    </div>
                </Card>
                <Card title='Skills'>
                    <div className={styles['card-content']}>
                        <div className={styles.list}>
                            {skills.map(skill => (
                                <span key={skills.id}>{skill.name}</span>
                            ))}
                        </div>
                    </div>
                </Card>
            </div>
        );
    }

    renderTabs = () => {
        const { currentTab } = this.state;
        if (currentTab === 1) return this.renderAbout();
        if (currentTab === 2) return this.renderSkills();
        return this.renderReviews();
    }

    render = () => {
        const { currentTab } = this.state;
        const { volunteers } = this.props;
        return (
            <Frame>
                {<Header isLoading={!volunteers.length} full user={this.getUser()}/>}
                <BrowseTab tabs={this.getTabs()} onClick={this.handleClickBrowseTab} currentTab={currentTab}/>
                <Container>
                    <div className={styles.list}>
                        {!volunteers.length ? <Loader/> : this.renderTabs()}
                    </div>
                </Container>
            </Frame>
        );
    }
}

export default connect(
    state => ({
        volunteers: state.volunteers.list,
        currentTab: state.app.browseTab,
    }),
    dispatch => bindActionCreators({ changeBrowseTab, requestVolunteers, requestVolunteer, requestJobs }, dispatch)
)(Volunteer);
