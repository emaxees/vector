import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Button, Input } from 'components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { isValidField } from 'utils';
import { updateSignUpForm, requestSignUpEnterprise } from 'actions';
import autobind from 'autobind';
import styles from './styles.module.css';


@connect(
    state => ({
        signUp: state.signUp,
    }),
    dispatch => bindActionCreators({ updateSignUpForm, requestSignUpEnterprise }, dispatch),
)
@autobind
class SocialEnterpriseForm extends Component {
    state = {
        shouldValidate: {},
    }

    getButtonDisabled() {
        const { companyName, administratorName, email, password } = this.props.signUp;
        return !(isValidField(email) && isValidField(password) && isValidField(companyName) && isValidField(administratorName));
    }

    handleBlur(key) {
        const { shouldValidate } = this.state;
        return () => this.setState({ shouldValidate: { ...shouldValidate, [key]: true } });
    }

    handleFocus() {
        window.scroll(0, 0);
    }

    handleChange(key) {
        const { updateSignUpForm } = this.props;
        return value => updateSignUpForm(key, value);
    }

    handleSubmit($event) {
        const { requestSignUpEnterprise } = this.props;
        const { companyName, administratorName, email, password } = this.props.signUp;
        $event.preventDefault();
        requestSignUpEnterprise({ companyName, administratorName, email, password });
    }

    render() {
        const { signUp: { companyName, administratorName, email, password, message }} = this.props;
        return (
            <div className={styles.root}>
                <form onSubmit={this.handleSubmit}>
                    <Input
                        placeholder='Company Name'
                        type="text"
                        value={companyName}
                        onChange={this.handleChange('companyName')}
                        onBlur={this.handleBlur('companyName')}
                    />
                    <Input
                        placeholder='Administrator Name'
                        type="text"
                        value={administratorName}
                        onChange={this.handleChange('administratorName')}
                        onBlur={this.handleBlur('administratorName')}
                    />
                    <Input
                        placeholder='Email Address'
                        type="text"
                        value={email}
                        onChange={this.handleChange('email')}
                        onBlur={this.handleBlur('email')}
                    />
                    <Input
                        placeholder='Password'
                        type="password"
                        value={password}
                        onChange={this.handleChange('password')}
                        onBlur={this.handleBlur('password')}
                    />
                    <Button theme='primary' disabled={this.getButtonDisabled()} type='submit'>
                        Create account
                    </Button>
                </form>
                { message &&  Object.keys(message).map(key => message[key].map(error => <p>{error}</p>))}
                <div className={styles.link}> By Tapping create account you agree Term of services and Privacy Policies</div>
                {/* <Line>Or</Line>
                <div className={styles.socialSignUp}>
                    <Button theme='facebook'>
                        Continue with Facebook
                    </Button>
                    <Button theme='google'>
                        Continue with Google
                    </Button>
                </div> */}
                <div className={styles.link}> Got account?, <NavLink to='/login'>Login Here</NavLink></div>
            </div>
        )
    }
}

export default SocialEnterpriseForm;