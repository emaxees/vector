import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Button, Input, Checkbox, Select } from 'components';
import {trackEvent} from 'core/tracking';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { isValidField } from 'core/utils';
import { updateSignUpForm, requestSignUpEnterprise } from 'core/store/actions';
import styles from './styles.module.css';


class SocialEnterpriseForm extends Component {
    state = {
        shouldValidate: {},
        agreePrivacyPolicy: false,
    }

    getButtonDisabled() {
        const { companyName, administratorName, email, password } = this.props.signUp;
        const { agreePrivacyPolicy } = this.state;
        return !(isValidField(email) && isValidField(password) && isValidField(companyName) && isValidField(administratorName) && agreePrivacyPolicy);
    }

    handleBlur(key) {
        const { shouldValidate } = this.state;
        return () => this.setState({ shouldValidate: { ...shouldValidate, [key]: true } });
    }

    handleFocus() {
        window.scroll(0, 0);
    }

    handleChange(key) {
        const { updateSignUpForm } = this.props;
        return value => updateSignUpForm(key, value);
    }

    handleSubmit($event) {
        const { requestSignUpEnterprise } = this.props;
        const { companyName, administratorName, email, password, phoneNumber, found } = this.props.signUp;
        trackEvent('submitVolunteerRegisterForm');
        $event.preventDefault();
        requestSignUpEnterprise({ companyName, administratorName, email, password, phoneNumber, found });
    }

    handleChangeCheckbox($event) {
        if ($event.target.value === 'false') return this.setState({ agreePrivacyPolicy: true });
        return this.setState({ agreePrivacyPolicy: false });
    }

    render() {
        const { signUp: { companyName, administratorName, email, password, message, phoneNumber, found }} = this.props;
        const { agreePrivacyPolicy } = this.state;
        return (
            <div className={styles.root}>
                <form onSubmit={this.handleSubmit}>
                    <Input
                        placeholder='Company Name'
                        type="text"
                        value={companyName}
                        onChange={this.handleChange('companyName')}
                        onBlur={this.handleBlur('companyName')}
                    />
                    <Input
                        placeholder='Administrator Name'
                        type="text"
                        value={administratorName}
                        onChange={this.handleChange('administratorName')}
                        onBlur={this.handleBlur('administratorName')}
                    />
                    <Input
                        placeholder='Email Address'
                        type="text"
                        value={email}
                        onChange={this.handleChange('email')}
                        onBlur={this.handleBlur('email')}
                    />
                    <Input
                        placeholder='Password'
                        type="password"
                        value={password}
                        onChange={this.handleChange('password')}
                        onBlur={this.handleBlur('password')}
                    />
                    <Input
                        placeholder='Phone Number'
                        type="phone"
                        value={phoneNumber}
                        onChange={this.handleChange('phoneNumber')}
                        onBlur={this.handleBlur('phoneNumber')}
                    />
                    <Select
                        label='How did you find us?'
                        options={[
                            { value: 'social', label: 'Social Media'},
                            { value: 'marketing', label: 'Face to face marketing'},
                            { value: 'recommendation', label: 'Peer recommendation'},
                            { value: 'newsletters', label: 'School newsletters/email'},
                            { value: 'others', label: 'Others'},
                        ]}
                        onChange={this.handleChange('found')}
                        onBlur={this.handleBlur('found')}
                        value={found}
                    />
                    <Button theme='primary' disabled={this.getButtonDisabled()} type='submit'>
                        Create account
                    </Button>
                </form>
                { message &&  Object.keys(message).map(key => message[key].map(error => <p>{error}</p>))}
                <p className={styles.text}>
                    By tapping Create account you agree to the Term of Services and Privacy Policies
                </p>
                <div className={styles.terms}>
                    <Checkbox value={agreePrivacyPolicy} checked={agreePrivacyPolicy} onChange={this.handleChangeCheckbox}/>
                    I agree to <NavLink to='/privacy-policy'>Privacy Policy</NavLink>
                </div>
                <div className={styles.link}> Got account?, <NavLink to='/login'>Login Here</NavLink></div>
            </div>
        )
    }
}

export default connect(
    state => ({
        signUp: state.signUp,
    }),
    dispatch => bindActionCreators({ updateSignUpForm, requestSignUpEnterprise }, dispatch),
)(SocialEnterpriseForm);
