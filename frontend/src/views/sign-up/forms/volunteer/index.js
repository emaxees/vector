import React, { Component } from 'react';
import { Button, Input, Checkbox, Select } from 'components';
import { NavLink } from 'react-router-dom';
import {trackEvent} from 'core/tracking';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { isValidField } from 'core/utils';
import { updateSignUpForm, requestSignUpVolunteer } from 'core/store/actions';
import styles from './styles.module.css';



class VolunteerForm extends Component {
    state = {
        shouldValidate: {},
        agreePrivacyPolicy: false,
    }

    getButtonDisabled() {
        const { firstName, lastName, email, password } = this.props.signUp;
        const { agreePrivacyPolicy } = this.state;
        return !(isValidField(email) && isValidField(password) && isValidField(firstName) && isValidField(lastName) && agreePrivacyPolicy);
    }

    handleBlur(key) {
        const { shouldValidate } = this.state;
        return () => this.setState({ shouldValidate: { ...shouldValidate, [key]: true } });
    }

    handleFocus() {
        window.scroll(0, 0);
    }

    handleChange(key) {
        const { updateSignUpForm } = this.props;
        return value => updateSignUpForm(key, value);
    }

    handleSubmit($event) {
        const { requestSignUpVolunteer } = this.props;
        const { firstName, lastName, email, password, phoneNumber, found } = this.props.signUp;
        $event.preventDefault();
        trackEvent('submitVolunteerRegisterForm');
        requestSignUpVolunteer({ firstName, lastName, email, password, phoneNumber, found });
    }

    handleChangeCheckbox($event) {
        if ($event.target.value === 'false') return this.setState({ agreePrivacyPolicy: true });
        return this.setState({ agreePrivacyPolicy: false });
    }

    render() {
        const { signUp: { firstName, lastName, email, password, message, phoneNumber, found }} = this.props;
        const { agreePrivacyPolicy } = this.state;
        return (
            <div className={styles.root}>
                <form onSubmit={this.handleSubmit}>
                    <Input
                        placeholder='First Name'
                        type="text"
                        value={firstName}
                        onChange={this.handleChange('firstName')}
                        onBlur={this.handleBlur('firstName')}
                    />
                    <Input
                        placeholder='Last Name'
                        type="text"
                        value={lastName}
                        onChange={this.handleChange('lastName')}
                        onBlur={this.handleBlur('lastName')}
                    />
                    <Input
                        placeholder='Email Address'
                        type="text"
                        value={email}
                        onChange={this.handleChange('email')}
                        onBlur={this.handleBlur('email')}
                    />
                    <Input
                        placeholder='Password'
                        type="password"
                        value={password}
                        onChange={this.handleChange('password')}
                        onBlur={this.handleBlur('password')}
                    />
                    <Input
                        placeholder='Phone Number'
                        type="phone"
                        value={phoneNumber}
                        onChange={this.handleChange('phoneNumber')}
                        onBlur={this.handleBlur('phoneNumber')}
                    />
                    <Select
                        label='How did you find us?'
                        options={[
                            { value: 'social', label: 'Social Media'},
                            { value: 'marketing', label: 'Face to face marketing'},
                            { value: 'recommendation', label: 'Peer recommendation'},
                            { value: 'newsletters', label: 'School newsletters/email'},
                            { value: 'others', label: 'Others'},
                        ]}
                        onChange={this.handleChange('found')}
                        onBlur={this.handleBlur('found')}
                        value={found}
                    />
                    <Button theme='primary' disabled={this.getButtonDisabled()} type='submit'>
                        Create account
                    </Button>
                </form>
                { message &&  Object.keys(message).map(key => message[key].map(error => <p>{error}</p>))}
                <p className={styles.text}>
                    By tapping Create account you agree to the Term of Services and Privacy Policies
                </p>
                <div className={styles.terms}>
                    <Checkbox value={agreePrivacyPolicy} checked={agreePrivacyPolicy} onChange={this.handleChangeCheckbox}/>
                    I agree to <NavLink to='/privacy-policy'>Privacy Policy</NavLink>
                </div>
                <div className={styles.link}> Got account?, <NavLink to='/login'>Login Here</NavLink></div>
            </div>
        )
    }
}

export default connect(
    state => ({
        signUp: state.signUp,
    }),
    dispatch => bindActionCreators({ updateSignUpForm, requestSignUpVolunteer }, dispatch)
)(VolunteerForm);