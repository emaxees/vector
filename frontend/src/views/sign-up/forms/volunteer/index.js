import React, { Component } from 'react';
import { Button, Input } from 'components';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { isValidField } from 'utils';
import { updateSignUpForm, requestSignUpVolunteer } from 'actions';
import autobind from 'autobind';
import styles from './styles.module.css';


@connect(
    state => ({
        signUp: state.signUp,
    }),
    dispatch => bindActionCreators({ updateSignUpForm, requestSignUpVolunteer }, dispatch),
)
@autobind
class VolunteerForm extends Component {
    state = {
        shouldValidate: {},
    }

    getButtonDisabled() {
        const { firstName, lastName, email, password } = this.props.signUp;
        return !(isValidField(email) && isValidField(password) && isValidField(firstName) && isValidField(lastName));
    }

    handleBlur(key) {
        const { shouldValidate } = this.state;
        return () => this.setState({ shouldValidate: { ...shouldValidate, [key]: true } });
    }

    handleFocus() {
        window.scroll(0, 0);
    }

    handleChange(key) {
        const { updateSignUpForm } = this.props;
        return value => updateSignUpForm(key, value);
    }

    handleSubmit($event) {
        const { requestSignUpVolunteer } = this.props;
        const { firstName, lastName, email, password } = this.props.signUp;
        $event.preventDefault();
        requestSignUpVolunteer({ firstName, lastName, email, password });
    }

    render() {
        const { signUp: { firstName, lastName, email, password, message }} = this.props;
        return (
            <div className={styles.root}>
                <form onSubmit={this.handleSubmit}>
                    <Input
                        placeholder='First Name'
                        type="text"
                        value={firstName}
                        onChange={this.handleChange('firstName')}
                        onBlur={this.handleBlur('firstName')}
                    />
                    <Input
                        placeholder='Last Name'
                        type="text"
                        value={lastName}
                        onChange={this.handleChange('lastName')}
                        onBlur={this.handleBlur('lastName')}
                    />
                    <Input
                        placeholder='Email Address'
                        type="text"
                        value={email}
                        onChange={this.handleChange('email')}
                        onBlur={this.handleBlur('email')}
                    />
                    <Input
                        placeholder='Password'
                        type="password"
                        value={password}
                        onChange={this.handleChange('password')}
                        onBlur={this.handleBlur('password')}
                    />
                    <Button theme='primary' disabled={this.getButtonDisabled()} type='submit'>
                        Create account
                    </Button>
                </form>
                { message &&  Object.keys(message).map(key => message[key].map(error => <p>{error}</p>))}
                <div className={styles.link}> By Tapping create account you agree Term of services and Privacy Policies</div>
                {/* <Line>Or</Line>
                <div className={styles.socialSignUp}>
                    <Button theme='facebook'>
                        Continue with Facebook
                    </Button>
                    <Button theme='google'>
                        Continue with Google
                    </Button>
                </div> */}
                <div className={styles.link}> Got account?, <NavLink to='/login'>Login Here</NavLink></div>
            </div>
        )
    }
}

export default VolunteerForm;