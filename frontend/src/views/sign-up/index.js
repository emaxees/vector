import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router';
import { Navbar } from 'components';
import Setup from './setup';
import { VolunteerForm, SocialEnterpriseForm } from './forms';


class SignUp extends Component {
    getNavbarText() {
        const { user : { type }} =  this.props;
        if (!type) return '';
        if (type === 'volunteer') return 'I\'m a Volunteer';
        return 'I\'m a Social Enterprise';
    }

    render() {
        const {match} = this.props;
        return (
            <Fragment>
                <Navbar text={this.getNavbarText()} />
                <Switch>
                    <Route
                        exact
                        path={match.path}
                        component={Setup}
                    />
                    <Route
                        path={`${match.path}/volunteer`}
                        component={VolunteerForm}
                    />
                    <Route
                        path={`${match.path}/social-enterprise`}
                        component={SocialEnterpriseForm}
                    />
                </Switch>
            </Fragment>
        )
    }
}

export default connect(
    state => ({
        user: state.user,
    }),
)(SignUp);
