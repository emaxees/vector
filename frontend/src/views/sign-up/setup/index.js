import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Glide } from 'components';
import { setUserType, resetUserType, resetSignUpForm } from 'core/store/actions';
import styles from './styles.module.css';
import enterprise from './assets/social-enterprise.png';
import volunteer from './assets/volunteer.png';


class Setup extends Component {

    componentDidMount() {
        const { resetSignUpForm, resetUserType } = this.props;
        resetSignUpForm();
        resetUserType();
    }

    handleClick = (type) => {
        const {
            setUserType,
            history: { push },
            history: { location : { pathname } }
        } = this.props;
        return () => {
            setUserType(type)
            push(`${pathname}/${type}`)
        };
    }

    render() {
        return (
            <div className={styles.root}>
                <div className={styles.text}> Tap on your engagement type </div>
                <Glide
                    options={{ type: 'carousel', perView: 1, focusAt: 'center' }}
                    arrowLeft={<i className="material-icons">keyboard_arrow_left</i>}
                    arrowRight={<i className="material-icons">keyboard_arrow_right</i>}
                >
                    <img onClick={this.handleClick('volunteer')} alt='volunteer' src={volunteer} />
                    <img alt='social-enterprise' onClick={this.handleClick('social-enterprise')} src={enterprise} />
                </Glide>
            </div>
        )
    }
}

export default connect(
    state => ({
        user: state.user,
    }),
    dispatch => bindActionCreators({ setUserType, resetSignUpForm, resetUserType }, dispatch),
)(Setup);