import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router';
import { bindActionCreators } from 'redux';
import {
    requestVolunteers, requestJobs, requestSkills,
    requestInterests, requestChats, requestNotifications,
} from 'actions';
import autobind from 'autobind';
import { Tabs } from 'components';
import WSC from 'wsc';
import { Dashboard, Chat, Notifications, ProfileDetail, Settings, ProfileForm } from 'views/home/tabs';
import styles from './styles.module.css';


@connect(
    state => ({
        user: state.user,
    }),
    dispatch => bindActionCreators({
        requestVolunteers, requestJobs, requestSkills,
        requestInterests, requestChats, requestNotifications
    }, dispatch)
)
@autobind
class Home extends Component {
    componentDidMount() {
        WSC.connect();
        const { requestVolunteers, requestJobs, user, requestSkills, requestInterests, requestChats, requestNotifications } = this.props;
        if (user.type === 'volunteer') {
            requestSkills();
        }
        if (user.type === 'enterprise') {
            requestInterests();
            requestVolunteers();
        };
        requestJobs();
        requestChats();
        requestNotifications();
    }

    renderTabs() {
        const { location: { pathname }, user} = this.props;
        if (/\/home\/chat\/\w*/.test(pathname)) return;
        if (/\/home\/profile\/\d*\/edit/.test(pathname)) return;
        return <Tabs user={user} />
    }

    render() {
        const { match } = this.props;
        return (
            <div className={styles.root}>
                <div className={styles.container}>
                    <Switch>
                        <Route exact path={match.path} render={() => <Redirect to={`${match.path}/dashboard`} />}/>
                        <Route
                            path={`${match.path}/dashboard`}
                            component={Dashboard}
                        />
                        <Route
                            path={`${match.path}/chat`}
                            component={Chat}
                        />
                        <Route
                            path={`${match.path}/notifications`}
                            component={Notifications}
                        />
                        <Route
                            path={`${match.path}/profile/:id/edit`}
                            component={ProfileForm}
                        />
                        <Route
                            path={`${match.path}/profile/:id`}
                            component={ProfileDetail}
                        />
                        <Route
                            path={`${match.path}/settings`}
                            component={Settings}
                        />
                    </Switch>
                </div>
                {this.renderTabs()}
            </div>
        )
    }
}

export default Home;
