import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router';
import { bindActionCreators } from 'redux';
import {
    requestVolunteers, requestJobs, requestSkills,
    requestInterests, requestChats, requestNotifications,
    requestCurrentUserProfile,
} from 'core/store/actions';
import { Tabs } from 'components';
import { Dashboard, Chat, Notifications, ProfileDetail, Settings, ProfileForm } from 'views/home/tabs';
import styles from './styles.module.css';


const Home = ({
    requestVolunteers,
    requestJobs,
    requestSkills,
    requestInterests,
    requestChats,
    requestNotifications,
    requestCurrentUserProfile,
    user,
    match,
    location,
}) => {
    useEffect(() => {
        requestCurrentUserProfile();
        requestJobs();
        requestChats();
        requestNotifications();
    }, [requestJobs, requestChats, requestNotifications, requestCurrentUserProfile])

    useEffect(() => {
        if (user.id) {
            if (user.type === 'volunteer') requestSkills();
            if (user.type === 'enterprise') {
                requestInterests();
                requestVolunteers();
            };
        }
    }, [user, requestInterests, requestVolunteers, requestSkills])

    const renderTabs = () => {
        const { pathname } = location;
        if (/\/home\/chat\/\w*/.test(pathname)) return;
        if (/\/home\/profile\/\d*\/edit/.test(pathname)) return;
        return <Tabs user={user} />
    }

    return (
        <div className={styles.root}>
            <div className={styles.container}>
                <Switch>
                    <Route exact path={match.path} render={() => <Redirect to={`${match.path}/dashboard`} />}/>
                    <Route
                        path={`${match.path}/dashboard`}
                        component={Dashboard}
                    />
                    <Route
                        path={`${match.path}/chat`}
                        component={Chat}
                    />
                    <Route
                        path={`${match.path}/notifications`}
                        component={Notifications}
                    />
                    <Route
                        path={`${match.path}/profile/:id/edit`}
                        component={ProfileForm}
                    />
                    <Route
                        path={`${match.path}/profile/:id`}
                        component={ProfileDetail}
                    />
                    <Route
                        path={`${match.path}/settings`}
                        component={Settings}
                    />
                </Switch>
            </div>
            {renderTabs()}
        </div>
    )
}

export default connect(
    state => ({
        user: state.profile.user,
    }),
    dispatch => bindActionCreators({
        requestVolunteers, requestJobs, requestSkills,
        requestInterests, requestChats, requestNotifications,
        requestCurrentUserProfile,
    }, dispatch)
)(Home);
