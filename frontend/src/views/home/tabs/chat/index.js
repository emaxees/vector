import React, { Component } from 'react';
import { Route, Switch } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import autobind from 'autobind';
import { Navbar, Frame, Container } from 'components';
import List from './list';
import View from './view';


@connect(
    state => ({
        user: state.user,
    }),
    dispatch => bindActionCreators({}, dispatch)
)
@autobind
class Chat extends Component {
    render() {
        const { match } = this.props;
        return (
            <Frame>
                <Navbar shadow text='Chat' />
                <Container noMarginBottom>
                    <Switch>
                        <Route
                            path={`${match.path}`}
                            exact
                            component={List}
                        />
                        <Route
                            path={`${match.path}/:email`}
                            component={View}
                        />
                    </Switch>
                </Container>
            </Frame>
        )
    }
}

export default Chat;