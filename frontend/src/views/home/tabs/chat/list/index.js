import React, { Component } from 'react';
import { Message } from 'components';
import { SlideUp } from 'animations';
import { Card } from 'components';
import { connect } from 'react-redux';
import Avatar from 'react-avatar';
import { bindActionCreators } from 'redux';
import { requestChats } from 'actions';
import autobind from 'autobind';
import styles from './styles.module.css';


@connect(
    state => ({
        chats: state.chats.list,
        user: state.user,
    }),
    dispatch => bindActionCreators({ requestChats }, dispatch)
)
@autobind
class List extends Component {
    handleClickChat(chat) {
        return () => {
            const { history: { push }} = this.props;
            const { match } = this.props;
            push(`${match.path}/${this.getChatUser(chat)}`)
        }
    }

    componentDidMount() {
        const { requestChats } = this.props;
        requestChats();
    }

    getChatUser(chat) {
        const { user } = this.props;
        const { sender, receiver } = chat;
        if ( receiver.id !== Number(user.id) ) return receiver.email;
        if ( sender.id !== Number(user.id) ) return sender.email;
    }

    getMessage(chat) {
        const { messages } = chat;
        if (!messages.length) return;
        return `${messages[messages.length - 1].content.slice(0,60)}...`;
    }

    render() {
        const { chats, user } = this.props;
        let text;
        if (user.type === 'volunteer') text = 'Chat will be initiated by organisation after you have applied for a position';
        else text = 'You can start a chat with applicants when they apply for your job posting';
        const props = {
            title: 'There are not chat here',
            text,
            icon: 'chat',
        };
        if (!chats.length) return <Message {...props} />;
        return (
            <div className={styles.root}>
                { chats.map((chat, index) => (
                    <SlideUp key={chat.id} initialPose="closed" pose="open" count={index}>
                        <Card key={chat.id}>
                            <div className={styles.chat} onClick={this.handleClickChat(chat)}>
                                <Avatar
                                    round
                                    size='60px'
                                    name={this.getChatUser(chat)}
                                    color={Avatar.getRandomColor('sitebase', ['red', 'orange'])}
                                />
                                <div className={styles.overview}>
                                    <div className={styles.user}>
                                        { this.getChatUser(chat) }
                                    </div>
                                    <div className={styles.messages}>
                                        {this.getMessage(chat)}
                                    </div>
                                </div>
                            </div>
                        </Card>
                    </SlideUp>
                )) }
            </div>
        )
    }
}

export default List;
