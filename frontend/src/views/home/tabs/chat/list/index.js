import React, { Component } from 'react';
import { Message, ClampLines } from 'components';
import { SlideUp } from 'animations';
import { Card } from 'components';
import { connect } from 'react-redux';
import Avatar from 'react-avatar';
import { bindActionCreators } from 'redux';
import { requestChats } from 'core/store/actions';
import styles from './styles.module.css';


class List extends Component {
    componentDidMount() {
        const { requestChats } = this.props;
        requestChats();
    }

    handleClickChat = (chat) => {
        return () => {
            const { history: { push }} = this.props;
            const { match } = this.props;
            push(`${match.path}/${chat.id}`)
        }
    }

    getChatUser = (chat) => {
        const { user } = this.props;
        const { sender, receiver } = chat;
        if (receiver.id !== Number(user.id)) return receiver.email;
        if (sender.id !== Number(user.id)) return sender.email;
    }

    getDisplayName = (chat) => {
        const { user } = this.props;
        const { sender, receiver } = chat;
        if (receiver.id !== Number(user.id)) return receiver.displayName;
        if (sender.id !== Number(user.id)) return sender.displayName;
    }

    getMessage = (chat) => {
        const { messages } = chat;
        if (!messages.length) return '';
        return `${messages[messages.length - 1].content.slice(0,60)}...`;
    }

    getReminder = (chat) => {
        const { sender, receiver } = chat;
        try {
            if ((sender.type === 'enterprise' && receiver.type === 'volunteer') || (sender.type === 'volunteer' && receiver.type === 'enterprise')) {
                const {title} = sender.jobs.find((job) => job.applicants.map((applicant) => applicant.volunteer).includes(receiver.id));
                return (
                    <div className={styles.reminder}>
                        <i className="material-icons">work</i>
                        {title}
                    </div>
                )
            }
        } catch(error) {
            return;
        }
    }

    getAvatar = (chat) => {
        const { user } = this.props;
        const { sender, receiver } = chat;
        let avatar;
        if (user.email !== receiver.email) avatar = receiver;
        else avatar = sender;
        return (
            <Avatar
                round
                src={avatar.profilePicture}
                size='60px'
                name={avatar.displayName}
                color={Avatar.getRandomColor('sitebase', ['red', 'orange'])}
            />
        );
    }

    render() {
        const { chats, user } = this.props;
        let text;
        if (user.type === 'volunteer') text = 'Chat will be initiated by organisation after you have applied for a position';
        else text = 'You can start a chat with applicants when they apply for your job posting';
        const props = {
            title: 'There is no chat here',
            text,
            icon: 'chat',
        };
        if (!chats.length) return <Message {...props} />;
        return (
            <div className={styles.root}>
                { chats.map((chat, index) => (
                    <SlideUp key={chat.id} initialPose="closed" pose="open" count={index}>
                        <Card key={chat.id}>
                            <div className={styles.chat} onClick={this.handleClickChat(chat)}>
                                <div className={styles.preview}>
                                    {this.getAvatar(chat)}
                                    <div className={styles.overview}>
                                        <div className={styles.user}>
                                            { this.getDisplayName(chat) }
                                        </div>
                                        <div className={styles.messages}>
                                            <ClampLines
                                                text={this.getMessage(chat)}
                                                lines={2}
                                                ellipsis="..."
                                                innerElement="p"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {this.getReminder(chat)}
                        </Card>
                    </SlideUp>
                )) }
            </div>
        )
    }
}

export default connect(
    state => ({
        chats: state.chats.list,
        user: state.user,
    }),
    dispatch => bindActionCreators({ requestChats }, dispatch)
)(List);
