import React, { Component } from 'react';
import WSC from 'wsc';
import { Textarea, Loader } from 'components';
import Avatar from 'react-avatar';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { requestChats } from 'core/store/actions';
import styles from './styles.module.css';


class View extends Component {
    state = {
        messages: [],
        fetched: false,
        message: '',
    };

    constructor(props) {
        super(props);
        this.chatsContainerRef = React.createRef();
    }

    componentDidMount() {
        const { chats, requestChats } = this.props;
        if (!chats.length) requestChats();
        else this.initChat();
    }

    componentDidUpdate(prevProps) {
        if (this.props.chats.length !== prevProps.chats.length) this.initChat();
    }

    initChat = () => {
        const { chats, match: { params: { id } }, user: { email } } = this.props;
        const { sender: { email: senderEmail }, receiver: { email: receiverEmail } } = chats.find((chat) => chat.id === Number(id));
        let userToChat;
        if (email !== senderEmail) userToChat = senderEmail;
        else userToChat = receiverEmail;
        WSC.waitForSocketConnection(()=> {
            WSC.initChatUser(userToChat);
            WSC.addCallbacks(this.setMessages.bind(this), this.addMessage.bind(this));
            WSC.fetchMessages();
        });
    }

    addMessage = (message) => {
        this.setState({ messages: [...this.state.messages, message]},() => {
            this.chatsContainerRef.current.scrollTo(0, this.chatsContainerRef.current.scrollHeight);
        });
    }

    setMessages = (messages) => {
        this.setState({ fetched: true, messages: messages.reverse()},() => {
            this.chatsContainerRef.current.scrollTo(0, this.chatsContainerRef.current.scrollHeight);
        });
    }

    handleClick = () => {
        const { message } = this.state;
        WSC.newChatMessage({ 'content': message });
        this.setState({ message: '' });
    }

    handleChangeInput = (message) => {
        this.setState({ message });
    }

    handleClickAvatar = (author) => () => {
        const { history: { push } } = this.props;
        push(`/profile/${author.type}/${author.id}`);
    }

    getAvatar = (message) => {
        return (
            <Avatar
                round
                src={message.author.profile_picture}
                size='40px'
                name={message.author.display_name}
                color={Avatar.getRandomColor('sitebase', ['red', 'orange'])}
                onClick={this.handleClickAvatar(message.author)}
            />
        );
    }

    renderMessage = (message) => {
        return (
            <div key={message.id} className={styles.chat}>
                <div className={styles.author}>
                    {this.getAvatar(message)}
                    {message.author.display_name}
                </div>
                <div className={styles.message}>
                    <p>{message.content}</p>
                </div>
            </div>
        )
    }

    getMessages = () => {
        const { fetched, messages } = this.state;
        if (!fetched) return <Loader />;
        if (!messages.length) return this.getDefaultMessage();
        return messages.map(message => (this.renderMessage(message)));
    }

    getDefaultMessage = () => {
        return (
            <div className={styles.chat}>
                <div className={styles.author}>
                    <Avatar
                        round
                        size='40px'
                        name="Vector App"
                        color={Avatar.getRandomColor( 'sitebase', ['red', 'orange'])}
                    />
                </div>
                <div>
                    <div className={styles.message}>
                        Hi there! Thank you
                        for your interest in
                        making an impact
                        with our
                        organisation. Our
                        HR will get in touch
                        with you shortly via
                        this chat! <br />
                        In the meantime, if
                        you have any
                        queries, send them
                        over!
                    </div>
                </div>
            </div>
        )
    }

    render() {
        const { message } = this.state;
        return (
            <div className={styles.root}>
                <div className={styles.chats} ref={this.chatsContainerRef}>
                    {this.getMessages()}
                </div>
                <div className={styles.input}>
                    <Textarea value={message} onChange={this.handleChangeInput}/>
                    <i onClick={this.handleClick} className='material-icons'>
                        send
                    </i>
                </div>
            </div>
        )
    }
}

export default connect(
    state => ({
        chats: state.chats.list,
        user: state.profile.user,
    }),
    dispatch => bindActionCreators({ requestChats }, dispatch)
)(View);
