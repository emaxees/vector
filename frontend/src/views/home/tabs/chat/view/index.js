import React, { Component } from 'react';
import WSC from 'wsc';
import { Input } from 'components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { requestChats } from 'actions';
import autobind from 'autobind';
import styles from './styles.module.css';


@connect(
    state => ({
        chats: state.chats.list,
    }),
    dispatch => bindActionCreators({ requestChats }, dispatch)
)
@autobind
class View extends Component {
    state = {
        messages: [],
        message: '',
    };

    constructor(props) {
        super(props);
        this.chatsContainerRef = React.createRef();
        const { match: { params: { email }}} =  this.props;
        WSC.waitForSocketConnection(()=>{
            WSC.initChatUser(email);
            WSC.addCallbacks(this.setMessages.bind(this), this.addMessage.bind(this));
            WSC.fetchMessages();
        });
    }

    addMessage(message) {
        this.setState({ messages: [...this.state.messages, message]},() => {
            this.chatsContainerRef.current.scrollTo(0, this.chatsContainerRef.current.scrollHeight);
        });
    }

    setMessages(messages) {
        this.setState({ messages: messages.reverse()},() => {
            this.chatsContainerRef.current.scrollTo(0, this.chatsContainerRef.current.scrollHeight);
        });
    }

    handleClick() {
        const { message } = this.state;
        WSC.newChatMessage({ 'content': message });
        this.setState({ message: '' });
    }

    handleChangeInput(message) {
        this.setState({ message });
    }

    renderMessage(message) {
        return (
            <div key={message.id} className={styles.chat}>
                <i className='material-icons'>
                    account_circle
                </i>
                <div>
                    <div className={styles.author}>{message.author}</div>
                    <div className={styles.message}>
                        {message.content}
                    </div>
                </div>
            </div>
        )
    }

    render() {
        const { messages, message } = this.state;
        return (
            <div className={styles.root}>
                <div className={styles.chats} ref={this.chatsContainerRef}>
                    {messages.map(message => (this.renderMessage(message)))}
                </div>
                <div className={styles.input}>
                    <Input value={message} onChange={this.handleChangeInput}/>
                    <i onClick={this.handleClick} className='material-icons'>
                        send
                    </i>
                </div>
            </div>
        )
    }
}

export default View;
