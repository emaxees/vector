import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Frame, Container, Navbar, Card, Message, Button } from 'components';
import { markNotificationAsRead } from 'actions';
import { bindActionCreators } from 'redux';
import { formatDate } from 'utils';
import autobind from 'autobind';
import styles from './styles.module.css';


@connect(
    state => ({
        notifications: state.notifications.list,
        user: state.user,
    }),
    dispatch => bindActionCreators({ markNotificationAsRead }, dispatch)
)
@autobind
class Notifications extends Component {

    getIcont(level) {
        if (level === 'success') return 'check_circle'
        return level
    }

    handleClickButton() {
        const { markNotificationAsRead, user: { id } } = this.props;
        markNotificationAsRead(id);
    }

    renderNotifications() {
        const { notifications } = this.props;
        if (!notifications.length) {
            const props = {

                icon: 'notification_important',
                text: `You don’t have any notifications`,
            };
            return <Message {...props} />;
        }
        return notifications.map(notification => (
            <Card key={notification.id}>
                <div className={styles.notification}>
                    <i className='material-icons'>
                        {this.getIcont(notification.level)}
                    </i>
                    <div className={styles.description}>
                        <div>{notification.verb}</div>
                        <div>{formatDate(notification.timestamp)}</div>
                    </div>
                </div>
            </Card>
        ));
    }

    renderButton() {
        const { notifications } = this.props;
        if (!notifications.filter(notification => notification.unread).length) return;
        return (
            <div className={styles['button-container']}>
                <Button onClick={this.handleClickButton} theme='circle'>
                    <i className='material-icons'>remove_red_eye</i>
                </Button>
            </div>
        );
    }

    render() {
        return (
            <Frame>
                <Navbar shadow text='Notifications' />
                <Container>
                    <div className={styles.root}>
                        {this.renderNotifications()}
                    </div>
                </Container>
                {this.renderButton()}
            </Frame>
        )
    }
}

export default Notifications;