import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom'
import { Card } from 'components'
import styles from './styles.module.css';


class List extends Component {
    handleClickLogout = () => {
        const { history: { push }} = this.props;
        localStorage.clear();
        push('/');
    };

    render() {
        const { match } = this.props;
        return (
            <Fragment>
                <Card>
                    <div className={styles.list}>
                        <div className={styles.item} onClick={this.handleClickLogout}>
                            <div>
                                <i className='material-icons'>exit_to_app</i>
                                <div>Logout</div>
                            </div>
                            <i className='material-icons'>arrow_forward_ios</i>
                        </div>
                    </div>
                </Card>
                <Card title='More'>
                    <div className={styles.list}>
                        <NavLink className={styles.item} to={`${match.path}about`}>
                            <div>
                                <i className='material-icons'>help</i>
                                <div>About</div>
                            </div>
                            <i className='material-icons'>arrow_forward_ios</i>
                        </NavLink>
                        <NavLink className={styles.item} to={`${match.path}change-password`}>
                            <div>
                                <i className='material-icons'>lock</i>
                                <div>Change Password</div>
                            </div>
                            <i className='material-icons'>arrow_forward_ios</i>
                        </NavLink>
                        <NavLink className={styles.item} to={`${match.path}feedback`}>
                            <div>
                                <i className='material-icons'>message</i>
                                <div>App feedback</div>
                            </div>
                            <i className='material-icons'>arrow_forward_ios</i>
                        </NavLink>
                    </div>
                </Card>
            </Fragment>
        )
    };
}

export default connect(
    state => ({
        user: state.user,
    }),
)(List);
