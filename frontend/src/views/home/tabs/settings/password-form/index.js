import React, { Component, Fragment } from 'react';
import { Card, Input, Button } from 'components'
import { changePassword } from 'actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import autobind from 'autobind';
import styles from './styles.module.css';


@connect(
    state => ({
        user: state.user,
    }),
    dispatch => bindActionCreators({ changePassword }, dispatch)
)
@autobind
class PasswordForm extends Component {
    state = {
        oldPassword: '',
        newPassword: '',
        reNewPassword: '',
        shouldValidate: {},
    }

    handleBlur(key) {
        const { shouldValidate } = this.state;
        return () => this.setState({ shouldValidate: { ...shouldValidate, [key]: true } });
    }

    handleChange(key) {
        return value => this.setState({ [key]: value });
    }

    handleClickButton() {
        const { changePassword } = this.props;
        const { oldPassword, newPassword } = this.state;
        changePassword(oldPassword, newPassword);
    }

    renderButton() {
        return <Button onClick={this.handleClickButton} theme='primary'>Change password</Button>;
    }

    render() {
        const { oldPassword, newPassword, reNewPassword } = this.state;
        return (
            <Fragment>
                <Card>
                    <div className={styles.form}>
                        <Input
                            label='Enter old password'
                            type='password'
                            value={oldPassword}
                            onChange={this.handleChange('oldPassword')}
                            onBlur={this.handleBlur('oldPassword')}
                        />
                        <Input
                            label='Enter new password'
                            type='password'
                            value={newPassword}
                            onChange={this.handleChange('newPassword')}
                            onBlur={this.handleBlur('newPassword')}
                        />
                        <Input
                            label='Re enter new password'
                            type='password'
                            value={reNewPassword}
                            onChange={this.handleChange('reNewPassword')}
                            onBlur={this.handleBlur('reNewPassword')}
                        />
                    </div>
                </Card>
                <div className={styles['button-container']}>
                    {this.renderButton()}
                </div>
            </Fragment>
        )
    }
}

export default PasswordForm;