import React, { Component, Fragment } from 'react';
import { Card } from 'components';
import styles from './styles.module.css';


class About extends Component {

    render() {
        return (
            <Fragment>
                <Card>
                    <div className={styles.root}>
                        <p>Vector is a job matching platform that focus on connecting skilled people to high impact organisations that needs helps in those skilled areas.</p>
                        <p>Our mission is to connect the right talent to the right job and empowering these organisations for a greater impact to society.</p>
                        <p>There are a lot of underlying problems in out work nowadays, and it's not easy to solve them unless we all work together. Hence Vector comes as platform to connect like minded people and together we are actively working on the solution.</p>
                        <p>At the end of the day, all we want is to see a world where people are being excellent to each other and helping each other to create a better world.</p>
                    </div>
                </Card>
            </Fragment>
        )
    }
}

export default About;