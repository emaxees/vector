import React, { Component } from 'react';
import { Route, Switch } from 'react-router';
import autobind from 'autobind';
import { Navbar, Container, Frame } from 'components';
import List from './list';
import About from './about';
import PasswordForm from './password-form'
import Feedback from './feedback';


@autobind
class Settings extends Component {

    render() {
        const { match } = this.props;
        return (
            <Frame>
                <Navbar shadow text='Settings' />
                <Container>
                <Switch>
                    <Route
                        path={`${match.path}/feedback`}
                        component={Feedback}
                    />
                    <Route
                        path={`${match.path}/change-password`}
                        component={PasswordForm}
                    />
                    <Route
                        path={`${match.path}/about`}
                        component={About}
                    />
                    <Route
                        path={`${match.path}/`}
                        component={List}
                    />
                    </Switch>
                </Container>
            </Frame>
        )
    }
}

export default Settings;
