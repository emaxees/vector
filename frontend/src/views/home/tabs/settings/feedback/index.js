import React, { Component, Fragment } from 'react';
import { Card, Textarea, Button, Select } from 'components'
import { submitFeedback } from 'actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { isValidField } from 'utils';
import autobind from 'autobind';
import styles from './styles.module.css';


@connect(
    state => ({
        user: state.user,
    }),
    dispatch => bindActionCreators({ submitFeedback }, dispatch)
)
@autobind
class Feedback extends Component {
    state = {
        type: 'comment',
        description: '',
    }

    getButtonDisabled() {
        const { type, description } = this.state;
        return !(isValidField(type) && isValidField(description))
    }

    handleBlur(key) {
        const { shouldValidate } = this.state;
        return () => this.setState({ shouldValidate: { ...shouldValidate, [key]: true } });
    }

    handleChange(key) {
        return value => this.setState({ [key]: value });
    }

    handleClickButton() {
        const { submitFeedback } = this.props;
        const { type, description } = this.state;
        submitFeedback({type, description});
    }

    renderButton() {
        return (
            <Button
                onClick={this.handleClickButton}
                disabled={this.getButtonDisabled()}
                theme='primary'>Submit Feedback
            </Button>
        );
    }

    render() {
        const { type, description } = this.state;
        return (
            <Fragment>
                <Card>
                    <div className={styles.form}>
                        <Select
                            label='Type of feedback'
                            options={[
                                { value:'comment', label: 'Comment' },
                                { value:'issue', label: 'Issue' },
                                { value:'improvement', label: 'Improvement' },
                                { value:'visual', label: 'Visual' },
                            ]}
                            onChange={this.handleChange('type')}
                            value={type}
                        />
                        <Textarea
                            label='Description'
                            value={description}
                            rows='6'
                            onChange={this.handleChange('description')}
                        />
                    </div>
                </Card>
                <div className={styles['button-container']}>
                    {this.renderButton()}
                </div>
            </Fragment>
        )
    }
}

export default Feedback;