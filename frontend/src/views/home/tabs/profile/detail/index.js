import React, { Component } from 'react';
import { changeProfileTab, requestReviews, requestUserProfile, resetProfileTab } from 'actions';
import { SlideUp } from 'animations';
import { Header, BrowseTab, Card, JobPreview, Review, Button, Message, Frame, Container } from 'components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import autobind from 'autobind';
import styles from './styles.module.css';


@connect(
    state => ({
        currentTab: state.app.profileTab,
        currentUser: state.user,
        user: state.profile.user,
        jobs: state.jobs.list,
        reviews: state.reviews.list
    }),
    dispatch => bindActionCreators({ changeProfileTab, requestReviews, requestUserProfile, resetProfileTab }, dispatch)
)
@autobind
class ProfileDetail extends Component {

    componentDidMount() {
        const { requestReviews, match: { params: { id } }, requestUserProfile } = this.props;
        requestUserProfile(id);
        requestReviews(id);
    }

    componentWillUnmount() {
        const { resetProfileTab } = this.props;
        resetProfileTab();
    }

    handleClickJobPreview(id) {
        return () => {
            const { history : { push } } = this.props;
            push(`/job/${id}`);
        }
    }

    handleClickEditProfile() {
        const { match, history : { push }} = this.props;
        push(`${match.url}/edit`);
    }

    handleClickProfileTab(id) {
        const { changeProfileTab } = this.props;
        changeProfileTab(id);
    }

    renderEnterpriseTabs() {
        const { currentTab } = this.props;
        const tabs = [
            { label: 'About Us', id: 1 },
            { label: 'Jobs', id: 2 },
            { label: 'Reviews', id: 3 }
        ]
        return (
            <BrowseTab
                onClick={this.handleClickProfileTab}
                tabs={tabs}
                currentTab={currentTab
            }/>
        )
    }

    renderVolunteerTabs() {
        const { currentTab } = this.props;
        const tabs = [
            { label: 'About', id: 1 },
            { label: 'Reviews', id: 2 }
        ]
        return (
            <BrowseTab
                onClick={this.handleClickProfileTab}
                tabs={tabs}
                currentTab={currentTab}
            />
        )
    }

    renderAboutUs() {
        const { user } = this.props;
        return (
            <div>
                <Card title='Company Overview'>
                    <div className={styles['card-content']}>
                        <div dangerouslySetInnerHTML={{__html: user.bio}} />
                    </div>
                </Card>
                <Card title='Interests'>
                    <div className={styles['card-content']}>
                        <div className={styles.list}>
                            {user.interests.map(interest => (
                                <span key={interest.id}>{interest.name}</span>
                            ))}
                        </div>
                    </div>
                </Card>
            </div>
        )
    }

    renderAbout() {
        const { user } = this.props;
        return (
            <div>
                <Card title='First Name'>
                    <div className={styles['card-content']}>
                        {user.firstName}
                    </div>
                </Card>
                <Card title='Last Name'>
                    <div className={styles['card-content']}>
                        {user.lastName}
                    </div>
                </Card>
                <Card title='Ocupation'>
                    <div className={styles['card-content']}>
                        {user.occupation}
                    </div>
                </Card>
                <Card title='Bio'>
                    <div className={styles['card-content']}>
                        <div dangerouslySetInnerHTML={{__html: user.bio}} />
                    </div>
                </Card>
                <Card title='Skills'>
                    <div className={styles['card-content']}>
                        <div className={styles.list}>
                            {user.skills.map(skill => (
                                <span key={skill.id}>{skill.name}</span>
                            ))}
                        </div>
                    </div>
                </Card>
            </div>
        )
    }

    renderJobs() {
        const { jobs } = this.props;
        if (!jobs.length) {
            const props = {

                icon: 'notification_important',
                text: `You don't have any jobs posted yet`,
            };
            return <Message {...props} />;
        };
        return jobs.map((job, index) => {
            return (
                <SlideUp key={job.id} initialPose="closed" pose="open" count={index}>
                    <JobPreview onClick={this.handleClickJobPreview(job.id)} key={job.id} job={job}/>
                </SlideUp>
            )
        });
    }

    renderReviews() {
        const { reviews } = this.props;
        if (!reviews.length) {
            const props = {
                icon: 'stars',
                text: `You don’t have any review`,

            };
            return <Message {...props} />;
        }
        return (
            <div className={styles.reviews}>
                {reviews.map((review, index) => (
                    <SlideUp key={review.id} initialPose="closed" pose="open" count={index}>
                        <Card key={review.id}>
                            <div className={styles['card-content']}>
                                <Review review={review} />
                            </div>
                        </Card>
                    </SlideUp>
                ))}
            </div>
        )
    }

    renderTabs() {
        const { user, currentTab } = this.props;
        if (!user) return;
        if (user.type === 'enterprise') {
            switch(currentTab) {
                case 1:
                    return this.renderAboutUs();
                case 2:
                    return this.renderJobs();
                case 3:
                    return this.renderReviews();
                default:
                    break;
            }
        } else {
            switch(currentTab) {
                case 1:
                    return this.renderAbout();
                case 2:
                    return this.renderReviews();
                default:
                    break;
            }
        }
    }

    renderView() {
        const { user } = this.props;
        if (!user) return;
        if (user.type === 'enterprise') return this.renderEnterpriseTabs();
        else return this.renderVolunteerTabs();
    }

    renderHeader() {
        const { user } = this.props;
        if (!user) return;
        return <Header user={user}/>
    }

    renderButton() {
        const { currentUser, user } = this.props
        if (user && Number(currentUser.id) !== Number(user.id)) return;
        return (
            <Button onClick={this.handleClickEditProfile} theme='circle'>
                <i className='material-icons'>
                    edit
                </i>
            </Button>
        );
    }

    render() {
        return (
            <Frame>
                {this.renderHeader()}
                {this.renderView() }
                <Container>
                    {this.renderTabs()}
                </Container>
                <div className={styles['button-container']}>
                    {this.renderButton()}
                </div>
            </Frame>
        )
    }
}

export default ProfileDetail;
