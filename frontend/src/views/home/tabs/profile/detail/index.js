import React, { Component } from 'react';
import {
    changeProfileTab,
    requestCurrentUserProfile,
    resetProfileTab
} from 'core/store/actions';
import { SlideUp } from 'animations';
import {
    Header, BrowseTab, Card,
    JobPreview, Review, Button,
    Message, Frame, Container, Loader,
} from 'components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styles from './styles.module.css';


class ProfileDetail extends Component {
    componentDidMount() {
        requestCurrentUserProfile();
    }

    componentWillUnmount() {
        const { resetProfileTab } = this.props;
        resetProfileTab();
    }

    handleClickJobPreview = (id) => {
        return () => {
            const { history : { push } } = this.props;
            push(`/job/${id}`);
        }
    }

    handleClickEditProfile = () => {
        const { match, history : { push }} = this.props;
        push(`${match.url}/edit`);
    }

    handleClickProfileTab = (id) => {
        const { changeProfileTab } = this.props;
        changeProfileTab(id);
    }

    renderEnterpriseTabs = () => {
        const { currentTab } = this.props;
        const tabs = [
            { label: 'About Us', id: 1 },
            { label: 'Jobs', id: 2 },
            { label: 'Reviews', id: 3 }
        ]
        return (
            <BrowseTab
                onClick={this.handleClickProfileTab}
                tabs={tabs}
                currentTab={currentTab
            }/>
        )
    }

    renderVolunteerTabs = () => {
        const { currentTab } = this.props;
        const tabs = [
            { label: 'About', id: 1 },
            { label: 'Reviews', id: 2 }
        ]
        return (
            <BrowseTab
                onClick={this.handleClickProfileTab}
                tabs={tabs}
                currentTab={currentTab}
            />
        )
    }

    renderAboutUs = () => {
        const { user } = this.props;
        return (
            <div>
                <Card title='Company Overview'>
                    <div className={styles['card-content']}>
                        <div dangerouslySetInnerHTML={{__html: user.bio}} />
                    </div>
                </Card>
                <Card title='Phone Number'>
                    <div className={styles['card-content']}>
                        {user.phoneNumber}
                    </div>
                </Card>
                <Card title='Interests'>
                    <div className={styles['card-content']}>
                        <div className={styles.list}>
                            {user.interests.map(interest => (
                                <span key={interest.id}>{interest.name}</span>
                            ))}
                        </div>
                    </div>
                </Card>
            </div>
        )
    }

    renderAbout = () => {
        const { user } = this.props;
        return (
            <div>
                <Card title='First Name'>
                    <div className={styles['card-content']}>
                        {user.firstName}
                    </div>
                </Card>
                <Card title='Last Name'>
                    <div className={styles['card-content']}>
                        {user.lastName}
                    </div>
                </Card>
                <Card title='Ocupation'>
                    <div className={styles['card-content']}>
                        {user.occupation}
                    </div>
                </Card>
                <Card title='Bio'>
                    <div className={styles['card-content']}>
                        <div dangerouslySetInnerHTML={{__html: user.bio}} />
                    </div>
                </Card>
                <Card title='Phone Number'>
                    <div className={styles['card-content']}>
                        {user.phoneNumber}
                    </div>
                </Card>
                <Card title='Skills'>
                    <div className={styles['card-content']}>
                        <div className={styles.list}>
                            {user.skills.map(skill => (
                                <span key={skill.id}>{skill.name}</span>
                            ))}
                        </div>
                    </div>
                </Card>
            </div>
        )
    }

    renderJobs = () => {
        const { jobs } = this.props;
        if (!jobs.length) {
            const props = {

                icon: 'notification_important',
                text: `You don't have any jobs posted yet`,
            };
            return <Message {...props} />;
        };
        return jobs.map((job, index) => {
            return (
                <SlideUp key={job.id} initialPose="closed" pose="open" count={index}>
                    <JobPreview onClick={this.handleClickJobPreview(job.id)} key={job.id} job={job}/>
                </SlideUp>
            )
        });
    }

    renderReviews = () => {
        const { user: { reviews } } = this.props;
        if (!reviews.length) {
            const props = {
                icon: 'stars',
                text: `You don’t have any reviews`,

            };
            return <Message {...props} />;
        }
        return (
            <div className={styles.reviews}>
                {reviews.map((review, index) => (
                    <SlideUp key={review.id} initialPose="closed" pose="open" count={index}>
                        <Card key={review.id}>
                            <div className={styles['card-content']}>
                                <Review review={review} />
                            </div>
                        </Card>
                    </SlideUp>
                ))}
            </div>
        )
    }

    renderTabs = () => {
        const { user, currentTab } = this.props;
        if (!user) return;
        if (user.type === 'enterprise') {
            switch(currentTab) {
                case 1:
                    return this.renderAboutUs();
                case 2:
                    return this.renderJobs();
                case 3:
                    return this.renderReviews();
                default:
                    break;
            }
        } else {
            switch(currentTab) {
                case 1:
                    return this.renderAbout();
                case 2:
                    return this.renderReviews();
                default:
                    break;
            }
        }
    }

    renderView = () => {
        const { user } = this.props;
        if (!user) return;
        if (user.type === 'enterprise') return this.renderEnterpriseTabs();
        else return this.renderVolunteerTabs();
    }

    renderHeader = () => {
        const { fetched, user } = this.props;
        return <Header full isLoading={!fetched} user={user}/>
    }

    renderButton = () => {
        return (
            <Button onClick={this.handleClickEditProfile} theme='circle'>
                <i className='material-icons'>
                    edit
                </i>
            </Button>
        );
    }

    render() {
        const { fetched } = this.props;
        return (
            <Frame>
                {this.renderHeader()}
                <Container>
                    {!fetched ? <Loader /> : this.renderTabs()}
                </Container>
                <div className={styles['button-container']}>
                    {this.renderButton()}
                </div>
            </Frame>
        )
    }
}

export default connect(
    state => ({
        currentTab: state.app.profileTab,
        user: state.profile.user,
        fetched: state.profile.user.fetched,
        jobs: state.jobs.list,
        reviews: state.reviews.list
    }),
    dispatch => bindActionCreators({ changeProfileTab, requestCurrentUserProfile, resetProfileTab }, dispatch)
)(ProfileDetail);
