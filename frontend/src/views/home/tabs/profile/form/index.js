import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
    updateUserProfileForm, updateUserProfile,
} from 'actions';
import autobind from 'autobind';
import ReactQuill from 'react-quill';
import { Navbar, Card, Input, Button, Selector, Frame, Container } from 'components';
import styles from './styles.module.css';


@connect(
    state => ({
        user: state.profile.user,
        skillsTypes: state.skills.types,
        interestsTypes: state.interests.types
    }),
    dispatch => bindActionCreators({
        updateUserProfileForm,
        updateUserProfile
    }, dispatch)
)
@autobind
class ProfileForm extends Component {
    state = {
        shouldValidate: {},
    }

    handleChange(key) {
        const { updateUserProfileForm } = this.props;
        return value => updateUserProfileForm(key, value);
    }

    handleBlur(key) {
        const { shouldValidate } = this.state;
        return () => this.setState({ shouldValidate: { ...shouldValidate, [key]: true } });
    }

    handleClickEditJob() {
        const { updateUserProfile, user: { type, administratorName, bio, companyName, email, id, firstName, lastName, occupation, skills, interests } } = this.props;
        let form = {}
        if (type === 'enterprise') form = { administratorName, bio, companyName, email, interests: interests.map(interest => interest.id) };
        else form = { firstName, lastName, occupation, bio, email, skills: skills.map(skill => skill.skill.id) };
        updateUserProfile({type, profile: form, id });
    }

    renderButton() {
        return <Button onClick={this.handleClickEditJob} theme='primary'>Save</Button>;
    }

    renderContent() {
        const { user } = this.props;
        if (user.type === 'volunteer') return this.renderVolunteerForm();
        return this.renderEnterpriseForm();
    }

    handleAddSkill(value) {
        const { user, updateUserProfileForm } = this.props;
        updateUserProfileForm('skills', [...user.skills, { skill: { id: value.skill } }]);
    }

    handleRemoveSkill(id) {
        const { user, updateUserProfileForm } = this.props;
        updateUserProfileForm('skills', user.skills.filter(skill => skill.skill.id !== id));
    }

    handleAddInterest(value) {
        const { user, updateUserProfileForm } = this.props;
        updateUserProfileForm('interests', [...user.interests, { id: value.skill }]);
    }

    handleRemoveInterest(id) {
        const { user, updateUserProfileForm } = this.props;
        updateUserProfileForm('interests', user.interests.filter(interest => interest.id !== id));
    }

    renderVolunteerForm() {
        const { user: { firstName, lastName, occupation, bio, email, skills }, skillsTypes } = this.props;
        return (
            <Fragment>
                <Card title='First Name'>
                    <div className={styles['card-content']}>
                        <Input
                            type="text"
                            value={firstName}
                            onChange={this.handleChange('firstName')}
                            onBlur={this.handleBlur('firstName')}
                        />
                    </div>
                </Card>
                <Card title='Last Name'>
                    <div className={styles['card-content']}>
                        <Input
                            type="text"
                            value={lastName}
                            onChange={this.handleChange('lastName')}
                            onBlur={this.handleBlur('lastName')}
                        />
                    </div>
                </Card>
                <Card title='Occupation'>
                    <div className={styles['card-content']}>
                        <Input
                            type="text"
                            value={occupation}
                            onChange={this.handleChange('occupation')}
                            onBlur={this.handleBlur('occupation')}
                        />
                    </div>
                </Card>
                <Card title= 'Bio'>
                    <div className={styles['card-content']}>
                        <ReactQuill
                            value={bio}
                            theme='snow'
                            onChange={this.handleChange('bio')}
                        />
                    </div>
                </Card>
                <Card title='Email'>
                    <div className={styles['card-content']}>
                        <Input
                            type="text"
                            value={email}
                            onChange={this.handleChange('email')}
                            onBlur={this.handleBlur('email')}
                        />
                    </div>
                </Card>
                <Card title='Skills'>
                    <div className={styles['card-content']}>
                        <Selector
                            placeholder='Type some Skill'
                            list={skillsTypes}
                            selected={skills.map(skill => ({skill: skill.skill.id, points: skill.skill.points}))}
                            onAdd={this.handleAddSkill}
                            onRemove={this.handleRemoveSkill}
                            hideSelect
                        />
                    </div>
                </Card>
            </Fragment>
        );
    }

    renderEnterpriseForm() {
        const { user: { administratorName, bio, companyName, email, interests }, interestsTypes } = this.props;
        return (
            <Fragment>
                <Card title='Administrator Name'>
                    <div className={styles['card-content']}>
                        <Input
                            type="text"
                            value={administratorName}
                            onChange={this.handleChange('administratorName')}
                            onBlur={this.handleBlur('administratorName')}
                        />
                    </div>
                </Card>
                <Card title= 'Bio'>
                    <div className={styles['card-content']}>
                        <ReactQuill
                            value={bio}
                            theme='snow'
                            onChange={this.handleChange('bio')}
                        />
                    </div>
                </Card>
                <Card title='Company Name'>
                    <div className={styles['card-content']}>
                        <Input
                            type="text"
                            value={companyName}
                            onChange={this.handleChange('companyName')}
                            onBlur={this.handleBlur('companyName')}
                        />
                    </div>
                </Card>
                <Card title='Email'>
                    <div className={styles['card-content']}>
                        <Input
                            type="text"
                            value={email}
                            onChange={this.handleChange('email')}
                            onBlur={this.handleBlur('email')}
                        />
                    </div>
                </Card>
                <Card title='Interests'>
                    <div className={styles['card-content']}>
                        <Selector
                            placeholder='Type some interest'
                            list={interestsTypes}
                            selected={interests.map(interest => ({ skill: interest.id }))}
                            onAdd={this.handleAddInterest}
                            onRemove={this.handleRemoveInterest}
                            hideSelect
                        />
                    </div>
                </Card>
            </Fragment>
        );
    }

    render() {
        return (
            <Frame>
                <Navbar shadow text='Edit Profile'/>
                <Container>
                    {this.renderContent()}
                </Container>
                <div className={styles['button-container']}>
                    {this.renderButton()}
                </div>
            </Frame>
        );
    }
}

export default ProfileForm;
