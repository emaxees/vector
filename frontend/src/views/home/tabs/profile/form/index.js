import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
    updateUserProfileForm, updateUserProfile, requestCurrentUserProfile,
} from 'core/store/actions';
import ReactQuill from 'react-quill';
import { JSONToFormData } from 'core/utils';
import {
    Navbar, Card, Input,
    Button, Selector, Frame,
    Container, Loader
} from 'components';
import styles from './styles.module.css';


class ProfileForm extends Component {
    state = {
        shouldValidate: {},
    }

    componentDidMount() {
        const { requestCurrentUserProfile } = this.props;
        requestCurrentUserProfile();
    }

    componentWillUnmount() {
        const { requestCurrentUserProfile } = this.props;
        requestCurrentUserProfile();
    }

    handleChange = (key) => {
        const { updateUserProfileForm } = this.props;
        return value => updateUserProfileForm(key, value);
    }

    handleBlur = (key) => {
        const { shouldValidate } = this.state;
        return () => this.setState({ shouldValidate: { ...shouldValidate, [key]: true } });
    }

    handleClickEditProfile = () => {
        const {
            updateUserProfile,
            user: {
                type,
                administratorName,
                bio,
                companyName,
                email,
                id,
                firstName,
                lastName,
                occupation,
                skills,
                interests,
                profilePicture,
                phoneNumber,
            }
        } = this.props;
        let data = { bio, email, phoneNumber }
        if (type === 'enterprise') {
            data = {
                ...data,
                administratorName,
                companyName,
                email,
                interests: JSON.stringify(interests.map(interest => interest.id)),
            };
        }
        else {
            data = {
                ...data,
                firstName,
                lastName,
                occupation,
                skills: JSON.stringify(skills.map(skill => skill.skill.id)),
            };
        }
        if (typeof profilePicture !== 'string') data = { ...data, profilePicture };
        const form = JSONToFormData(data);
        updateUserProfile({type, profile: form, id });
    }

    renderButton = () => {
        return <Button onClick={this.handleClickEditProfile} theme='primary'>Save</Button>;
    }

    renderContent = () => {
        const { user } = this.props;
        if (user.type === 'volunteer') return this.renderVolunteerForm();
        return this.renderEnterpriseForm();
    }

    handleAddSkill = (value) => {
        const { user, updateUserProfileForm } = this.props;
        updateUserProfileForm('skills', [...user.skills, { skill: { id: value.skill } }]);
    }

    handleRemoveSkill = (id) => {
        const { user, updateUserProfileForm } = this.props;
        updateUserProfileForm('skills', user.skills.filter(skill => skill.skill.id !== id));
    }

    handleAddInterest = (value) => {
        const { user, updateUserProfileForm } = this.props;
        updateUserProfileForm('interests', [...user.interests, { id: value.skill }]);
    }

    handleRemoveInterest = (id) => {
        const { user, updateUserProfileForm } = this.props;
        updateUserProfileForm('interests', user.interests.filter(interest => interest.id !== id));
    }

    handleImageChange = ($event) => {
        const { updateUserProfileForm } = this.props;
        $event.preventDefault();
        let reader = new FileReader();
        let file = $event.target.files[0];
        reader.onloadend = () => updateUserProfileForm('profilePicture', file);
        reader.readAsDataURL(file);
    }

    renderVolunteerForm = () => {
        const { user: { firstName, lastName, occupation, bio, email, skills, phoneNumber }, skillsTypes } = this.props;
        return (
            <Fragment>
                <Card title='First Name'>
                    <div className={styles['card-content']}>
                        <Input
                            type="text"
                            value={firstName}
                            onChange={this.handleChange('firstName')}
                            onBlur={this.handleBlur('firstName')}
                        />
                    </div>
                </Card>
                <Card title='Last Name'>
                    <div className={styles['card-content']}>
                        <Input
                            type="text"
                            value={lastName}
                            onChange={this.handleChange('lastName')}
                            onBlur={this.handleBlur('lastName')}
                        />
                    </div>
                </Card>
                <Card title='Occupation'>
                    <div className={styles['card-content']}>
                        <Input
                            type="text"
                            value={occupation}
                            onChange={this.handleChange('occupation')}
                            onBlur={this.handleBlur('occupation')}
                        />
                    </div>
                </Card>
                <Card title= 'Bio'>
                    <div className={styles['card-content']}>
                        <ReactQuill
                            value={bio}
                            theme='snow'
                            onChange={this.handleChange('bio')}
                        />
                    </div>
                </Card>
                <Card title='Email'>
                    <div className={styles['card-content']}>
                        <Input
                            type="text"
                            value={email}
                            onChange={this.handleChange('email')}
                            onBlur={this.handleBlur('email')}
                        />
                    </div>
                </Card>
                <Card title='Skills'>
                    <div className={styles['card-content']}>
                        <Selector
                            placeholder='Type some Skill'
                            list={skillsTypes}
                            selected={skills.map(skill => ({skill: skill.skill.id, points: skill.skill.points}))}
                            onAdd={this.handleAddSkill}
                            onRemove={this.handleRemoveSkill}
                            hideSelect
                        />
                    </div>
                </Card>
                <Card title='Phone Number'>
                    <div className={styles['card-content']}>
                        <Input
                            type="text"
                            value={phoneNumber}
                            onChange={this.handleChange('phoneNumber')}
                            onBlur={this.handleBlur('phoneNumber')}
                        />
                    </div>
                </Card>
                <Card title='Profile Picture'>
                    <div className={styles['card-content']}>
                        <input
                            type="file"
                            accept="image/*"
                            onChange={this.handleImageChange}
                            onBlur={this.handleBlur('profilePicture')}
                        />
                    </div>
                </Card>
            </Fragment>
        );
    }

    renderEnterpriseForm() {
        const { user: { administratorName, bio, companyName, email, interests, phoneNumber }, interestsTypes } = this.props;
        return (
            <Fragment>
                <Card title='Administrator Name'>
                    <div className={styles['card-content']}>
                        <Input
                            type="text"
                            value={administratorName}
                            onChange={this.handleChange('administratorName')}
                            onBlur={this.handleBlur('administratorName')}
                        />
                    </div>
                </Card>
                <Card title= 'Bio'>
                    <div className={styles['card-content']}>
                        <ReactQuill
                            value={bio}
                            theme='snow'
                            onChange={this.handleChange('bio')}
                        />
                    </div>
                </Card>
                <Card title='Company Name'>
                    <div className={styles['card-content']}>
                        <Input
                            type="text"
                            value={companyName}
                            onChange={this.handleChange('companyName')}
                            onBlur={this.handleBlur('companyName')}
                        />
                    </div>
                </Card>
                <Card title='Email'>
                    <div className={styles['card-content']}>
                        <Input
                            type="text"
                            value={email}
                            onChange={this.handleChange('email')}
                            onBlur={this.handleBlur('email')}
                        />
                    </div>
                </Card>
                <Card title='Interests'>
                    <div className={styles['card-content']}>
                        <Selector
                            placeholder='Type some interest'
                            list={interestsTypes}
                            selected={interests.map(interest => ({ skill: interest.id }))}
                            onAdd={this.handleAddInterest}
                            onRemove={this.handleRemoveInterest}
                            hideSelect
                        />
                    </div>
                </Card>
                <Card title='Phone Number'>
                    <div className={styles['card-content']}>
                        <Input
                            type="text"
                            value={phoneNumber}
                            onChange={this.handleChange('phoneNumber')}
                            onBlur={this.handleBlur('phoneNumber')}
                        />
                    </div>
                </Card>
                <Card title='Profile Picture'>
                    <div className={styles['card-content']}>
                        <input
                            type="file"
                            accept="image/*"
                            onChange={this.handleImageChange}
                            onBlur={this.handleBlur('profilePicture')}
                        />
                    </div>
                </Card>
            </Fragment>
        );
    }

    render() {
        const { fetched } = this.props;
        return (
            <Frame>
                <Navbar shadow text='Edit Profile'/>
                <Container>
                { fetched ? this.renderContent() : <Loader /> }
                </Container>
                <div className={styles['button-container']}>
                    {this.renderButton()}
                </div>
            </Frame>
        );
    }
}

export default connect(
    state => ({
        fetched: state.profile.user.fetched,
        user: state.profile.user,
        skillsTypes: state.skills.types,
        interestsTypes: state.interests.types
    }),
    dispatch => bindActionCreators({
        updateUserProfileForm,
        updateUserProfile,
        requestCurrentUserProfile,
    }, dispatch)
)(ProfileForm);
