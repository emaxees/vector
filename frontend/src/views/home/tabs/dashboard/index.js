import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SlideUp } from 'animations';
import autobind from 'autobind';
import { changeBrowseTab } from 'actions';
import { JobPreview, VolunteerPreview, Button, Header, BrowseTab, Frame, Container, Message } from 'components';
import styles from './styles.module.css';


@connect(
    state => ({
        user: state.user,
        volunteers: state.volunteers.list,
        currentTab: state.app.browseTab,
        jobs: state.jobs.list,
    }),
    dispatch => bindActionCreators({ changeBrowseTab }, dispatch)
)
@autobind
class Dashboard extends Component {
    handleClickJobPreview(id) {
        return () => {
            const { history : { push } } = this.props;
            push(`/job/${id}`);
        }
    }

    renderEnterpriseContent() {
        const { currentTab } = this.props;
        if (currentTab === 1) return this.renderBroserVolunteers();
        else return this.renderJobListings();
    }

    renderJobListings() {
        const { jobs, user } = this.props;
        if (!jobs.length) {
            let text;
            if (user.type === 'enterprise') text = 'Simply click the + button on the corner';
            else text = 'There are not jobs posted!';
            const props = {
                icon: 'notification_important',
                text
            };
            return <Message {...props} />;
        };
        return jobs.map((job, index) => {
            return (
                <SlideUp key={job.id} initialPose="closed" pose="open" count={index}>
                    <JobPreview onClick={this.handleClickJobPreview(job.id)} job={job}/>
                </SlideUp>
            );
        });
    }

    renderJobsApplied() {
        const { jobs, user } = this.props;
        const applied = jobs.filter(job => job.applicants.map(applicant => applicant.volunteer.id).includes(Number(user.id)));
        if (!applied.length) {
            const props = {
                icon: 'notification_important',
                text: `You have not applied to any job`,
            };
            return <Message {...props} />;
        };
        return applied.map((job, index) => {
            return (
                <SlideUp key={job.id} initialPose="closed" pose="open" count={index}>
                    <JobPreview onClick={this.handleClickJobPreview(job.id)} job={job}/>
                </SlideUp>
            );
        });
    }

    renderVolunteerContent() {
        const { currentTab } = this.props;
        if (currentTab === 1) return this.renderJobListings();
        return this.renderJobsApplied();
    }

    renderBroserVolunteers() {
        const { volunteers } = this.props;
        return volunteers.map((volunteer, index) => {
            return (
                <SlideUp key={volunteer.id} initialPose="closed" pose="open" count={index}>
                    <VolunteerPreview volunteer={volunteer} />
                </SlideUp>
            );
        });
    }

    hangleClickTab(id) {
        return () => this.setState({ tab: id });
    }

    handleClickButton() {
        const { history:{ push } } = this.props;
        push('/job/add')
    }

    getTabs() {
        const { user } = this.props;
        if (user.type === 'enterprise') return [{ label: 'Volunteers', id: 1 },{ label: 'Jobs', id: 2 }];
        else return [{ label: 'Jobs', id: 1 },{ label: 'Applied', id: 2 }]
    }

    renderButton() {
        const { currentTab, user } = this.props;
        if (currentTab === 2 && user.type === 'enterprise') {
            return (
                <div className={styles['button-container']}>
                    <Button onClick={this.handleClickButton} theme='circle'><i className='material-icons'>add</i></Button>
                </div>
            );
        }
    }

    handleClickBrowseTab(id) {
        const { changeBrowseTab } = this.props;
        changeBrowseTab(id);
    }

    render() {
        const { user, currentTab } = this.props;
        return (
            <Frame>
                <Header user={user}/>
                <BrowseTab tabs={this.getTabs()} onClick={this.handleClickBrowseTab} currentTab={currentTab}/>
                <Container>
                    <div className={styles.list}>
                        { user.type === 'volunteer' ? this.renderVolunteerContent() : this.renderEnterpriseContent() }
                    </div>
                </Container>
                {this.renderButton()}
            </Frame>
        )
    }
}

export default Dashboard;