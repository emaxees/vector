import React, { Component } from 'react';
import { connect } from 'react-redux';
import {trackEvent} from 'core/tracking';
import uuidv4 from 'uuid/v4';
import { bindActionCreators } from 'redux';
import { changeBrowseTab, requestVolunteersNextPage } from 'core/store/actions';
import {
    JobPreview, VolunteerPreview, Button,
    Header, BrowseTab, Frame,
    Container, Message, Loader, InfiniteScroll,
} from 'components';
import styles from './styles.module.css';


class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.listId = uuidv4();
    }

    handleClickJobPreview = (id) => {
        return () => {
            const { history : { push } } = this.props;
            push(`/job/${id}`);
        }
    }

    renderEnterpriseContent = () => {
        const { currentTab } = this.props;
        if (currentTab === 1) return this.renderBroserVolunteers();
        else return this.renderJobListings();
    }

    renderJobListings = () => {
        const { jobs, user, isJobsFetched } = this.props;
        if (!isJobsFetched) return <Loader style={{height: '100%'}} />;
        if (!jobs.length) {
            let text;
            if (user.type === 'enterprise') text = 'Simply click the + button on the corner';
            else text = 'There are no jobs posted!';
            const props = {
                icon: 'notification_important',
                text
            };
            return <Message {...props} />;
        };
        if (user.type === 'enterprise') {
            return jobs.filter(job => !job.finished).map((job) => (
                <JobPreview key={job.id} onClick={this.handleClickJobPreview(job.id)} job={job}/>
            ));
        } else {
            return jobs.filter(job => !job.finished && !job.inProgress).map((job) => (
                <JobPreview key={job.id} onClick={this.handleClickJobPreview(job.id)} job={job}/>
            ));
        }

    }

    renderJobsApplied = () => {
        const { jobs, user } = this.props;
        const applied = jobs.filter(job => job.applicants.map(applicant => applicant.volunteer.id).includes(Number(user.id)));
        if (!applied.length) {
            const props = {
                icon: 'notification_important',
                text: `You have not applied to any job`,
            };
            return <Message {...props} />;
        };
        return applied.map((job) => (
            <JobPreview key={job.id} onClick={this.handleClickJobPreview(job.id)} job={job}/>
        ));
    }

    renderVolunteerContent = () => {
        const { currentTab } = this.props;
        if (currentTab === 1) return this.renderJobListings();
        return this.renderJobsApplied();
    }

    handleNext = () => {
        const { requestVolunteersNextPage, current } = this.props;
        requestVolunteersNextPage(current+1)
    }

    renderBroserVolunteers = () => {
        const { volunteers, isVolunteersFetched, count } = this.props;
        if (!isVolunteersFetched) return <Loader style={{height: '100%'}}/>;
        return (
            <InfiniteScroll
                className={styles.list}
                dataLength={volunteers.length}
                next={this.handleNext}
                hasMore={volunteers.length < count}
                loader={<div className={styles['load-more-loader']}><Loader style={{marginTop: '2rem', height: '10px' }}/></div>}
                scrollableTarget={this.listId}
                endMessage={
                    <p style={{textAlign: 'center'}}>
                    <b>Yay! You have seen it all</b>
                    </p>
                }
            >
                {volunteers.map((volunteer) => (<VolunteerPreview key={volunteer.id} volunteer={volunteer} />))}
            </InfiniteScroll>
        )
    }

    hangleClickTab = (id) => {
        return () => this.setState({ tab: id });
    }

    handleClickButton = () => {
        const { history:{ push } } = this.props;
        trackEvent('addJob')
        push('/job/add')
    }

    getTabs = () => {
        const { user } = this.props;
        if (user.type === 'enterprise') return [{ label: 'Volunteers', id: 1 },{ label: 'Jobs', id: 2 }];
        else return [{ label: 'Jobs', id: 1 },{ label: 'Applied', id: 2 }]
    }

    renderButton = () => {
        const { currentTab, user } = this.props;
        if (currentTab === 2 && user.type === 'enterprise') {
            return (
                <div className={styles['button-container']}>
                    <Button onClick={this.handleClickButton} theme='circle'><i className='material-icons'>add</i></Button>
                </div>
            );
        }
    }

    handleClickBrowseTab = (id) => {
        const { changeBrowseTab } = this.props;
        changeBrowseTab(id);
    }

    renderContent = () => {
        const { isUserFetched, user, currentTab } = this.props;
        if (!isUserFetched) return <Loader style={{height: '100%'}}/>;
        return (
            <>
                <BrowseTab
                    tabs={this.getTabs()}
                    onClick={this.handleClickBrowseTab}
                    currentTab={currentTab}
                />
                <Container className={styles.list} id={this.listId}>
                    { user.type === 'volunteer' ? this.renderVolunteerContent() : this.renderEnterpriseContent() }
                </Container>
            </>
        );
    }

    render() {
        const { user, isUserFetched } = this.props;
        return (
            <Frame>
                <Header isLoading={!isUserFetched} user={user}/>
                {this.renderContent()}
                {this.renderButton()}
            </Frame>
        )
    }
}

export default connect(
    state => ({
        user: state.profile.user,
        isUserFetched: state.profile.user.fetched,
        volunteers: state.volunteers.list,
        count: state.volunteers.count,
        current: state.volunteers.current,
        currentTab: state.app.browseTab,
        jobs: state.jobs.list,
        isJobsFetched: state.jobs.fetched,
        isVolunteersFetched: state.volunteers.fetched,
    }),
    dispatch => bindActionCreators({ changeBrowseTab, requestVolunteersNextPage }, dispatch)
)(Dashboard);
