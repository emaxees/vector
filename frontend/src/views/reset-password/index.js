import React, { Component } from 'react';
import { Button, Input, Navbar } from 'components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { isValidField } from 'core/utils';
import {
    updateLoginForm, requestValidateToken, requestResetPassword,
} from 'core/store/actions';
import styles from './styles.module.css';
import logo from './assets/logo.svg'


class ResetPassword extends Component {
    state = {
        shouldValidate: {},
    }
    componentDidMount = () => {
        const { requestValidateToken } = this.props;
        const { match: { params: { token } } } = this.props;
        requestValidateToken(token)
    }

    getButtonDisabled = () => {
        const { confirmPassword, password } = this.props.login;
        return !(isValidField(confirmPassword) && isValidField(password) && password === confirmPassword);
    }

    handleBlur = (key) => {
        const { shouldValidate } = this.state;
        return () => this.setState({ shouldValidate: { ...shouldValidate, [key]: true } });
    }

    handleFocus = () => {
        window.scroll(0, 0);
    }

    handleChange = (key) => {
        const { updateLoginForm } = this.props;
        return value => updateLoginForm(key, value);
    }

    handleSubmit = ($event) => {
        $event.preventDefault();
        const { match: { params: { token } } } = this.props;
        const { requestResetPassword } = this.props;
        const { password } = this.props.login;
        requestResetPassword(password, token)
    }

    render() {
        const { login: { confirmPassword, password, message } } = this.props;
        return (
            <div className={styles.root}>
                <Navbar />
                <div className={styles.container}>
                    <img className={styles.logo} alt='logo' src={logo} />
                        <br />
                    <form onSubmit={this.handleSubmit}>
                        <Input
                            placeholder='Enter New Password'
                            type='password'
                            value={password}
                            onChange={this.handleChange('password')}
                        />
                        <br />
                        <Input
                            placeholder='Confirm Password'
                            type='password'
                            value={confirmPassword}
                            onChange={this.handleChange('confirmPassword')}
                        />
                        <br />
                        <Button theme='primary' disabled={this.getButtonDisabled()} type='submit'>
                            Send
                        </Button>
                        { message &&  Object.keys(message).map(key => message[key].map(error => <p>{error}</p>))}
                    </form>
                </div>
            </div>
        )
    }
}

export default connect(
    state => ({
        login: state.login,
    }),
    dispatch => bindActionCreators({ updateLoginForm, requestValidateToken, requestResetPassword }, dispatch),
)(ResetPassword);