import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
    requestSkills, requestInterests, addSkill, removeSkill,
    updateInterests, updateSkills, addInterest, removeInterest
} from 'actions';
import { Title, Button, Checkbox } from 'components';
import autobind from 'autobind';
import styles from './styles.module.css';


@connect(
    state => ({
        user: state.user,
        skills: state.skills,
        interests: state.interests
    }),
    dispatch => bindActionCreators(
        {
            requestSkills, requestInterests, addSkill, removeSkill,
            updateSkills, addInterest, removeInterest, updateInterests
        },
        dispatch
    ),
)
@autobind
class Guide extends Component {

    componentDidMount() {
        const { requestSkills, requestInterests, user: { type } } =  this.props;
        if (type === 'volunteer') requestSkills();
        if (type === 'enterprise') requestInterests();
    }

    handleChange($event) {
        const { addSkill, removeSkill } = this.props;
        const { target: { value, checked } } = $event;
        if (checked) addSkill(Number(value));
        else removeSkill(Number(value));
    }

    handleClick() {
        const { user: { id, type } } = this.props;
        if (type === 'volunteer') {
            const { updateSkills, skills : { selected } } = this.props;
            updateSkills({ skills: selected, id });
        }
        if (type === 'enterprise') {
            const { updateInterests, interests: { selected } } = this.props;
            updateInterests({ interests: selected, id  })
        }
    }

    handleClickInterest(id) {
        return () => {
            const { addInterest, removeInterest, interests: { selected } } = this.props;
            if (selected.includes(id)) removeInterest(Number(id));
            else addInterest(Number(id));
        }
    }

    renderInterest() {
        const { interests : { types, selected } } = this.props;
        return(
            <Fragment>
                <div className={styles.header}>
                    <Title>Welcome To Vector!</Title>
                    <p>Select one or more areas that interest you</p>
                </div>
                <div className={styles.grid}>
                    {types.map(type => (
                        <div onClick={this.handleClickInterest(type.id)} key={type.id} className={styles.square}>
                            <div>
                                { type.name }
                            </div>
                            {selected.includes(type.id) && (
                                <div className={styles.checked}>
                                    <i className='material-icons'>
                                        check_circle
                                    </i>
                                    Selected
                                </div>)
                            }
                        </div>
                    ))}
                </div>
            </Fragment>
        );
    }

    renderSkills() {
        const {skills : { types, selected }} = this.props;
        return (
            <Fragment>
                <div className={styles.header}>
                    <Title>Welcome To Vector!</Title>
                    <p>Select one or more skills that are you good at</p>
                </div>
                <div className={styles.container}>
                   <ul>
                        {types.map(type => (
                            <li key={type.id}>
                                <Checkbox
                                    value={type.id}
                                    id={type.id}
                                    label={type.name}
                                    onChange={this.handleChange}
                                    checked={selected.includes(type.id)}
                                />
                            </li>
                        ))}
                   </ul>
                </div>
            </Fragment>
        );
    }

    render() {
        const { user: { type }} = this.props;
        return (
            <div className={styles.root}>
                { type === 'volunteer' ? this.renderSkills() : this.renderInterest()}
                <div className={styles['button-container']}>
                    <Button
                        theme='primary'
                        onClick={this.handleClick}
                    >
                        Continue
                    </Button>
                </div>
            </div>
        )
    }
}

export default Guide;
