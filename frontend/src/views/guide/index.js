import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import {trackEvent} from 'core/tracking';
import { bindActionCreators } from 'redux';
import {
    requestSkills, requestInterests, addSkill, removeSkill,
    updateInterests, updateSkills, addInterest, removeInterest,
    requestCurrentUserProfile,
} from 'core/store/actions';
import { Title, Button, Checkbox } from 'components';
import styles from './styles.module.css';


const Guide = ({
    user,
    skills,
    interests,
    requestSkills,
    requestInterests,
    addSkill,
    removeSkill,
    updateSkills,
    addInterest,
    removeInterest,
    updateInterests,
    requestCurrentUserProfile,
}) => {
    useEffect(() => {
        requestCurrentUserProfile();
    }, [requestCurrentUserProfile])

    useEffect(() => {
        if (user.type === 'volunteer') requestSkills();
        if (user.type === 'enterprise') requestInterests();
    },[user, requestSkills, requestInterests])

    const handleChange = ($event) => {
        const { target: { value, checked } } = $event;
        if (checked) addSkill(Number(value));
        else removeSkill(Number(value));
    }

    const handleClick = () => {
        const { id, type } = user;
        if (type === 'volunteer') {
            const { selected } = skills;
            updateSkills({ skills: selected, id });
            trackEvent('updateSkills')
        }
        if (type === 'enterprise') {
            const { selected } = interests;
            updateInterests({ interests: selected, id  })
            trackEvent('updateInterests')
        }
    }

    const handleClickInterest = (id) => () => {
        const { selected } = interests;
        if (selected.includes(id)) removeInterest(Number(id));
        else addInterest(Number(id));
    }

    const renderInterest = () => {
        const { types, selected } = interests;
        return(
            <>
                <div className={styles.header}>
                    <Title>Welcome To Vector!</Title>
                    <p>Select one or more areas that interest you</p>
                </div>
                <div className={styles.grid}>
                    {types.map(type => (
                        <div onClick={handleClickInterest(type.id)} key={type.id} className={styles.square}>
                            <div>
                                { type.name }
                            </div>
                            {selected.includes(type.id) && (
                                <div className={styles.checked}>
                                    <i className='material-icons'>
                                        check_circle
                                    </i>
                                    Selected
                                </div>)
                            }
                        </div>
                    ))}
                </div>
            </>
        );
    }

    const renderSkills = () => {
        const { types, selected } = skills;
        return (
            <>
                <div className={styles.header}>
                    <Title>Welcome To Vector!</Title>
                    <p>Select one or more skills that you are good at</p>
                </div>
                <div className={styles.container}>
                   <ul>
                        {types.map(type => (
                            <li key={type.id}>
                                <Checkbox
                                    value={type.id}
                                    id={type.id}
                                    label={type.name}
                                    onChange={handleChange}
                                    checked={selected.includes(type.id)}
                                />
                            </li>
                        ))}
                   </ul>
                </div>
            </>
        );
    }

    return (
        <div className={styles.root}>
            { user.type === 'volunteer' ? renderSkills() : renderInterest()}
            <div className={styles['button-container']}>
                <Button
                    theme='primary'
                    onClick={handleClick}
                >
                    Continue
                </Button>
            </div>
        </div>
    );
}
export default connect(
    (state) => ({
        user: state.profile.user,
        skills: state.skills,
        interests: state.interests
    }),
    (dispatch) => bindActionCreators({
        requestSkills, requestInterests,
        addSkill, removeSkill,
        updateSkills, addInterest,
        removeInterest, updateInterests,
        requestCurrentUserProfile,
    }, dispatch),
)(Guide)

