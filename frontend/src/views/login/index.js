import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Button, Title, Input, Navbar } from 'components';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { isValidField } from 'utils';
import { updateLoginForm, requestLogin } from 'actions';
import autobind from 'autobind';
import styles from './styles.module.css';
import logo from './assets/logo.svg'


@connect(
    state => ({
        login: state.login,
    }),
    dispatch => bindActionCreators({ updateLoginForm, requestLogin }, dispatch),
)
@autobind
class Login extends Component {
    state = {
        shouldValidate: {},
    }

    getButtonDisabled() {
        const { email, password } = this.props.login;
        return !(isValidField(email) && isValidField(password));
    }

    handleBlur(key) {
        const { shouldValidate } = this.state;
        return () => this.setState({ shouldValidate: { ...shouldValidate, [key]: true } });
    }

    handleFocus() {
        window.scroll(0, 0);
    }

    handleChange(key) {
        const { updateLoginForm } = this.props;
        return value => updateLoginForm(key, value);
    }

    handleSubmit($event) {
        $event.preventDefault();
        const { requestLogin } = this.props;
        const { email, password } = this.props.login;
        requestLogin({ email, password })
    }

    render() {
        const { login: { email, password, message } } = this.props;
        return (
            <div className={styles.root}>
                <Navbar />
                <div className={styles.container}>
                    <img className={styles.logo} alt='logo' src={logo} />
                    <Title>Welcome Back</Title>
                    <form onSubmit={this.handleSubmit}>
                        <Input
                            placeholder='Email'
                            type="text"
                            value={email}
                            onChange={this.handleChange('email')}
                            onBlur={this.handleBlur('email')}
                        />
                        <Input
                            placeholder='Password'
                            type='password'
                            value={password}
                            onChange={this.handleChange('password')}
                            onBlur={this.handleBlur('password')}
                        />
                        <Button theme='primary' disabled={this.getButtonDisabled()} type='submit'>
                            Log In
                        </Button>
                    </form>
                    { message &&  Object.keys(message).map(key => message[key].map(error => <p>{error}</p>))}
                    <div className={styles.link}> Forgotten your password?</div>
                    {/* <Line>Or</Line>
                    <div className={styles.socialSignUp}>
                        <Button theme='facebook'>
                            Continue with Facebook
                        </Button>
                        <Button theme='google'>
                            Continue with Google
                        </Button>
                    </div> */}
                    <div className={styles.link}> Don't have an account, <NavLink to='/signup'>Login Here</NavLink></div>
                </div>
            </div>
        )
    }
}

export default Login;