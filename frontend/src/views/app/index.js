import React, { Component } from 'react';
import Responsive from 'react-responsive-decorator';
import { Route, Switch, Redirect } from 'react-router';
import { Popup } from 'components';
import {trackPageView} from 'core/tracking';
import Welcome from 'views/welcome';
import ForgetPassword from 'views/forget-password'
import ResetPassword from 'views/reset-password'
import Home from 'views/home';
import Login from 'views/login';
import SignUp from 'views/sign-up';
import Guide from 'views/guide';
import Job from 'views/job';
import Profile from 'views/profile';
import PrivacyPolicy from 'views/privacy-policy';
import { isAuthenticated } from 'core/utils';
import styles from './styles.module.css';


class App extends Component {
    state = {
        isMobile: false,
    }

    componentDidMount() {
        const { media } = this.props;
        this.initListenRouterChange();
        media({ minWidth: 480 }, () => {
          this.setState({ isMobile: false });
        });
        media({ maxWidth: 480 }, () => {
            this.setState({ isMobile: true });
        });
    }

    initListenRouterChange = () => {
        const { history } = this.props;
        history.listen((location, action) => {
            trackPageView(location.pathname);
        });
    }

    renderDesktop = () => {
        return (
            <div className={styles.desktop}>
                <div className={styles.phone}>
                    {this.renderRoutes()}
                </div>
            </div>
        );
    }

    renderMobile = () => {
        return (
            <div className={styles.mobile}>
                {this.renderRoutes()}
            </div>
        )
    }

    renderPrivateRoute = (Component) => {
        return (props) => {
            if (isAuthenticated()) return <Component {...props} />;
            return <Redirect to={{ pathname: '/' }} />;
        };
    }

    renderRoutes = () => {
        return (
            <div className={styles.root}>
                <div className={styles['app-container']}>
                    <Switch>
                        <Route
                            exact
                            path='/'
                            component={Welcome}
                        />
                        <Route
                            exact
                            path='/login'
                            component={Login}
                        />
                        <Route
                            strict
                            path='/signup'
                            component={SignUp}
                        />
                        <Route
                            strict
                            path='/home'
                            render={this.renderPrivateRoute(Home)}
                        />
                        <Route
                            strict
                            path='/job'
                            component={this.renderPrivateRoute(Job)}
                        />
                        <Route
                            exact
                            path='/guide'
                            component={this.renderPrivateRoute(Guide)}
                        />
                        <Route
                            strict
                            path='/profile'
                            component={this.renderPrivateRoute(Profile)}
                        />
                         <Route
                            exact
                            path='/privacy-policy'
                            component={PrivacyPolicy}
                        />
                        <Route
                            exact
                            path='/forget-password'
                            component={ForgetPassword}
                        />
                        <Route
                            exact
                            path='/reset-password'
                            component={ResetPassword}
                        />
                    </Switch>
                    <div id='popup' />
                    <Popup />
                </div>
            </div>
        )
    }

    render() {
        const { isMobile } = this.state;
        if (isMobile) return this.renderMobile();
        else return this.renderDesktop();
    }
}

export default Responsive(App);
