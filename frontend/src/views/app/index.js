import React, { Component } from 'react';
import Responsive from 'react-responsive-decorator';
import { Route, Switch, Redirect } from 'react-router';
import { Popup } from 'components';
import autobind from 'autobind';
import Welcome from 'views/welcome';
import Home from 'views/home';
import Login from 'views/login';
import SignUp from 'views/sign-up';
import Guide from 'views/guide';
import Job from 'views/job';
import Profile from 'views/profile';
import { isAuthenticated } from 'utils';
import styles from './styles.module.css';


@Responsive
@autobind
class App extends Component {
    state = {
        isMobile: false,
    }

    componentDidMount() {
        const { media } = this.props;
        media({ minWidth: 480 }, () => {
          this.setState({ isMobile: false });
        });
        media({ maxWidth: 480 }, () => {
            this.setState({ isMobile: true });
        });
    }

    renderDesktop() {
        return (
            <div className={styles.desktop}>
                <div className={styles.phone}>
                    {this.renderRoutes()}
                </div>
            </div>
        );
    }

    renderMobile() {
        return (
            <div className={styles.mobile}>
                {this.renderRoutes()}
            </div>
        )
    }

    renderPrivateRoute(Component) {
        return (props) => {
            if (isAuthenticated()) return <Component {...props} />;
            return <Redirect to={{ pathname: '/' }} />;
        };
    }

    renderRoutes() {
        return (
            <div className={styles.root}>
                <div className={styles['app-container']}>
                    <Switch>
                        <Route
                            exact
                            path='/'
                            component={Welcome}
                        />
                        <Route
                            exact
                            path='/login'
                            component={Login}
                        />
                        <Route
                            strict
                            path='/signup'
                            component={SignUp}
                        />
                        <Route
                            strict
                            path='/home'
                            render={this.renderPrivateRoute(Home)}
                        />
                        <Route
                            strict
                            path='/job'
                            component={this.renderPrivateRoute(Job)}
                        />
                        <Route
                            exact
                            path='/guide'
                            component={this.renderPrivateRoute(Guide)}
                        />
                        <Route
                            exact
                            path='/profile/:id'
                            component={this.renderPrivateRoute(Profile)}
                        />
                    </Switch>
                    <div id='popup' />
                    <Popup />
                </div>
            </div>
        )
    }

    render() {
        const { isMobile } = this.state;
        return isMobile ? this.renderMobile() : this.renderDesktop();
    }
}

export default App;
