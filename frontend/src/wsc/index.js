class WebSocketClient {
    static instance = null;
    callbacks = {};

    static getInstance() {
        if (!WebSocketClient.instance) WebSocketClient.instance = new WebSocketClient();
        return WebSocketClient.instance;
    }

    constructor() {
        this.socketRef = null;
    }

    connect = () => {
        const path = `${process.env.REACT_APP_WS_URL}?token=${localStorage.getItem('token')}`;
        this.socketRef = new WebSocket(path);
        this.socketRef.onopen = () => {
            console.log('WebSocket open');
        };

        this.socketRef.onmessage = e => {
            this.socketNewMessage(e.data);
        };

        this.socketRef.onerror = e => {
            console.log(e.message);
        };

        this.socketRef.onclose = () => {
            console.log("WebSocket closed let's reopen");
            this.connect();
        };
    }

    close = () => {
        this.socketRef.close();
        this.socketRef = null;
    }

    socketNewMessage = (data) => {
        const parsedData = JSON.parse(data);
        const command = parsedData.command;
        if (Object.keys(this.callbacks).length === 0) {
            return;
        }
        if (command === 'messages') {
            this.callbacks[command](parsedData.messages);
        }
        if (command === 'new_message') {
            this.callbacks[command](parsedData);
        }
    }

    initChatUser = (user) => {
        this.sendMessage({
            command: 'init_chat',
            user
        });
    }

    fetchMessages = () => {
        this.sendMessage({ command: 'fetch_messages' });
    }

    newChatMessage = (message) => {
        this.sendMessage({
            command: 'new_message',
            content: message.content,
        });
    }

    addCallbacks = (messagesCallback, newMessageCallback) => {
        this.callbacks['messages'] = messagesCallback;
        this.callbacks['new_message'] = newMessageCallback;
    }

    sendMessage = (data) => {
        try {
            this.socketRef.send(JSON.stringify({ ...data
            }));
        } catch (err) {
            console.log(err.message);
        }
    }

    state = () => {
        return this.socketRef.readyState;
    }

    waitForSocketConnection = (callback) => {
        const socket = this.socketRef;
        const recursion = this.waitForSocketConnection;
        if (!socket) {
            this.connect();
            recursion(callback);
        } else {
            setTimeout(
                function () {
                    if (socket.readyState === 1) callback();
                    else {
                        console.log("wait for connection...")
                        recursion(callback);
                    }
                },
            1);
        }
    }

}

export default WebSocketClient.getInstance();
