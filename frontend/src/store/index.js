import {
    createStore, applyMiddleware,
    combineReducers, compose,
} from 'redux';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import createSagaMiddleware from 'redux-saga';
import logger from 'redux-logger';
import { createBrowserHistory } from 'history';
import rootSaga from 'sagas';
import {
    LoginReducer, SignUpReducer, UserReducer, SkillsReducer, ChatsReducer,
    InterestsReducer, VolunteersReducer, AppReducer, JobsReducer, ReviewsReducer,
    ProfileReducer, NotificationsReducer
} from 'reducers';

export const history = createBrowserHistory();

const sagaMiddleware = createSagaMiddleware();
let middlewares = [sagaMiddleware, routerMiddleware(history)];

if (process.env.NODE_ENV === 'development') middlewares = [...middlewares, logger];

const createRootReducer = (history) => combineReducers({
    router: connectRouter(history),
    app: AppReducer,
    user: UserReducer,
    login: LoginReducer,
    signUp: SignUpReducer,
    skills: SkillsReducer,
    interests: InterestsReducer,
    volunteers: VolunteersReducer,
    jobs: JobsReducer,
    chats: ChatsReducer,
    reviews: ReviewsReducer,
    profile: ProfileReducer,
    notifications: NotificationsReducer,
})

const configureStore = (preloadedState) => {
    const store = createStore(
      createRootReducer(history),
      preloadedState,
      compose(
        applyMiddleware(
          ...middlewares
        ),
      ),
    )
    return store
}

const store = configureStore();
sagaMiddleware.run(rootSaga);
export default store;