/* eslint-disable max-len */
import ReactGA from 'react-ga';


const events = {
    submitHours: {category: 'Jobs', action: 'Submit working hours', label: 'From hours form'},
    addJob: { category: 'Jobs', action: 'Press add Job button', label: 'From dashboard'},
    editJob: { category: 'Jobs', action: 'Edit Job', label: 'From job form'},
    createJob: { category: 'Jobs', action: 'Create Job', label: 'From job form'},
    applyJob: { category: 'Jobs', action: 'Apply for Job', label: 'Apply for job from job\'s detail'},
    rejectApplicant: { category: 'Jobs', action: 'Reject applicant', label: 'Reject applicant from applicants\'s list'},
    approveApplicant: { category: 'Jobs', action: 'Approve Applicant', label: 'Approve applicant from applicants\'s list'},
    markJobAsDone: { category: 'Jobs', action: 'Mark Job as done', label: 'Mark job as done from applicants\'s list'},
    submitReview: {category: 'Reviews', action: 'Submit review', label: 'From review form'},
    requestForgetPassword: { category: 'Users', action: 'Request password reset', label: 'From forget password form'},
    signup: { category: 'Users', action: 'Signup user', label: 'From welcome page'},
    submitVolunteerRegisterForm: { category: 'Users', action: 'Submit volunteer register form', label: 'From volunteer register form page'},
    submitEnterpriseForm: { category: 'Users', action: 'Submit enterprise register form', label: 'From enterprise register form page'},
    login: { category: 'Users', action: 'Login', label: 'From login page'},
    updateSkills: { category: 'Skills', action: 'Update Skills', label: 'From guide'},
    updateInterests: { category: 'Interests', action: 'Update Interests', label: 'From guide'},
    markNotificationAsRead: { category: 'Notifications', action: 'Mark notifications as Read', label: 'From notifications view'},
    filterByCountryStatistics: ({countryName}) => ({category: 'Calendar', action: 'Filter by country', label: countryName}),
};

export const trackEvent = (key, payload) => {
    const event = (typeof events[key] === 'function')
        ? events[key](payload)
        : events[key];
    ReactGA.event({ ...event});
};

export const trackEventCallback = (...args) => () => trackEvent(...args);

export const trackPageView = path => ReactGA.pageview(path);
