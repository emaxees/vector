import {
    fetchLogin, fetchResetPassword, fetchForgetPassword, fetchValidateToken,
} from './login';
import { signUpVolunteer, signUpEnterprise } from './sign-up';
import { fetchSkills, updateSkills } from './skills';
import { fetchInterests, updateInterests } from './interests';
import { fetchVolunteers, fetchVolunteer, fetchVolunteersNextPage } from './volunteers';
import { changePassword } from './user';
import {
    fetchJobs, postJob, updateJob,
    applyJob, markJobAsDone, updateTimesheet,
    rejectApplicant, approveApplicant, fetchJob,
    deleteJob,
} from './jobs';
import { fetchChats } from './chats';
import { fetchReviews, submitReview } from './reviews';
import { fetchUserProfile, updateUserProfile, fetchEnterprise, fetchCurrentUserProfile } from './profile';
import { fetchNotifications, markNotificationAsRead } from './notifications';
import { submitFeedback } from './feedback';
import { takeLatest } from 'redux-saga/effects';


function* rootSaga() {
    yield takeLatest('REQUEST_LOGIN', fetchLogin);
    yield takeLatest('REQUEST_SIGN_UP_VOLUNTEER', signUpVolunteer);
    yield takeLatest('REQUEST_SIGN_UP_ENTERPRISE', signUpEnterprise);
    yield takeLatest('REQUEST_SKILLS', fetchSkills);
    yield takeLatest('REQUEST_INTERESTS', fetchInterests);
    yield takeLatest('UPDATE_SKILLS', updateSkills);
    yield takeLatest('UPDATE_INTERESTS', updateInterests);
    yield takeLatest('REQUEST_VOLUNTEERS', fetchVolunteers);
    yield takeLatest('REQUEST_VOLUNTEERS_NEXT_PAGE', fetchVolunteersNextPage);
    yield takeLatest('REQUEST_JOBS', fetchJobs);
    yield takeLatest('REQUEST_JOB', fetchJob);
    yield takeLatest('POST_JOB', postJob);
    yield takeLatest('UPDATE_JOB', updateJob);
    yield takeLatest('APPLY_JOB', applyJob);
    yield takeLatest('REQUEST_CHATS', fetchChats);
    yield takeLatest('REQUEST_REVIEWS', fetchReviews);
    yield takeLatest('REQUEST_USER_PROFILE', fetchUserProfile);
    yield takeLatest('UPDATE_PROFILE', updateUserProfile);
    yield takeLatest('MARK_JOB_AS_DONE', markJobAsDone );
    yield takeLatest('UPDATE_TIMESHEET', updateTimesheet );
    yield takeLatest('SUBMIT_REVIEW', submitReview);
    yield takeLatest('REJECT_APPLICANT', rejectApplicant);
    yield takeLatest('APPROVE_APPLICANT', approveApplicant);
    yield takeLatest('REQUEST_NOTIFICATIONS', fetchNotifications);
    yield takeLatest('MARK_NOTIFICATION_AS_READ', markNotificationAsRead);
    yield takeLatest('CHANGE_PASSWORD', changePassword);
    yield takeLatest('SUBMIT_FEEDBACK', submitFeedback);
    yield takeLatest('REQUEST_VOLUNTEER', fetchVolunteer);
    yield takeLatest('DELETE_JOB', deleteJob);
    yield takeLatest('REQUEST_ENTERPRISE', fetchEnterprise)
    yield takeLatest('REQUEST_FORGETPASSWORD', fetchForgetPassword)
    yield takeLatest('REQUEST_RESETPASSWORD', fetchResetPassword)
    yield takeLatest('REQUEST_VALIDATETOKEN', fetchValidateToken)
    yield takeLatest('REQUEST_CURRENT_USER_PROFILE', fetchCurrentUserProfile)
}

export default rootSaga;
