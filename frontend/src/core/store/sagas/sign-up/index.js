import Api from 'api';
import { requestSignUpVolunteerSucceeded, requestSignUpEnterpriseSucceeded } from 'core/store/actions';
import { call, put } from 'redux-saga/effects';
import { push } from 'connected-react-router';


const api = new Api();
export function* signUpVolunteer(action) {
    try {
        const data = action.payload.data;
        yield call(api.signUpVolunteer, data);
        yield put(push('/login'));
        yield put(requestSignUpVolunteerSucceeded());
    } catch (error) {
        yield put({ type: 'REQUEST_SIGN_UP_FAIL', message: JSON.parse(error.message) });
    }
}

export function* signUpEnterprise(action) {
    try {
        const data = action.payload.data;
        yield call(api.signUpEnterprise, data);
        yield put(push('/login'));
        yield put(requestSignUpEnterpriseSucceeded());
    } catch (error) {
        yield put({ type: 'REQUEST_SIGN_UP_FAIL', message: JSON.parse(error.message) });
    }
}