import React from 'react';
import { Message } from 'components';
import Api from 'api';
import {
    requestUserProfileSucceeded, updateUserProfileSucceeded,
    showPopup, setPopupContent, requestEnterpriseSucceeded,
    requestCurrentUserProfileSucceeded,
 } from 'core/store/actions';
import { call, put } from 'redux-saga/effects';
import { push, goBack } from 'connected-react-router';


const api = new Api();

export function* fetchCurrentUserProfile() {
    try {
        const response = yield call(api.getCurrentUser);
        const { type, skills, interests } = response;
        if (type === 'volunteer' && !skills.length) yield put(push('/guide'));
        else if (type === 'enterprise' && !interests.length) yield put(push('/guide'));
        yield put(requestCurrentUserProfileSucceeded(response));
    } catch (error) {
        yield put({ type: 'REQUEST_CURRENT_USER_FAIL', message: error.detail });
        const props =  {
            title: 'Warning',
            icon: 'warning',
            text: error.detail,
        };
        yield(put(setPopupContent(<Message {...props}/>)));
        yield(put(showPopup()));
        yield put(push('/login'));
    }
}

export function* fetchUserProfile(action) {
    try {
        const { id } = action.payload;
        const response = yield call(api.getUser, id);
        yield put(requestUserProfileSucceeded(response));

    } catch (error) {
        yield put({ type: 'REQUEST_USER_PROFILE_FAIL', message: JSON.parse(error.message) });
        if (JSON.parse(error.message).code === 'token_not_valid') {
            const props =  {
                title: 'Warning',
                icon: 'warning',
                text: 'Your session expired please login again',
            };
            yield(put(setPopupContent(<Message {...props}/>)));
            yield(put(showPopup()));
            yield put(push('/login'));
        }
    }
}

export function* fetchEnterprise(action) {
    try {
        const { id } = action.payload;
        const response = yield call(api.getEnterprise, id);
        yield put(requestEnterpriseSucceeded(response));
    } catch (error) {
        yield put({ type: 'REQUEST_ENTERPRISE_FAIL', message: JSON.parse(error.message) });
    }
}

export function* updateUserProfile(action) {
    try {
        const { type, id, profile } = action.payload;
        let response;
        if (type === 'volunteer') response = yield call(api.updateVolunteer,{ data: profile, id});
        else response = yield call(api.updateEnterprise,{ data: profile, id});
        yield put(updateUserProfileSucceeded(response));
        const props = {
            title: 'Success',
            icon: 'thumb_up_alt',
            text: 'Profile updated successfully',
        };
        yield(put(setPopupContent(<Message {...props}/>)));
        yield(put(showPopup()));
        yield put(goBack());
    } catch (error) {
        const props = {
            title: 'Success',
            icon: 'thumb_up_alt',
            text: 'Profile updated successfully',
        };
        yield(put(setPopupContent(<Message {...props}/>)));
        yield(put(showPopup()));
        yield put({ type: 'UPDATE_PROFILE_FAIL', message: JSON.parse(error.message) });
    }
}
