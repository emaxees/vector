import Api from 'api';
import { requestVolunteersSucceeded, requestVolunteerSucceeded, requestVolunteersNextPageSucceeded } from 'core/store/actions';
import { call, put } from 'redux-saga/effects';


const api = new Api();
export function* fetchVolunteers() {
    try {
        const response = yield call(api.getVolunteers);
        yield put(requestVolunteersSucceeded(response));
    } catch (error) {
        yield put({ type: 'REQUEST_VOLUNTEERS_FAIL', message: JSON.parse(error.message) });
    }
}

export function* fetchVolunteersNextPage(action) {
    try {
        const { page } = action.payload;
        const response = yield call(api.getVolunteers, page);
        yield put(requestVolunteersNextPageSucceeded(response));
    } catch (error) {
        yield put({ type: 'REQUEST_VOLUNTEERS_NEXT_PAGE_FAIL', message: JSON.parse(error.message) });
    }
}

export function* fetchVolunteer(action) {
    try {
        const { id } = action.payload;
        const response = yield call(api.getVolunteer, id);
        yield put(requestVolunteerSucceeded(response));
    } catch (error) {
        yield put({ type: 'REQUEST_VOLUNTEER_PROFILE_FAIL', message: JSON.parse(error.message) });
    }
}
