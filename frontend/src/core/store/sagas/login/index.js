import Api from 'api';
import React from 'react';
import { Message } from 'components';
import {
    requestLoginSucceeded, requestForgetPasswordSucceeded, requestResetPasswordSucceeded,
    requestValidateTokenSucceeded, setPopupContent, showPopup,
} from 'core/store/actions';
import { call, put } from 'redux-saga/effects';
import { push } from 'connected-react-router';


const api = new Api();

export function* fetchLogin(action) {
    try {
        const credentials = action.payload.data;
        const response = yield call(api.getAuth, credentials);
        localStorage.setItem('token', response.access);
        yield put(push('/home'));
        yield put(requestLoginSucceeded(response));
    } catch (error) {
        yield put({ type: 'REQUEST_LOGIN_FAIL', message: error.detail });
    }
}

export function* fetchForgetPassword(action) {
    try {
        const email = action.payload.data;
        const response = yield call(api.forgetPassword, email);
        const props = {
            title: 'Success',
            icon: 'thumb_up_alt',
            text: 'Check your email to complete the reset password process',
        };
        yield(put(setPopupContent(<Message {...props}/>)));
        yield put(requestForgetPasswordSucceeded(response));
        yield(put(showPopup()));
        yield put(push('/login'));
    } catch (error) {
        yield put({ type: 'REQUEST_FORGETPASSWORD_FAIL', message: JSON.parse(error.message) });
    }
}

export function* fetchResetPassword(action) {
    try {
        const {password , token} = action.payload;
        const response = yield call(api.resetPasswordConfirm, {password , token});
        yield put(requestResetPasswordSucceeded(response));
        const props = {
            title: 'Success',
            icon: 'thumb_up_alt',
            text: 'The password was successfully modified',
        };
        yield(put(setPopupContent(<Message {...props}/>)));
        yield(put(showPopup()));
        yield put(push('/login'));
    } catch (error) {
        yield put({ type: 'REQUEST_FORGETPASSWORD_FAIL', message: JSON.parse(error.message) });
    }
}

export function* fetchValidateToken(action) {
    const props = {
        title: 'Error',
        icon: 'warning',
        text: 'The token has expired or is invalid, try again',
    };
    try {
        const token = action.payload;
        const response = yield call(api.validateToken, token);
        yield put(requestValidateTokenSucceeded(response));
    } catch (error) {
        yield put({ type: 'REQUEST_VALIDATETOKEN_FAIL', message: JSON.parse(error.message) });
        yield(put(setPopupContent(<Message {...props}/>)));
        yield(put(showPopup()));
        yield put(push('/forget-password'));
    }
}