import React from 'react';
import { Message } from 'components';
import Api from 'api';
import {
    showPopup, setPopupContent, changePasswordSucceeded
} from 'core/store/actions';
import { call, put } from 'redux-saga/effects';
import { goBack } from 'connected-react-router';



const api = new Api();
export function* changePassword(action) {
    try {
        const { oldPassword, newPassword } = action.payload;
        yield call(api.updatePassword, { oldPassword, newPassword });
        yield(
            put(setPopupContent(
                React.createElement(
                    Message,
                    {
                        title: 'Success',
                        icon: 'thumb_up_alt',
                        text: 'Password update succeded',
                    }
                )
            ))
        )
        yield(put(showPopup()));
        yield put(goBack());
        yield put(changePasswordSucceeded());
    } catch (error) {
        yield put({ type: 'CHANGE_PASSWORD_FAIL', message: JSON.parse(error.message) });
    }
}