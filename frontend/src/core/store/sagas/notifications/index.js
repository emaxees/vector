import Api from 'api';
import React from 'react';
import { Message } from 'components';
import {
    requestNotificationsSucceeded,
    markNotificationAsReadSucceeded,
    setPopupContent,
    showPopup,
} from 'core/store/actions';
import { call, put } from 'redux-saga/effects';
import { push } from 'connected-react-router';


const api = new Api();
export function* fetchNotifications() {
    try {
        const response = yield call(api.getNotifications);
        yield put(requestNotificationsSucceeded(response));
    } catch (error) {
        yield put({ type: 'REQUEST_NOTIFICATIONS_FAIL', message: error.detail });
        const props =  {
            title: 'Warning',
            icon: 'warning',
            text: error.detail,
        };
        yield(put(setPopupContent(<Message {...props}/>)));
        yield(put(showPopup()));
        yield put(push('/login'));
    }
}

export function* markNotificationAsRead(action) {
    try {
        const { id } = action.payload;
        const response = yield call(api.markNotificationAsRead, id);
        yield put(markNotificationAsReadSucceeded(response));
    } catch (error) {
        yield put({ type: 'MARK_NOTIFICATION_AS_READ_FAIL', message: JSON.parse(error.message) });
    }
}