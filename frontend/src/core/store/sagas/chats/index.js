import Api from 'api';
import React from 'react';
import { Message } from 'components';
import {
    requestChatsSucceeded,
    setPopupContent,
    showPopup,
} from 'core/store/actions';
import { call, put } from 'redux-saga/effects';
import { push } from 'connected-react-router';


const api = new Api();
export function* fetchChats() {
    try {
        const response = yield call(api.getChats);
        yield put(requestChatsSucceeded(response));
    } catch (error) {
        yield put({ type: 'REQUEST_CHATS_FAIL', message: error.detail });
        const props =  {
            title: 'Warning',
            icon: 'warning',
            text: error.detail,
        };
        yield(put(setPopupContent(<Message {...props}/>)));
        yield(put(showPopup()));
        yield put(push('/login'));
    }
}
