import Api from 'api';
import React from 'react';
import { Message } from 'components';
import { requestReviewsSucceeded, submitReviewSucceeded, setPopupContent } from 'core/store/actions';
import { call, put } from 'redux-saga/effects';


const api = new Api();
export function* fetchReviews(action) {
    try {
        const { id } = action.payload;
        const response = yield call(api.getReviews, id);
        yield put(requestReviewsSucceeded(response));
    } catch (error) {
        yield put({ type: 'REQUEST_REVIEWS_FAIL', message: JSON.parse(error.message) });
    }
}

export function* submitReview(action) {
    try {
        const { review } = action.payload;
        const response = yield call(api.postReview, review);
        yield(
            put(setPopupContent(
                React.createElement(
                    Message,
                    {
                        title: 'Success',
                        icon: 'thumb_up_alt',
                        text: 'Review submited succesfully!',
                    }
                )
            ))
        )
        yield put(submitReviewSucceeded(response));
    } catch (error) {
        yield put({ type: 'SUBMIT_REVIEW_FAIL', message: JSON.parse(error.message) });
    }
}
