import Api from 'api';
import React from 'react';
import { Message } from 'components';
import {
    requestJobsSucceeded, postJobSucceeded,
    updateJobSucceeded, applyJobSucceeded,
    showPopup, setPopupContent, markJobAsDoneSucceeded,
    updateTimesheetSucceeded, rejectApplicantSucceeded,
    approveApplicantSucceeded, requestJobSucceeded,
    deleteJobSucceeded,
} from 'core/store/actions';
import { call, put } from 'redux-saga/effects';
import { push } from 'connected-react-router';


const api = new Api();
export function* fetchJobs() {
    try {
        const response = yield call(api.getJobs);
        yield put(requestJobsSucceeded(response));
    } catch (error) {
        yield put({type: 'REQUEST_JOBS_FAIL', message: error.detail});
        const props =  {
            title: 'Warning',
            icon: 'warning',
            text: error.detail,
        };
        yield(put(setPopupContent(<Message {...props}/>)));
        yield(put(showPopup()));
        yield put(push('/login'));
    }
}

export function* fetchJob(action) {
    try {
        const { id } = action.payload;
        const response = yield call(api.getJob, id);
        yield put(requestJobSucceeded(response));
    } catch (error) {
        yield put({ type: 'REQUEST_JOBS_FAIL', message: JSON.parse(error.message) });
    }
}

export function* postJob(action) {
    try {
        const { data } = action.payload;
        const response = yield call(api.postJob, data);
        yield put(postJobSucceeded(response));
        const props = {
            title: 'Success',
            icon: 'thumb_up_alt',
            text: 'Job created successfully',
        };
        yield(put(setPopupContent(<Message {...props}/>)));
        yield(put(showPopup()));
        yield put(push('/home'));
    } catch (error) {
        yield(
            put(setPopupContent(
                React.createElement(
                    Message,
                    {
                        title:'Error',
                        icon:'warning',
                        text: (() => {
                            const message = JSON.parse(error.message);
                            return Object.keys(message).map(
                                (key) => {
                                    let text;
                                    if (key === 'date') text = 'Date: This field may not be blank';
                                    else text = `${key[0].toUpperCase()}${key.replace(key[0],'')}: ${message[key][0]}`;
                                    return (
                                        <div key={key} style={{textAlign: 'left'}}>
                                            <p>{text}</p>
                                            <br />
                                        </div>
                                    )
                                }
                            );
                        })()
                    }
                )
            ))
        )
        yield(put(showPopup()));
        yield put({ type: 'POST_JOB_FAIL', message: JSON.parse(error.message) });
    }
}

export function* updateJob(action) {
    try {
        const { form, id } = action.payload;
        const response = yield call(api.updateJob, { id, form });
        const props = {
            title: 'Success',
            icon: 'thumb_up_alt',
            text: 'Job has been edited successfully',
        };
        yield(put(setPopupContent(<Message {...props}/>)));
        yield put(updateJobSucceeded(response));
        yield(put(showPopup()));
        yield put(push('/home'));
    } catch (error) {
        const props = {
            title:'Error',
            icon:'warning',
            text: (() => {
                const message = JSON.parse(error.message);
                return Object.keys(message).map(
                    (key) => {
                        let text;
                        if (key === 'date') text = 'Date: This field may not be blank';
                        else text = `${key[0].toUpperCase()}${key.replace(key[0],'')}: ${message[key][0]}`;
                        return (
                            <div key={key} style={{textAlign: 'left'}}>
                                <p>{text}</p>
                                <br />
                            </div>
                        )
                    }
                );
            })()
        }
        yield(put(setPopupContent(<Message {...props}/>)));
        yield(put(showPopup()));
        yield put({ type: 'UPDATE_JOB_FAIL', message: JSON.parse(error.message) });
    }
}

export function* applyJob(action) {
    try {
        const { id } = action.payload;
        yield call(api.applyJob, id);
        yield put(applyJobSucceeded());
        yield(
            put(setPopupContent(
                React.createElement(
                    Message,
                    {
                        title: 'Success',
                        icon: 'thumb_up_alt',
                        text: 'You applied successfully to this Job',
                    }
                )
            ))
        )
        yield(put(showPopup()));
        yield put(push('/home'));
    } catch (error) {
        yield(
            put(setPopupContent(
                React.createElement(
                    Message,
                    {
                        title:'Error',
                        icon:'warning',
                        text: JSON.parse(error.message).detail
                    }
                )
            ))
        )
        yield(put(showPopup()));
        yield put({ type: 'UPDATE_JOB_FAIL', message: JSON.parse(error.message) });
    }
}

export function* markJobAsDone(action) {
    try {
        const { job, id } = action.payload;
        const response = yield call(api.updateJobBase, { id, job });
        yield(
            put(setPopupContent(
                React.createElement(
                    Message,
                    {
                        title: 'Success',
                        icon: 'thumb_up_alt',
                        text: 'Job marked as done, please complete the timesheet and leave a review!',
                    }
                )
            ))
        )
        yield(put(showPopup()));
        yield put(markJobAsDoneSucceeded(response));
    } catch (error) {
        yield(
            put(setPopupContent(
                React.createElement(
                    Message,
                    {
                        title:'Error',
                        icon:'warning',
                        text: JSON.parse(error.message).detail
                    }
                )
            ))
        )
        yield(put(showPopup()));
        yield put({ type: 'MARK_JOB_AS_DONE_FAIL', message: JSON.parse(error.message) });
    }
}

export function* updateTimesheet(action) {
    try {
        const { job: { id, applicants } } = action.payload;
        const response = yield call(
            api.updateJobBase,
            {
                id,
                job: { applicants: [ ...applicants.map(applicant => ({...applicant, volunteer: applicant.volunteer.id }))]}
            }
        );
        yield(
            put(setPopupContent(
                React.createElement(
                    Message,
                    {
                        title: 'Success',
                        icon: 'thumb_up_alt',
                        text: `Timesheet updated!`,
                    }
                )
            ))
        )
        yield put(updateTimesheetSucceeded(response));
    } catch (error) {
        yield(
            put(setPopupContent(
                React.createElement(
                    Message,
                    {
                        title:'Error',
                        icon:'warning',
                        text: JSON.parse(error.message).detail
                    }
                )
            ))
        )
        yield(put(showPopup()));
        yield put({ type: 'UPDATE_TIMESHEET_FAIL', message: JSON.parse(error.message) });
    }
}

export function* rejectApplicant(action) {
    try {
        const { job, id } = action.payload;
        const response = yield call(api.updateJobBase, { id, job });
        yield put(rejectApplicantSucceeded(response));
        yield(
            put(setPopupContent(
                React.createElement(
                    Message,
                    {
                        title: 'Success',
                        icon: 'thumb_up_alt',
                        text: 'Applicant rejected successfully',
                    }
                )
            ))
        )
        yield(put(showPopup()));
    } catch (error) {
        yield(
            put(setPopupContent(
                React.createElement(
                    Message,
                    {
                        title:'Error',
                        icon:'warning',
                        text: JSON.parse(error.message).detail
                    }
                )
            ))
        )
        yield(put(showPopup()));
        yield put({ type: 'REJECT_APPLICANT_FAIL', message: JSON.parse(error.message) });
    }
}

export function* approveApplicant(action) {
    try {
        const { job, id } = action.payload;
        const response = yield call(api.updateJobBase, { id, job });
        yield put(approveApplicantSucceeded(response));
        yield(
            put(setPopupContent(
                React.createElement(
                    Message,
                    {
                        title: 'Success',
                        icon: 'thumb_up_alt',
                        text: 'Applicant approved successfully',
                    }
                )
            ))
        )
        yield(put(showPopup()));
    } catch (error) {
        yield(
            put(setPopupContent(
                React.createElement(
                    Message,
                    {
                        title:'Error',
                        icon:'warning',
                        text: JSON.parse(error.message).detail
                    }
                )
            ))
        )
        yield(put(showPopup()));
        yield put({ type: 'APPROVE_APPLICANT_FAIL', message: JSON.parse(error.message) });
    }
}

export function* deleteJob(action) {
    try {
        const { id } = action.payload;
        yield call(api.deleteJob, id);
        yield put(deleteJobSucceeded(id));
        yield(
            put(setPopupContent(
                React.createElement(
                    Message,
                    {
                        title: 'Success',
                        icon: 'thumb_up_alt',
                        text: 'Job has been deleted successfully',
                    }
                )
            ))
        )
        yield(put(showPopup()));
        yield put(push('/home/dashboard'));
    } catch (error) {
        yield(
            put(setPopupContent(
                React.createElement(
                    Message,
                    {
                        title:'Error',
                        icon:'warning',
                        text: JSON.parse(error.message).detail
                    }
                )
            ))
        )
        yield(put(showPopup()));
    }
}