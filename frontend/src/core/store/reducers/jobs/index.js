const initialState = {
    list: [],
    current: undefined,
    fetched: false,
    form: {
        title: '',
        description: '',
        date: '',
        location: '',
        commitHours: '',
        vacancy: '',
        requirements: [],
        fee: 'bono',
    },
    applicant: undefined,
};

const JobsReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'REQUEST_JOB_SUCCEEDED': {
            const { response } = action.payload;
            const {
                title,
                description,
                date,
                location,
                commitHours,
                vacancy,
                requirements,
                fee,
            } = response;
            return {
                ...state,
                list: [...state.list, response],
                form:  {
                    title,
                    description,
                    date,
                    location,
                    commitHours,
                    vacancy,
                    fee ,
                    requirements: requirements.map(
                        requirement => ({
                            skill: requirement.skill.id,
                            points: requirement.points
                        })
                    )
                }
            };
        }
        case 'UPDATE_JOB_FORM_IN_BULK': {
            const {
                title,
                description,
                date,
                location,
                commitHours,
                vacancy,
                requirements,
                fee,
            } = action.payload.data;
            return {
                ...state,
                form:  {
                    title,
                    description,
                    date,
                    location,
                    commitHours,
                    vacancy,
                    fee ,
                    requirements: requirements.map(
                        requirement => ({
                            skill: requirement.skill.id,
                            points: requirement.points
                        })
                    )
                }
            };
        }
        case 'REQUEST_JOBS_SUCCEEDED': {
            const { response } = action.payload;
            return { ...state,  list: [...response], fetched: true };
        }
        case 'UPDATE_JOB_FORM': {
            const {key, value} = action.payload;
            return {...state, form: { ...state.form, [key]: value } }
        }
        case 'ADD_REQUIREMENT': {
            const {value} = action.payload;
            return {...state, form: { ...state.form, requirements: [...state.form.requirements, value ] } }
        }
        case 'REMOVE_REQUIREMENT': {
            const { id } = action.payload;
            return {
                ...state,
                form: {
                    ...state.form,
                    requirements: state.form.requirements.filter(requirement => requirement.skill !== id)
                }
            }
        }
        case 'SET_JOB': {
            const { data } = action.payload;
            return {...state, form: { ...data } }
        }
        case 'RESET_JOB_FORM': {
            return {...state, form: { ...initialState.form } }
        }
        case 'MARK_JOB_AS_DONE_SUCCEEDED': {
            const { response } = action.payload;
            return {
                ...state,
                list: [
                    ...state.list.filter(item => item.id !== response.id),
                    { ...state.list.find(item => item.id === response.id), finished: true }
                ]
            }
        }
        case 'UPDATE_TIMESHEET_SUCCEEDED': {
            const { response } = action.payload;
            return {
                ...state,
                list: [
                    ...state.list.filter(item => item.id !== response.id),
                    {
                        ...state.list.find(item => item.id === response.id),
                        applicants: state.list.find(item => item.id === response.id).applicants.map(applicant => ({
                            ...applicant,
                            hours: (() => {
                                const ids = response.applicants.map(applicant => applicant.id);
                                const { id } = applicant;
                                if (ids.includes(id)) return response.applicants.find(applicant => applicant.id === id).hours;
                                return applicant.hours
                            })()
                        }))
                    }
                ]
            }
        }
        case 'SUBMIT_REVIEW_SUCCEEDED': {
            const { response } = action.payload;
            return {
                ...state,
                list: [
                    ...state.list.filter(item => item.id !== response.job),
                    {
                        ...state.list.find(item => item.id === response.job),
                        applicants: state.list.find(item => item.id === response.job).applicants.map(applicant => ({
                            ...applicant,
                            hasReview: (() => {
                                if (applicant.volunteer.id === response.user) return true
                                return false
                            })()
                        })),
                        reviews: [...state.list.find(item => item.id === response.job), response]
                    }
                ]
            }
        }
        case 'APPROVE_APPLICANT_SUCCEEDED': {
            const { response } = action.payload;
            return {
                ...state,
                list: [
                    ...state.list.filter(item => item.id !== response.id),
                    {
                        ...state.list.find(item => item.id === response.id),
                        applicants: state.list.find(item => item.id === response.id).applicants.map(applicant => ({
                            ...applicant,
                            status: (() => {
                                const { id } = applicant;
                                return response.applicants.find(applicant => applicant.id === id).status
                            })()
                        })),
                    }
                ]
            }
        }
        case 'REJECT_APPLICANT_SUCCEEDED': {
            const { response } = action.payload;
            return {
                ...state,
                list: [
                    ...state.list.filter(item => item.id !== response.id),
                    {
                        ...state.list.find(item => item.id === response.id),
                        applicants: state.list.find(item => item.id === response.id).applicants.map(applicant => ({
                            ...applicant,
                            status: (() => {
                                const { id } = applicant;
                                return response.applicants.find(applicant => applicant.id === id).status
                            })()
                        })),
                    }
                ]
            }
        }
        case 'SET_APPLICANT': {
            const { applicant } = action.payload;
            return {
                ...state,
                applicant
            }
        }
        case 'RESET_APPLICANT': {
            return {
                ...state,
                applicant: initialState.applicant
            }
        }
        case 'UPDATE_APPLICANTS_HOURS': {
            const { id, hours } = action.payload;
            return {
                ...state,
                form: {
                    ...state.form,
                    applicants: [
                        ...state.form.applicants.filter(applicant => applicant.id !== id),
                        {
                            ...state.form.applicants.find(applicant => applicant.id === id),
                            hours
                        }
                    ]
                }
            }
        }
        case 'DELETE_JOB_SUCCEEDED': {
            const { id } = action.payload;
            return {
                ...state,
                list: {
                    ...state.list,
                    requirements: state.list.filter(job => job.id !== id)
                }
            }
        }
        default:
            return state
    }
};

export default JobsReducer;