import { getQueryParamFromKey } from 'core/utils';

const initialState = {
    list: [],
    count: 0,
    current: 1,
    fetched: false,
};

const VolunteersReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'REQUEST_VOLUNTEERS_SUCCEEDED': {
            const { response: { count, next, results }  } = action.payload;
            return {
                ...state,
                list: results,
                count,
                next: next && Number(getQueryParamFromKey('page', next)) ,
                fetched: true
            }
        }
        case 'REQUEST_VOLUNTEERS_NEXT_PAGE_SUCCEEDED': {
            const { response: { count, next, results }  } = action.payload;
            return {
                ...state,
                list: [...state.list, ...results],
                current: state.current + 1,
                count,
                next: next && Number(getQueryParamFromKey('page', next)),
                fetched: true,
            }
        }
        case 'REQUEST_VOLUNTEER_SUCCEEDED': {
            const { response } = action.payload;
            return {
                ...state,
                list: [
                    ...state.list.filter((volunteer) => volunteer.id !== response.id),
                    response,
                ]
            }
        }
        default:
            return state
    }
};

export default VolunteersReducer;
