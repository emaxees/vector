const initialState = {
    types: [],
    isFetching: true,
    selected: []
};

const InterestsReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'REQUEST_INTERESTS_SUCCEEDED': {
            const { response } = action.payload;
            return { ...state,  types: [...response] };
        }
        case 'ADD_INTEREST': {
            const { id } = action.payload;
            return { ...state,  selected: [...state.selected, id] };
        }
        case 'REMOVE_INTEREST': {
            const { id } = action.payload;
            return {
                ...state,
                selected: state.selected.filter(skill => skill !== id)
            };
        }
        default:
            return state
    }
};

export default InterestsReducer;
