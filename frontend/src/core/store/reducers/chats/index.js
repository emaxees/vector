const initialState = {
    list: [],
    isFetching: true,
};

const ChatsReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'REQUEST_CHATS_SUCCEEDED': {
            const { response } = action.payload;
            return { ...state,  list: [...response] };
        }
        default:
            return state
    }
};

export default ChatsReducer;
