const initialState = {
    list: [],
    isFetching: true,
};

const NotificationsReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'REQUEST_NOTIFICATIONS_SUCCEEDED': {
            const { response } = action.payload;
            return { ...state,  list: [...response] };
        }
        case 'MARK_NOTIFICATION_AS_READ_SUCCEEDED': {
            const { response } = action.payload;
            return { ...state,  list: [...response] };
        }
        default:
            return state
    }
};

export default NotificationsReducer;
