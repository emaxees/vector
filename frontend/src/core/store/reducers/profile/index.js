const initialState = {
    user: {
        id: undefined,
        fetched: false,
    },
    enterprise: undefined,
    fetched: false,
    isEnterpriseFetched: false,
};

const ProfileReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'REQUEST_USER_PROFILE_SUCCEEDED': {
            const { response } = action.payload;
            return { ...state,  user: response, fetched: true, };
        }
        case 'REQUEST_CURRENT_USER_PROFILE_SUCCEEDED': {
            const { response } = action.payload;
            return { ...state,  user: { ...state.user, ...response, fetched: true }};
        }
        case 'UPDATE_USER_PROFILE_FORM': {
            const {key, value} = action.payload;
            return {...state, user: { ...state.user, [key]: value } };
        }
        case 'RESET_USER_PROFILE': {
            return initialState;
        }
        case 'REQUEST_ENTERPRISE': {
            return { ...state, isEnterpriseFetched: false };
        }
        case 'REQUEST_ENTERPRISE_SUCCEEDED': {
            const { data } = action.payload;
            return { ...state, enterprise: data, isEnterpriseFetched: true };
        }
        default:
            return state
    }
};

export default ProfileReducer;
