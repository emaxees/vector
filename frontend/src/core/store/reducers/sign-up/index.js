const initialState = {
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    companyName: '',
    administratorName: '',
    phoneNumber: '',
    found: 'social',
    message: undefined,
}

const SignUpReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'RESET_SIGN_UP_FORM': {
            return { ...initialState }
        }
        case 'UPDATE_SIGN_UP_FORM': {
            const {key, value} = action.payload;
            return {...state, [key]: value }
        }
        case 'REQUEST_SIGN_UP_FAIL': {
            const { message } = action;
            return {...state, message }
        }
        default:
            return state
        }
}

export default SignUpReducer