const initialState = {
    types: [],
    selected: [],
    isFetching: true,
};

const SkillsReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'REQUEST_SKILLS_SUCCEEDED': {
            const { response } = action.payload;
            return { ...state,  types: [...response] };
        }
        case 'ADD_SKILL': {
            const { id } = action.payload;
            return { ...state,  selected: [...state.selected, id] };
        }
        case 'REMOVE_SKILL': {
            const { id } = action.payload;
            return {
                ...state,
                selected: state.selected.filter(skill => skill !== id)
            };
        }
        default:
            return state
    }
};

export default SkillsReducer;
