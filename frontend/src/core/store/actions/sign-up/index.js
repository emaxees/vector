export const updateSignUpForm = (key, value) => ({ type: 'UPDATE_SIGN_UP_FORM', payload: { key, value } });

export const resetSignUpForm = () => ({ type: 'RESET_SIGN_UP_FORM' });

export const requestSignUpVolunteer = data => ({ type: 'REQUEST_SIGN_UP_VOLUNTEER', payload: { data } });
export const requestSignUpVolunteerSucceeded = () => ({ type: 'REQUEST_SIGN_UP_VOLUNTEER_SUCCEEDED' });

export const requestSignUpEnterprise = data => ({ type: 'REQUEST_SIGN_UP_ENTERPRISE', payload: { data } });
export const requestSignUpEnterpriseSucceeded = () => ({ type: 'REQUEST_SIGN_UP_ENTERPRISE_SUCCEEDED' });
