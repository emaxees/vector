export const requestReviews = id => ({ type: 'REQUEST_REVIEWS', payload: { id } });
export const requestReviewsSucceeded = response => ({ type: 'REQUEST_REVIEWS_SUCCEEDED', payload: { response } });
export const updateReviewForm = (key, value) => ({ type: 'UPDATE_REVIEW_FORM', payload: { key, value } });
export const resetReviewForm = () => ({ type: 'RESET_REVIEW_FORM' });
export const submitReview = review => ({ type: 'SUBMIT_REVIEW', payload: { review } });
export const submitReviewSucceeded = response => ({ type: 'SUBMIT_REVIEW_SUCCEEDED', payload: { response } });
