export const requestUserProfile = id => ({ type: 'REQUEST_USER_PROFILE', payload: { id } });
export const requestUserProfileSucceeded = response => ({ type: 'REQUEST_USER_PROFILE_SUCCEEDED', payload: { response } });

export const requestCurrentUserProfile = () => ({ type: 'REQUEST_CURRENT_USER_PROFILE' });
export const requestCurrentUserProfileSucceeded = response => ({ type: 'REQUEST_CURRENT_USER_PROFILE_SUCCEEDED', payload: { response } });

export const resetUserProfile = () => ({ type: 'RESET_USER_PROFILE' });
export const updateUserProfileForm = (key, value) => ({ type: 'UPDATE_USER_PROFILE_FORM', payload: { key, value } });

export const updateUserProfile = ({type, id, profile}) => ({ type: 'UPDATE_PROFILE', payload: { type, id, profile } });
export const updateUserProfileSucceeded = () => ({ type: 'UPDATE_PROFILE_SUCCEEDED' });

export const requestVolunteer = id => ({ type: 'REQUEST_VOLUNTEER', payload: { id } });
export const requestVolunteerSucceeded = response => ({ type: 'REQUEST_VOLUNTEER_SUCCEEDED', payload: { response } });

export const requestEnterprise = (id) => ({ type: 'REQUEST_ENTERPRISE', payload: { id } });
export const requestEnterpriseSucceeded = data => ({ type: 'REQUEST_ENTERPRISE_SUCCEEDED', payload: { data } });
