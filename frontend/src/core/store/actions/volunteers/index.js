export const requestVolunteers = () => ({ type: 'REQUEST_VOLUNTEERS' });
export const requestVolunteersSucceeded = response => ({ type: 'REQUEST_VOLUNTEERS_SUCCEEDED', payload: { response } });

export const requestVolunteersNextPage = (page) => ({ type: 'REQUEST_VOLUNTEERS_NEXT_PAGE', payload: { page } });
export const requestVolunteersNextPageSucceeded = (response) => ({ type: 'REQUEST_VOLUNTEERS_NEXT_PAGE_SUCCEEDED', payload: { response } });
