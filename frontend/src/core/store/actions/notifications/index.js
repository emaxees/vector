export const requestNotifications = () => ({ type: 'REQUEST_NOTIFICATIONS' });
export const requestNotificationsSucceeded = response => ({ type: 'REQUEST_NOTIFICATIONS_SUCCEEDED', payload: { response } });
export const markNotificationAsRead = id => ({ type: 'MARK_NOTIFICATION_AS_READ', payload: { id } });
export const markNotificationAsReadSucceeded = response => ({ type: 'MARK_NOTIFICATION_AS_READ_SUCCEEDED', payload: { response } });
