export const changeBrowseTab = tab => ({ type: 'CHANGE_BROWSE_TAB', payload: { tab } });
export const changeProfileTab = tab => ({ type: 'CHANGE_PROFILE_TAB', payload: { tab } });
export const resetProfileTab = () => ({ type: 'RESET_PROFILE_TAB' });
export const showPopup = () => ({ type: 'SHOW_POPUP' });
export const hidePopup = () => ({ type: 'HIDE_POPUP' });
export const setPopupContent = content => ({ type: 'SET_POPUP_CONTENT', payload: { content }});
export const resetApp = () => ({ type: 'RESET_APP' });
