import moment from 'moment';
import UAParser from 'ua-parser-js';
import jsonFormData from 'json-form-data';
import getQueryParam from  'get-query-param';


export { default as stripHtml } from 'string-strip-html';
/* eslint max-len: 0 */
/* eslint no-useless-escape: 0 */

export const isValidEmail = (email) => {
    const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(String(email).toLowerCase());
};

export const isValidField = value => Boolean(value);

export const isAuthenticated = () => Boolean(localStorage.getItem('token'));

export const decodeHtmlString = (stringToDecode) => {
    const parser = new DOMParser();
    return parser.parseFromString(`<!doctype html><body>${stringToDecode}`, 'text/html').body.textContent;
};

export const isObjectEmpty = obj => !(Object.keys(obj).length);

export const formatDate = (date, format='DD MMMM YYYY') => moment(date).format(format);

export const formatTime = time => moment(time, 'HH:mm:ss').format('hh:mm A');

export const openUrl = (url, history) => {
    if (url.includes(window.location.hostname)) {
        const pathParts = url.split('/');
        const path = pathParts.pop() || pathParts.pop();
        history.push(`post/${path}`);
    } else if (url.includes('http://') || url.includes('https://')) window.open(url);
    else window.open(`http://${url}`);
};

export const stripTags = (html) => {
    const tmp = document.createElement('div');
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || '';
};

export const capitalize = (string) => string.charAt(0).toUpperCase() + string.slice(1);

export const between = (x, min, max) => x >= min && x <= max;

export const getSkillLevel = (points) => {
    if (between(points, 0, 500)) return 'Beginner';
    if (between(points, 500, 999)) return 'Intermediate';
    return 'Advanced';
}

export const isMobile = () => new UAParser().getResult().device.type === 'mobile';

export const getOS = () => new UAParser().getOS().name;

export const getModel = () => new UAParser().getOS().name;

export const urlBase64ToUint8Array = base64String => {
    var padding = '='.repeat((4 - base64String.length % 4) % 4)
    var base64 = (base64String + padding)
            .replace(/\-/g, '+')
            .replace(/_/g, '/')

    var rawData = window.atob(base64)
    var outputArray = new Uint8Array(rawData.length)

    for (var i = 0; i < rawData.length; ++i) {
            outputArray[i] = rawData.charCodeAt(i)
    }
    return outputArray;
}

export const loadVersionBrowser = (userAgent) => {
    var ua = userAgent;
    var tem;
    var M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
            return {name: 'IE', version: (tem[1] || '')};
    }
    if (M[1] === 'Chrome') {
            tem = ua.match(/\bOPR\/(\d+)/);
            if (tem != null) {
                    return {name: 'Opera', version: tem[1]};
            }
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) {
            M.splice(1, 1, tem[1]);
    }
    return {
            name: M[0],
            version: M[1]
    };
};

export const JSONToFormData = (json) => jsonFormData(json, {
    initialFormData: new FormData(),
    includeNullValues: false,
    showLeafArrayIndexes: true,
});

export const  getQueryParamFromKey = (key, url) => getQueryParam(key, url);
