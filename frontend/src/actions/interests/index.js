export const requestInterests = () => ({ type: 'REQUEST_INTERESTS' });
export const requestInterestsSucceeded = response => ({ type: 'REQUEST_INTERESTS_SUCCEEDED', payload: { response } });
export const addInterest = id => ({ type: 'ADD_INTEREST', payload: { id } });
export const removeInterest = id => ({ type: 'REMOVE_INTEREST', payload: { id } });
export const updateInterests = ({id, interests}) => ({ type: 'UPDATE_INTERESTS', payload: { interests, id } });
export const updateInterestsSucceeded = () => ({ type: 'UPDATE_INTERESTS_SUCCEEDED' });
