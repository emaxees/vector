export const requestVolunteers = () => ({ type: 'REQUEST_VOLUNTEERS' });
export const requestVolunteersSucceeded = volunteers => ({ type: 'REQUEST_VOLUNTEERS_SUCCEEDED', payload: { volunteers } });
