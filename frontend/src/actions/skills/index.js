export const requestSkills = () => ({ type: 'REQUEST_SKILLS' });
export const requestSkillsSucceeded = response => ({ type: 'REQUEST_SKILLS_SUCCEEDED', payload: { response } });
export const addSkill = id => ({ type: 'ADD_SKILL', payload: { id } });
export const removeSkill = id => ({ type: 'REMOVE_SKILL', payload: { id } });
export const updateSkills = ({id, skills}) => ({ type: 'UPDATE_SKILLS', payload: { skills, id } });
export const updateSkillsSucceeded = () => ({ type: 'UPDATE_SKILLS_SUCCEEDED' });
