export const requestChats = () => ({ type: 'REQUEST_CHATS' });
export const requestChatsSucceeded = response => ({ type: 'REQUEST_CHATS_SUCCEEDED', payload: { response } });
