export const setUserType = type => ({ type: 'SET_USER_TYPE', payload: { type } });
export const resetUserType = type => ({ type: 'RESET_USER_TYPE', payload: { type } });
export const changePassword = (oldPassword, newPassword) => ({ type: 'CHANGE_PASSWORD', payload: { oldPassword, newPassword } });
export const changePasswordSucceeded = () => ({ type: 'CHANGE_PASSWORD_SUCCEEDED' });
