export const requestUserProfile = id => ({ type: 'REQUEST_USER_PROFILE', payload: { id } });
export const requestUserProfileSucceeded = response => ({ type: 'REQUEST_USER_PROFILE_SUCCEEDED', payload: { response } });
export const resetUserProfile = () => ({ type: 'RESET_USER_PROFILE' });
export const updateUserProfileForm = (key, value) => ({ type: 'UPDATE_USER_PROFILE_FORM', payload: { key, value } });
export const updateUserProfile = ({type, id, profile}) => ({ type: 'UPDATE_PROFILE', payload: { type, id, profile } });
export const updateUserProfileSucceeded = () => ({ type: 'UPDATE_PROFILE_SUCCEEDED' });