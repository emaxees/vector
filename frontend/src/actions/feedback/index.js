export const submitFeedback = feedback => ({ type: 'SUBMIT_FEEDBACK', payload: { feedback } });
export const submitFeedbackSucceeded = () => ({ type: 'SUBMIT_FEEDBACK_SUCCEEDED' });
