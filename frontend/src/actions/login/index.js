export const updateLoginForm = (key, value) => ({ type: 'UPDATE_LOGIN_FORM', payload: { key, value } });
export const requestLogin = data => ({ type: 'REQUEST_LOGIN', payload: { data } });
export const requestLoginSucceeded = user => ({ type: 'REQUEST_LOGIN_SUCCEEDED', payload: { user } });
export const logout = () => ({ type: 'LOG_OUT' });
export const logoutSucceeded = () => ({ type: 'LOG_OUT_SUCCEEDED' });
