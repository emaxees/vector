import Api from 'api';
import { requestNotificationsSucceeded, markNotificationAsReadSucceeded } from 'actions';
import { call, put } from 'redux-saga/effects';


const api = new Api();
export function* fetchNotifications() {
    try {
        const response = yield call(api.getNotifications);
        yield put(requestNotificationsSucceeded(response));
    } catch (error) {
        yield put({ type: 'REQUEST_NOTIFICATIONS_FAIL', message: JSON.parse(error.message) });
    }
}

export function* markNotificationAsRead(action) {
    try {
        const { id } = action.payload;
        const response = yield call(api.markNotificationAsRead, id);
        yield put(markNotificationAsReadSucceeded(response));
    } catch (error) {
        yield put({ type: 'MARK_NOTIFICATION_AS_READ_FAIL', message: JSON.parse(error.message) });
    }
}