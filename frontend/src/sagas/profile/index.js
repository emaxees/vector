import React from 'react';
import { Message } from 'components';
import Api from 'api';
import {
    requestUserProfileSucceeded, updateUserProfileSucceeded,
    showPopup, setPopupContent,
 } from 'actions';
import { call, put } from 'redux-saga/effects';
import { goBack } from 'connected-react-router';



const api = new Api();
export function* fetchUserProfile(action) {
    try {
        const { id } = action.payload;
        const response = yield call(api.getUser, id);
        yield put(requestUserProfileSucceeded(response));
    } catch (error) {
        yield put({ type: 'REQUEST_USER_PROFILE_FAIL', message: JSON.parse(error.message) });
    }
}

export function* updateUserProfile(action) {
    try {
        const { type, id, profile } = action.payload;
        let response;
        if (type === 'volunteer') response = yield call(api.updateVolunteer,{ data: profile, id});
        else response = yield call(api.updateEnterprise,{ data: profile, id});
        yield put(updateUserProfileSucceeded(response));
        yield(
            put(setPopupContent(
                React.createElement(
                    Message,
                    {
                        title: 'Success',
                        icon: 'thumb_up_alt',
                        text: 'Profile updated successfully',
                    }
                )
            ))
        )
        yield(put(showPopup()));
        yield put(goBack());
    } catch (error) {
        yield(
            put(setPopupContent(
                React.createElement(
                    Message,
                    {
                        title:'Error',
                        icon:'warning',
                        text: JSON.parse(error.message).detail
                    }
                )
            ))
        )
        yield(put(showPopup()));
        yield put({ type: 'UPDATE_PROFILE_FAIL', message: JSON.parse(error.message) });
    }
}
