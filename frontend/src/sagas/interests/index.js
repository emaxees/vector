import Api from 'api';
import { requestInterestsSucceeded, updateInterestsSucceeded } from 'actions';
import { call, put } from 'redux-saga/effects';
import { push } from 'connected-react-router';


const api = new Api();
export function* fetchInterests() {
    try {
        const response = yield call(api.getInterests);
        yield put(requestInterestsSucceeded(response));
    } catch (error) {
        yield put({ type: 'REQUEST_INTERESTS_FAIL', message: JSON.parse(error.message) });
    }
}

export function* updateInterests(action) {
    try {
        const { id, interests } = action.payload;
        yield call(api.updateInterests, { id, interests });
        yield put(updateInterestsSucceeded());
        yield put(push('/home'));
    } catch (error) {
        yield put({ type: 'UPDATE_SKILLS_FAIL', message: JSON.parse(error.message) });
    }
}
