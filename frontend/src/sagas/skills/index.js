import Api from 'api';
import { requestSkillsSucceeded, updateSkillsSucceeded } from 'actions';
import { call, put } from 'redux-saga/effects';
import { push } from 'connected-react-router';


const api = new Api();
export function* fetchSkills() {
    try {
        const response = yield call(api.getSkills);
        yield put(requestSkillsSucceeded(response));
    } catch (error) {
        yield put({ type: 'REQUEST_SKILLS_FAIL', message: JSON.parse(error.message) });
    }
}

export function* updateSkills(action) {
    try {
        const { id, skills } = action.payload;
        yield call(api.updateSkills, { id, skills });
        yield put(updateSkillsSucceeded());
        yield put(push('/home'));
    } catch (error) {
        yield put({ type: 'UPDATE_SKILLS_FAIL', message: JSON.parse(error.message) });
    }
}
