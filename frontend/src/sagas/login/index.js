import Api from 'api';
import { requestLoginSucceeded } from 'actions';
import { call, put } from 'redux-saga/effects';
import { push } from 'connected-react-router';


const api = new Api();

export function* fetchLogin(action) {
    try {
        const credentials = action.payload.data;
        const response = yield call(api.getAuth, credentials);
        localStorage.setItem('token', response.access);
        Object.keys(response.user).forEach(key => {
            if ( key === 'skills' || key === 'interests' || key === 'jobs' || key === 'reviews') localStorage.setItem(key, JSON.stringify(response.user[key]));
            else localStorage.setItem(key, response.user[key]);
        })
        const { user: { type, skills, interests } } = response;
        yield put(requestLoginSucceeded(response.user));
        if (type === 'volunteer' && !skills.length) yield put(push('/guide'));
        else if (type === 'enterprise' && !interests.length) yield put(push('/guide'));
        else yield put(push('/home'));
        yield put(push('/home'));
    } catch (error) {
        yield put({ type: 'REQUEST_LOGIN_FAIL', message: JSON.parse(error.message) });
    }
}
