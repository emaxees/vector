import Api from 'api';
import React from 'react';
import { Message } from 'components';
import { setPopupContent, showPopup, submitFeedbackSucceeded } from 'actions';
import { call, put } from 'redux-saga/effects';
import { goBack } from 'connected-react-router';


const api = new Api();
export function* submitFeedback(action) {
    try {
        const { feedback } = action.payload;
        const response = yield call(api.postFeedback, feedback);
        yield(
            put(setPopupContent(
                React.createElement(
                    Message,
                    {
                        title: 'Success',
                        icon: 'thumb_up_alt',
                        text: 'Feedback submited succesfully!',
                    }
                )
            ))
        )
        yield put(showPopup());
        yield put(goBack());
        yield put(submitFeedbackSucceeded(response));
    } catch (error) {
        yield put({ type: 'SUBMIT_FEEDBACK_FAIL', message: JSON.parse(error.message) });
    }
}
