import Api from 'api';
import { requestChatsSucceeded } from 'actions';
import { call, put } from 'redux-saga/effects';


const api = new Api();
export function* fetchChats() {
    try {
        const response = yield call(api.getChats);
        yield put(requestChatsSucceeded(response));
    } catch (error) {
        yield put({ type: 'REQUEST_CHATS_FAIL', message: JSON.parse(error.message) });
    }
}
