import Api from 'api';
import { requestVolunteersSucceeded } from 'actions';
import { call, put } from 'redux-saga/effects';


const api = new Api();
export function* fetchVolunteers() {
    try {
        const response = yield call(api.getVolunteers);
        yield put(requestVolunteersSucceeded(response));
    } catch (error) {
        yield put({ type: 'REQUEST_VOLUNTEERS_FAIL', message: JSON.parse(error.message) });
    }
}
