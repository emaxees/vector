const initialState = {
    browseTab: 1,
    profileTab: 1,
    isPopupVisible: false,
    popupContent: null,
};

const AppReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'CHANGE_BROWSE_TAB': {
            const { tab } = action.payload;
            return { ...state,  browseTab: tab };
        }
        case 'CHANGE_PROFILE_TAB': {
            const { tab } = action.payload;
            return { ...state,  profileTab: tab };
        }
        case 'RESET_PROFILE_TAB': {
            return { ...state,  profileTab: initialState.profileTab };
        }
        case 'SHOW_POPUP': {
            return { ...state, isPopupVisible: true };
        }
        case 'HIDE_POPUP': {
            return { ...state, isPopupVisible: false };
        }
        case 'SET_POPUP_CONTENT': {
            const { content } =  action.payload;
            return { ...state, popupContent: content };
        }
        default:
            return state
    }
};

export default AppReducer;
