const initialState = {
    bio: localStorage.getItem('bio'),
    id: localStorage.getItem('id'),
    email: localStorage.getItem('email'),
    companyName: localStorage.getItem('companyName'),
    administratorName: localStorage.getItem('administratorName'),
    firstName: localStorage.getItem('firstName'),
    lastName: localStorage.getItem('lastName'),
    occupation: localStorage.getItem('occupation'),
    skills: JSON.parse(localStorage.getItem('skills')),
    interests: JSON.parse(localStorage.getItem('interests')),
    jobs: JSON.parse(localStorage.getItem('jobs')),
    reviews: JSON.parse(localStorage.getItem('reviews')),
    type: localStorage.getItem('type'),
    fetching: false,
};

const UserReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'RESET_USER_TYPE': {
            return { ...state, type: undefined }
        }
        case 'REQUEST_LOGIN_SUCCEEDED': {
            const { user } = action.payload;
            return {...state, ...user }
        }
        case 'SET_USER_TYPE': {
            const { type } = action.payload;
            return { ...state,  type };
        }
        default:
            return state
    }
};

export default UserReducer;
