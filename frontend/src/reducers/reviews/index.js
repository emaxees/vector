const initialState = {
    list: [],
    isFetching: true,
    form: {
        title: '',
        description: '',
        rating: 1,
        hours: 0,
        reviewer: undefined,
        user: undefined,
    }
};

const ReviewsReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'REQUEST_REVIEWS_SUCCEEDED': {
            const { response } = action.payload;
            return { ...state,  list: [...response] };
        }
        case 'UPDATE_REVIEW_FORM': {
            const {key, value} = action.payload;
            return {...state, form: { ...state.form, [key]: value } }
        }
        case 'RESET_REVIEW_FORM': {
            return {...state, form: { ...initialState.form } }
        }
        default:
            return state
    }
};

export default ReviewsReducer;
