const initialState = {
    list: []
};

const VolunteersReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'REQUEST_VOLUNTEERS_SUCCEEDED': {
            const { volunteers } = action.payload;
            return {...state, list: [...volunteers] }
        }
        default:
            return state
    }
};

export default VolunteersReducer;
