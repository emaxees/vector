const initialState = {
    user: undefined,
    isFetching: true,
};

const ProfileReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'REQUEST_USER_PROFILE_SUCCEEDED': {
            const { response } = action.payload;
            return { ...state,  user: response };
        }
        case 'UPDATE_USER_PROFILE_FORM': {
            const {key, value} = action.payload;
            return {...state, user: { ...state.user, [key]: value } }
        }
        case 'RESET_USER_PROFILE': {
            return initialState;
        }
        default:
            return state
    }
};

export default ProfileReducer;
