const initialState = {
    email: '',
    password: '',
    message: undefined,
}

const LoginReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'UPDATE_LOGIN_FORM': {
            const {key, value} = action.payload;
            return {...state, [key]: value }
        }
        case 'REQUEST_LOGIN_FAIL': {
            const { message } = action;
            return {...state, message }
        }
        default:
            return state
    }
}

export default LoginReducer