import React from 'react';
import ReactGA from 'react-ga';
import * as serviceWorker from './serviceWorker';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import store, { history } from 'core/store';
import App from './views/app';
import './styles/styles.css';


ReactGA.initialize('UA-160823562-1');
ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App history={history}/>
        </ConnectedRouter>
    </Provider>,
    document.getElementById('root'),
);

serviceWorker.register();
