import moment from 'moment';
/* eslint max-len: 0 */

export const isValidEmail = (email) => {
    const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(String(email).toLowerCase());
};

export const isValidField = value => Boolean(value);

export const isAuthenticated = () => Boolean(localStorage.getItem('token'));

export const decodeHtmlString = (stringToDecode) => {
    const parser = new DOMParser();
    return parser.parseFromString(`<!doctype html><body>${stringToDecode}`, 'text/html').body.textContent;
};

export const isObjectEmpty = obj => !(Object.keys(obj).length);

export const formatDate = date => moment(date).format('DD MMMM YYYY');

export const formatTime = time => moment(time, 'HH:mm:ss').format('hh:mm A');

export const openUrl = (url, history) => {
    if (url.includes(window.location.hostname)) {
        const pathParts = url.split('/');
        const path = pathParts.pop() || pathParts.pop();
        history.push(`post/${path}`);
    } else if (url.includes('http://') || url.includes('https://')) window.open(url);
    else window.open(`http://${url}`);
};

export const stripTags = (html) => {
    const tmp = document.createElement('div');
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || '';
};

export const capitalize = (string) => string.charAt(0).toUpperCase() + string.slice(1);

export const between = (x, min, max) => x >= min && x <= max;

export const getSkillLevel = (points) => {
    if (between(points, 0, 500)) return 'Beginner';
    if (between(points, 500, 999)) return 'Intermediate';
    return 'Advanced';
}
